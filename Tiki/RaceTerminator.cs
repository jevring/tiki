using System;

namespace Tiki
{
	/// <summary>
	/// Summary description for RaceTerminator.
	/// </summary>
	public class RaceTerminator
	{
		private bool force;
		private Race race;

		public RaceTerminator(bool force, Race race)
		{
			this.force = force;
			this.race = race;
		}

		public void Terminate()
		{
			race.Terminate(force);
		}
	}
}

using System;
using System.Threading;
using System.Windows.Forms;
using Tiki.Gui;

namespace Tiki
{
	/// <summary>
	/// Summary description for ClientEngine.
	/// </summary>
	public class ClientEngine
	{
		private ClientGui gui;
		private ServerEngine engine;
		private bool connectedToIrc = false;
		IdentServer ident = null;
		Thread identThread = null;

		
		Thread irt;
		IrcConnection ircc;

		public ClientEngine(ClientGui gui)
		{
			this.gui = gui;
			// It would appear that we have to start the gui first if we want to be able to view it at all
			Logger.Initialize();
			
			// load the last 2 days of dupes into memory
			DupeDB.Initialize(2);
			Config.Load();

			object o = Config.GeneralSettings["use_ident"];
			bool useIdent = false;
			if (o != null)
			{
				useIdent = (bool)o;	
			}
			if (useIdent) 
			{
				ident = new IdentServer();
				identThread = new Thread(new ThreadStart(ident.Run));
				identThread.Name = "IdentServer";
				identThread.IsBackground = true;
				identThread.Start();
			}
		}
		public void Shutdown()
		{
			Logger.Log("saveing 1");
			Config.Save();
			Logger.Log("saveing 2");
			DupeDB.Save();
			Logger.Log("saveing 3");
			if (ident != null) 
			{
				Logger.Log("saveing 4");
				ident.Shutdown();
				Logger.Log("saveing 5");
				identThread.Abort();
				Logger.Log("saveing 6");
			}
			if (connectedToIrc)
			{
				// we obviously need to disconnect before we abort the thread, duh.
				Logger.Log("saveing 7");
				ircc.Disconnect();
				Logger.Log("saveing 8");
				irt.Abort();
				Logger.Log("saveing 9");
			}
			ShutdownServerEngine();
			Logger.Log("saveing 11");
			ConnectionPool.Shutdown();
			Logger.Log("saveing 12");
			Logger.Close();

			// called from the gui
		}
		public void ConnectToIrc()
		{
			if (!connectedToIrc)
			{
				connectedToIrc = true;
				for (int i = 0; i < Config.IrcConnectionSettings.Count; i++)
				{
					IrcConnectionSettings ircs = (IrcConnectionSettings) Config.IrcConnectionSettings[i];
					ircc = new IrcConnection(ircs);
					bool connected = ircc.Connect();

					if (connected)
					{
						IrcParser parser = new IrcParser(this);
						IrcReader ir = new IrcReader(ircc.Reader, parser);
						irt = new Thread(new ThreadStart(ir.run));
						irt.Name = "IrcReader";
						irt.IsBackground = true;
						irt.Start();
					}	
				}
			}
		}

		public void CreateServerEngine()
		{
			if (engine == null) 
			{
				// todo: execute this in it's own thread
				engine = new ServerEngine(this);
				ConnectionPool.Initialize();
			}
			else
			{
				Logger.Log("server engine already exists");
			}
		}

		
		public void HandleRaceAnnounce(RaceAnnounce announce)
		{
			// we need to check here if we already have done something about the announce for this release
			// if we have (i.e., it's in the dupe db, just update the time-from-newdir/pre value for the source
			// otherwise, send it to the gui
			//Logger.Log(announce.ReleaseName + ":" + announce.Source.Name + ":" + gui.DisplaySiteArrayList(announce.Destinations) + ":" + Environment.NewLine + announce.AnnounceLine);
			if (announce.Destinations.Count > 0)
			{
				// todo: update the section for the site in question with the time from-from-newdir/pre value
				gui.Announce(announce);
			}
		}
		public void CreateNewRace(RaceAnnounce announce)
		{
			// tell the server engine to trade this release
			// since this waits for the thread to finish anyway, we might aswell not create a thread, but conenct the sites in engine.NewRace(...)
			if (engine != null) 
			{
				RaceStart rs = new RaceStart(announce, engine);
				Thread t = new Thread(new ThreadStart(rs.Start));
				t.Name = "RaceStart:" + announce.ReleaseName;
				t.IsBackground = true;
				t.Start();
			}
			else
			{
				if (gui.dupeDB.Checked)
				{
					// it's ok, we don't need the engine here.
				}
				else
				{
					MessageBox.Show("Please turn on the trading engine first");
				}
			}
		}

		public Race GetRace(string releaseName)
		{
			return (Race)engine.Races[releaseName];
		}

		public void UpdateRace(Race race)
		{
			gui.UpdateRace(race);
		}

		public void IrcActivity()
		{
			// tell the gui, and blink a led or something
			gui.IrcActivity();
		}

		public void RemoveRaceFromQueue(Race race)
		{
			engine.RemoveRaceFromQueue(race);
		}

		public void Nuke(RaceAnnounce announce, bool entireRace)
		{
			engine.Nuke(announce, entireRace);
		}
		public void ShutdownServerEngine()
		{
			if (engine != null) 
			{
				engine.Shutdown(false);
			}
		}
		public void ShutdownIrcConnection()
		{
			if (connectedToIrc)
			{
				irt.Abort();
				ircc.Disconnect();
			}
		}



		public bool ConnectedToIrc
		{
			get { return connectedToIrc; }
			set { connectedToIrc = value; }
		}

		public void Join(string channel)
		{
			gui.Join(channel);
		}
	}
}


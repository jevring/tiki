using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Soap;

namespace Tiki
{
	/// <summary>
	/// Summary description for DupeDB.
	/// </summary>
	public class DupeDB
	{
		private static Hashtable dupes = new Hashtable();
		private static Hashtable groups = new Hashtable();
		private static bool initialized = false;

		public static void Initialize(int days)
		{
			// load the last [days] days from the file
			// (should this be a "normal" file, or a serialized file?)
			// note: do this for the dupes aswell

			InitializeStatisticsEngine();
		}
		public static void Save()
		{
			// writes the dupedb to file in a proper manner
			// note: do this for the dupes aswell
			
			SaveStatisticsEngine();
		}
		public static void AddRelease(string section, string releasename)
		{
			if (!dupes.Contains(releasename)) 
			{
				//Logger.Log("Adding " + releasename + " in section " + section + " to DupeDB");
				dupes[releasename] = new DupeEntry(section, releasename);

				string group = releasename.Substring(releasename.LastIndexOf("-") + 1);
				//Logger.Log("Adding another release for group " + group + " in section " + section + " to the statistics engine");
				// notice that this only happens once per release name
				IncreaseReleaseGroupToSectionFrequency(group, section);
			}
		}
		public static DupeEntry GetDupe(string releasename)
		{
			return (DupeEntry)dupes[releasename];
		}
		public static void InitializeStatisticsEngine()
		{
			SoapFormatter sf = new SoapFormatter();
			FileStream fs;

			try 
			{
				fs = new FileStream("GroupStatistics.xml", FileMode.Open);
				groups = (Hashtable)sf.Deserialize(fs);
				fs.Close();
			}
			catch
			{
				Logger.Log("Could not load GroupStatistics, skipping");
			}
			initialized = true;
		}
		public static void SaveStatisticsEngine()
		{
			Logger.Log("saving statistics engine");
			// saves the db to disk
			FileStream fs = new FileStream("GroupStatistics.xml", FileMode.Create);
			SoapFormatter sf = new SoapFormatter();
			if (initialized) 
			{
				sf.Serialize(fs, groups);
			}
			fs.Close();
		}
		public static double GetSectionProbabilityForGroup(string group, string section)
		{
			GroupNameStatisticsEntry gnse = (GroupNameStatisticsEntry)groups[group];
			if (gnse != null)
			{
				return gnse.GetSectionProbability(section);
			}
			else
			{
				return -1.0d;
			}
		}
		public static string GetMostProbableSectionForGroup(string group)
		{
			GroupNameStatisticsEntry gnse = (GroupNameStatisticsEntry)groups[group];
			if (gnse != null)
			{
				return gnse.GetMostProbableSection();
			}
			else
			{
				return "unknown";
			}
		}
		public static void IncreaseReleaseGroupToSectionFrequency(string group, string section)
		{
			GroupNameStatisticsEntry gnse;
			if (groups.Contains(group))
			{
				gnse = (GroupNameStatisticsEntry)groups[group];
			}
			else
			{
				// adding a new entry for this group
				gnse = new GroupNameStatisticsEntry(group);
				groups[group] = gnse;
			}
			gnse.NewRelease(section);

		}
	}
}

using System;
using System.Collections;

namespace Tiki
{
	/// <summary>
	/// Summary description for RaceAnnounce.
	/// </summary>
	public class RaceAnnounce
	{
		private string announceType;
		private string releaseName;
		private Site source;
		private ArrayList destinations;
		private string section;
		private long time;
		private string announceLine;

		public RaceAnnounce(string announceType, string releaseName, Site source, ArrayList destinations, string section, string announceLine)
		{
			if (announceType == null)
			{
				announceType = "other";
			}
			this.announceType = announceType;
			this.releaseName = releaseName;
			this.source = source;
			this.destinations = destinations;
			this.section = section;
			this.time = System.DateTime.Now.Ticks;
			this.announceLine = announceLine;
		}

		public string AnnounceType
		{
			get { return announceType; }
		}

		public string ReleaseName
		{
			get { return releaseName; }
		}

		public Site Source
		{
			get { return source; }
		}

		public ArrayList Destinations
		{
			get { return destinations; }
			set { destinations = value; }
		}

		public string Section
		{
			get { return section; }
			set { section = value; }
		}

		public long Time
		{
			get { return time; }
		}

		public string AnnounceLine
		{
			get { return announceLine; }
		}
		public override string ToString()
		{
			return releaseName;
		}

	}
}

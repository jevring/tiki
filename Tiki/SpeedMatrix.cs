using System;
using System.Collections;

namespace Tiki
{
	/// <summary>
	/// Summary description for SpeedMatrix.
	/// </summary>
	[Serializable]
	public class SpeedMatrix
	{
		private Hashtable sources;
		public SpeedMatrix()
		{
			sources = new Hashtable();
		}
		public double GetSpeed(Site source, Site destination)
		{
			if (source.Equals(destination))
			{
				return -1.0d;
			}
			Hashtable s = (Hashtable)sources[source.Name];
			if (s != null)
			{
				object o = s[destination.Name];
				if (o == null)
				{
					Logger.Log("found speed, returning 0.0d");
					return 0.0d;
				}
				else
				{
					Logger.Log("found speed, returning " + (double)s[destination.Name]);
					return (double)s[destination.Name];
				}
			}
			else
			{
				Logger.Log("speed in matrix was null, adding");
				// update the list for future reference
				SetSpeed(source, destination, 0);
				return 0.0d;
			}
		}
		public void SetSpeed(Site source, Site destination, double speed)
		{
			if (double.IsInfinity(speed) || double.IsNaN(speed))
			{
				Logger.Log("Not adding speed, it's either Infinity or NaN");
			}
			else 
			{
				Logger.Log("adding speed from " + source.Name + " to " + destination.Name + " : " + speed + " bytes per second");
				Hashtable s = (Hashtable)sources[source.Name];
				if (s != null)
				{
					Logger.Log("we found a speed entry, updating");
					s[destination.Name] = speed;
				}
				else
				{
					Logger.Log("making a new speed entry");
					sources.Add(source.Name, new Hashtable());
					SetSpeed(source, destination, speed);
				}
			}
		}
		public Hashtable GetSpeeds(Site source)
		{

			return (Hashtable)sources[source.Name];
		}
	}
}

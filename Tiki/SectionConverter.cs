using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Soap;
using System.Windows.Forms;

namespace Tiki
{
	/// <summary>
	/// Summary description for SectionConverter.
	/// </summary>
	public class SectionConverter
	{
		public SectionConverter()
		{
			FileStream fs;
			Hashtable sites = new Hashtable();
			SoapFormatter sf = new SoapFormatter();
			// todo: remove this later and change NewSites.xml to just Sites.xml
			// sites
			DialogResult dr = MessageBox.Show("You can only run the converter once. Make SURE you don't run it twice, bad things might happen. \r\nAlso, before you run it, do a search-and-replace in Sites.xml and replace ':Section' with ':OldSection'\r\nAre you sure you want to run the converter (only answer yes if you have made the changes to Sites.xml)", "Run converter?", MessageBoxButtons.YesNo);

			if (dr == DialogResult.Yes) 
			{
				try 
				{
					fs = new FileStream("Sites.xml", FileMode.Open);
					sites = (Hashtable)sf.Deserialize(fs);
					fs.Close();
					foreach (Site site in sites.Values)
					{
						Hashtable sections = new Hashtable();
						foreach (OldSection section in site.Sections.Values)
						{
							Section ns = new Section(section.Name, section.Site, section.Path, section.Botname, section.Active);
							ns.AlwaysAllow = section.AlwaysAllow;
							ns.Banned = section.Banned;
							ns.AllowedYear = 2005;
							try 
							{
								for (int i = 0; i < section.AllowedYears.Count; i++)
								{
									string year = (string) section.AllowedYears[i];
									if (Convert.ToInt32(year) < ns.AllowedYear)
									{
										ns.AllowedYear = Convert.ToInt32(year);
									}
								}
							}
							catch
							{
								Console.Out.WriteLine("Failed to convert year, defaulting to 2005");
							}
							ns.Required = new ArrayList();
							sections[ns.Name] = ns;
						}
						site.Sections = sections;
					}
					fs.Close();	
					// sites
					fs = new FileStream("Sites.xml", FileMode.Create);
					if (sites != null) 
					{
						sf.Serialize(fs, sites);
					}
					fs.Close();
				} 
				catch (Exception e)
				{
					MessageBox.Show("You did not change :Section to :OldSection in Sites.xml, OR you have already ran this converter.\r\n" + e.Message + Environment.NewLine + e.StackTrace);
					/*
					Console.Out.WriteLine(e.Message);
					Console.Out.WriteLine(e.StackTrace);
					Console.Out.WriteLine("Could not load Sites, skipping");
					*/
				}
			
			
				
			}
		}

		public static void Main(string[] args)
		{
			new SectionConverter();
		}
	}
}

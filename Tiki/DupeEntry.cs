using System;

namespace Tiki
{
	/// <summary>
	/// Summary description for DupeEntry.
	/// </summary>
	public class DupeEntry
	{
		private string section;
		private DateTime time;
		private string releasename;
		private string nukereason;
		private bool nuked;
		private TimeZone timeZone;

		public DupeEntry(string section, string releasename)
		{
			this.section = section;
			this.releasename = releasename;
			this.time = System.DateTime.Now;
			this.timeZone = System.TimeZone.CurrentTimeZone;
		}
		public void Nuke(string nukereason)
		{
			this.nuked = true;
			this.nukereason = nukereason;
		}

		public string Section
		{
			get { return section; }
		}

		public DateTime Time
		{
			get { return time; }
		}

		public string Releasename
		{
			get { return releasename; }
		}

		public string Nukereason
		{
			get { return nukereason; }
		}

		public bool IsNuked
		{
			get { return nuked; }
		}

		public TimeZone TimeZone
		{
			get { return timeZone; }
		}
	}
}

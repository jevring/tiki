using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace Tiki
{
	/// <summary>
	/// Summary description for IrcParser.
	/// </summary>
	public class IrcParser
	{
		private Regex release;
		private ClientEngine engine;
		private Hashtable knownAnnounces;
		private Regex stripper;
		private Regex mp3Live;
		private Regex year;
		private Hashtable handledAnnounces;

		public IrcParser(ClientEngine engine)
		{
			knownAnnounces = new Hashtable();
			handledAnnounces = new Hashtable();
			this.engine = engine;
			// todo: make sure this regexp can handle (and remove) .!? (remove colons in strip : ) at the end of announces
			this.release = new Regex(@" ((?:[()a-zA-Z0-9]+[-|_|.]+)+[()a-zA-Z0-9]+-[a-zA-Z0-9]+) ", RegexOptions.Compiled);
			this.year = new Regex(@"-(\d{4})-", RegexOptions.Compiled | RegexOptions.RightToLeft);
			this.stripper = new Regex(@"[^-_.()0-9a-zA-Z]", RegexOptions.Compiled);
			// hmm, maybe this should be only one backslash, since tht works for the year stuff above
			this.mp3Live = new Regex(@"(?:-CABLE-|-LIVE-|-DAB-|DVB|-MD-|-RADIO-|-SAT-|LIVE.*\d{2}-\d{2}|\d{2}-\d{2}.*[1-2]?\d{3}|LIVE_AT|LIVE_IN|LIVE_ON)+", RegexOptions.IgnoreCase | RegexOptions.Compiled);
		}

		public void parse(Response response)
		{
			// parse the command and whatnot here...
			switch (response.Command)
			{
				case "PRIVMSG": Privmsg(response); break;
				case "JOIN": Join(response); break;
			}
			engine.IrcActivity();
		}

		private void Join(Response response)
		{
			engine.Join(response.Message);
		}

		private void Privmsg(Response response)
		{
			// this both checks that the response came from a bot we know is connected to a site, and that there is a site.
			// IMPORTANT: the botnick NEEDS to be lowercase
			Site site = (Site)Config.Bots[response.SenderNick.ToLower()];
			if (site != null)
			{
				OnKnownBotAnnounce(response, site);
			}
		}

		private void OnKnownBotAnnounce(Response response, Site site)
		{
			Match m = release.Match(response.Message);
			string releaseName = "";
			if (m.Success)
			{
				releaseName = m.Groups[1].ToString();
			}
			DupeEntry dupe = DupeDB.GetDupe(releaseName);
			// we can use this if the announce type was "unknown"...
			// we don't want to do anything if we've already caught this announce
			// we can't check if dupe == null here, since we might have added a dupe from a dead of source_dissalowed site
			if (releaseName.Length > 0 && !handledAnnounces.Contains(releaseName))
			{
				string sectionName = GetSectionName(response.SenderNick, response.Message, releaseName);
				if ("unknown".Equals(sectionName))
				{
					if (dupe != null && !"unknown".Equals(dupe.Section))
					{
						sectionName = dupe.Section;
					}
				}
				Logger.Log("Found section name " + sectionName + " for release " + releaseName);
				Section section = (Section)site.Sections[sectionName];
				DupeDB.AddRelease(sectionName, releaseName);
				if (section != null)
				{
					if (section.Active)
					{
						ArrayList destinationSites = GetDestinationSites(sectionName);
						ArrayList allowedDestinationSites = GetAllowedDestinationSites(destinationSites, releaseName, sectionName, site.Name);

						// todo: try to determine if we know what kind of announce we have
						// if we can't figure it out, it's null
						RaceAnnounce announce = new RaceAnnounce(GetAnnounceType(response.Message, releaseName), releaseName, site, allowedDestinationSites, sectionName, response.Message);

						if (!knownAnnounces.Contains(releaseName + "#" + site.Name)) 
						{
							knownAnnounces.Add(releaseName + "#" + site.Name, System.DateTime.Now);
						}
						if (site.AllowAsSource && !ConnectionPool.IsDead(site.Name))
						{
							handledAnnounces[releaseName] = System.DateTime.Now;
							engine.HandleRaceAnnounce(announce);		
						}
						else
						{
							Logger.Log("Announce on DisallowedSource or Dead site section: " + sectionName + " name: " + releaseName + " on site " + site.Name + " (added to dupedb)");
							return;
						}
					}
					else
					{
						Logger.Log("Announce in INACTIVE section: " + sectionName + " name: " + releaseName + " on site " + site.Name + " (added to dupedb)");
					}	
				}
				else
				{
					// we end up here if either a) the site doesn't have the specified section (which should be impossible), or b) if we couldn't parse it (which with the statistics engine should also be fairly difficult)
					Logger.Log("Announce in UNDEFINED section: " + sectionName + " name: " + releaseName + " on site " + site.Name + " (added to dupedb)");
					//Logger.Log("Announce line was: " + response.Message);
					Logger.Log("Stripped announce line was: " + response.Message);
				}
			} 
		}

		private string GetAnnounceType(string message, string releaseName)
		{
			message = message.ToLower();
			int nukeIndex = message.IndexOf("nuke");
			if (nukeIndex > -1)
			{
				// check if the nuke is inside the releasename, but we'll do this later.
				Logger.Log("We found a nuke: " + releaseName);
				return "nuke";
			}
			else
			{
				// todo: we should also check the dupedb if this has been nuked before.
				return "other";
			}
		}

		private string GetSectionName(string botname, string announceLine, string releaseName)
		{
			// NOTE: this method needs ALOT more work to stop making perfectly parsable announces as unknown
			// fuck

			// todo: ALWAYS return "unknown" when the announce line contains "request"!

			// TODO: l�gg till n�tt h�r som bara kollar i dom sektionerna som botten har hand om.
			// if "sectionname" (som man skulle matcha i detta blocket) is in bot.sectionnames...


			// 2.0: ska man g�ra s� att man k�r inte en elseif, utan man f�rs�ker matcha p� flera sektioner, s� att man skaffar sig en lista, sen fr�gar man statistics enginen vilken sektion som �r mest sannorlik?

			string releaseGroup = releaseName.Substring(releaseName.LastIndexOf("-"));
			string mostProbablSection = DupeDB.GetMostProbableSectionForGroup(releaseGroup);
			// (requires 10 or more recorded announces from this group):
			//Logger.Log("I predict the section for " + releaseName + " announce will be: " + mostProbablSection);
			//Logger.Log("Looking for section in: " + announceLine.ToLower());
			// TODO: act more on this guessed section. (like actually using it when we get somehting that's unlikely)

			string announce = announceLine.ToLower();
			string release = releaseName.ToLower();
			// inga spaces runt denna!
			if (announce.IndexOf("mp3") > -1)
			{
				// this has problems with things like: Fatman_and_Marini-Technologic-Retail_Vinyl-2005-BWA 
				// maybe make this part into a regexp. something like [-_.]vinyl[-_.]

				// check for single
				if (release.IndexOf("vinyl-") > -1 || release.IndexOf("-ep-") > -1 || release.IndexOf("-cdm-") > -1 || release.IndexOf("-cds-") > -1)
				{
					// todo: make this better, check for .Vinyl. and such, maybe not make this lowercase check, just do it better
					return "mp3.singles";
				}
				// check for live
				if (mp3Live.Match(releaseName).Success)
				{
					return "mp3.live";
				}
				// *-CABLE-* *-LIVE-* *-DAB-* *DVB* *-MD-* *-RADIO-* *-SAT-* *LIVE*[0-9][0-9]-[0-9][0-9]* *[0-9][0-9]-[0-9][0-9]*[1-2][0-9][0-9][0-9]* *LIVE_AT* *LIVE_IN* *LIVE_ON*
				return "mp3";
			}
			else if (announce.IndexOf("0day") > -1)
			{
				return "0day";
			}
			else if (announce.IndexOf("tv") > -1)
			{
				// here, we MUST be able to specify REQUIRED strings, (-banned), otherwise we'll trade vcd when we only allod hdtv or pdtv
				if (announce.IndexOf("dvdr") > -1 && release.IndexOf("dvdrip") == -1)
				{
					return "tv.dvdr";
				}
				else if (release.IndexOf("divx") > -1 || release.IndexOf("xvid") > -1)
				{
					return "tv.divx";
				}
				else
				{
					return "tv";
				}
			}
			else if (release.IndexOf("svcd") > -1)
			{
				if ((release.IndexOf("screener") > -1 || release.IndexOf("scr") > -1) && release.IndexOf("dvd") > -1)
				{
					return "svcd.dvdscreener";
				}
				else if (release.IndexOf("screener") > -1 || release.IndexOf("telecine") > -1 || release.IndexOf("telesync") > -1 || release.IndexOf(".ts-") > -1 || release.IndexOf(".tc-") > -1 || release.IndexOf("cam") > -1)
				{
					return "vcd";
				}
				else if (release.IndexOf("dvdrip") > -1)
				{
					return "svcd";
				}
				else
				{
					// really?
					return "mvids";
				}
			}
			else if (announce.IndexOf("xxx") > -1)
			{
				// xxx must come before divx, otherwise it'll never catch xxx.divx
				if (release.IndexOf("divx") > -1 || release.IndexOf("xvid") > -1)
				{
					return "xxx.divx";
				}
				else 
				{
					return "xxx";
				}
			}
			else if (release.IndexOf("divx") > -1 || release.IndexOf("xvid") > -1)
			{
				if ((release.IndexOf("screener") > -1 || release.IndexOf("scr") > -1) && release.IndexOf("dvd") > -1)
				{
					return "divx.dvdscr";
				}
				else
				{
					return "divx";
				}
			}
			else if (release.IndexOf("ps2") > -1)
			{

				if (announce.IndexOf("rip") > -1)
				{
					if (release.IndexOf("pal") > -1)
					{
						return "ps2.rip.pal";
					}
					else
					{
						return "ps2.rip.ntsc";
					}
				}
				else
				{
					if (release.IndexOf("pal") > -1)
					{
						return "ps2.dvd.pal";
					}
					else
					{
						return "ps2.dvd.ntsc";
					}	
				}
			}
			else if (release.IndexOf("xbox") > -1)
			{
				if (announce.IndexOf("rip") > -1)
				{
					if (release.IndexOf("pal") > -1)
					{
						return "xbox.rip.pal";
					}
					else
					{
						return "xbox.rip.ntsc";
					}
				}
				else
				{
					if (release.IndexOf("pal") > -1)
					{
						return "xbox.dvd.pal";
					}
					else
					{
						return "xbox.dvd.ntsc";
					}	
				}
			}
			else if (release.IndexOf("dvdr") > -1 && release.IndexOf("dvdrip") == -1)
			{
				// this main condition really only needs to be if the releasename contains "DVDR"

				if (release.IndexOf("mdvdr") > -1)
				{
					// this is really music dvdr, we don't trade that quite yet
					return "musicdvdr";
				}
				else if (release.IndexOf("screener") > -1 || release.IndexOf("dvdscr") > -1)
				{
					if (release.IndexOf("pal") > -1)
					{
						return "dvdr.pal.dvdscr";
					}
					else
					{
						return "dvdr.ntsc.dvdscr";
					}
				}
				else
				{
					if (release.IndexOf("pal") > -1)
					{
						return "dvdr.pal";
					}
					else
					{
						return "dvdr.ntsc";
					}
				}
			}
			else if (announce.IndexOf("vcd") > -1 && release.IndexOf("svcd") == -1)
			{
				// this works, because we have already tested for "svcd"
				return "vcd";
			}
			else if (announce.IndexOf(" games ") > -1)
			{
				return "games";				
			}
			else if (announce.IndexOf("scd") > -1 || announce.IndexOf("sample cd") > -1 || announce.IndexOf("sample-cd") > -1 || announce.IndexOf("samplecd") > -1)
			{
				return "samplecd";
			}
			else if (announce.IndexOf(" dox ") > -1)
			{
				return "dox";
			}
			else if (announce.IndexOf(" pda ") > -1)
			{
				return "pda";
			}
			else if (announce.IndexOf(" gba ") > -1 || announce.IndexOf("game-boy") > -1 || announce.IndexOf("game boy") > -1)
			{
				return "gba";
			}
			else if (announce.IndexOf("apps") > -1 || announce.IndexOf("utils") > -1 || announce.IndexOf("isoapps") > -1 || announce.IndexOf("iso-apps") > -1)
			{
				return "isoapps";
			}
			else if (announce.IndexOf("music vid") > -1 || announce.IndexOf("music-vid") > -1 || announce.IndexOf("mvid") > -1)
			{
				return "mvids";
			}
			else if (announce.IndexOf(" gamecube ") > -1 || announce.IndexOf("game-cube") > -1 || announce.IndexOf("game cube") > -1 || announce.IndexOf(" GC ") > -1)
			{
				return "gamecube";
			}
			else if (announce.IndexOf("books") > -1 || announce.IndexOf("bookware") > -1)
			{
				return "bookware";
			}
			else
			{
				//todo: this is where we check the statistics database based on what group released this particular release
				if (botname == null)
				{
					return mostProbablSection;
				}
				else
				{
					Logger.Log("We couldn't find section in announce, trying botname");
					// if we fail, try to get the sectionname from the botname (long shot) (replace this later with the group checker dupe db stuff)
					return GetSectionName(null, botname, releaseName);
				}
			}
		}
		

		public ArrayList GetAllowedDestinationSites(ArrayList destinationSites, string releaseName, string sectionName, string sourceSiteName)
		{
			bool alwaysAllow = false;
			bool bannedAllow = true;
			bool yearAllow = false;
			bool requiredAllow = true;
			ArrayList allowedDestinationSites = new ArrayList();
			releaseName = releaseName.ToLower();
			for (int i = 0; i < destinationSites.Count; i++)
			{
				Site site = (Site) destinationSites[i];
				//Logger.Log("Checking to see if site " + site.Name + " is allowed as destination for " + releaseName);
				if (site.Name.Equals(sourceSiteName)) 
				{
					//Logger.Log("Skipping " + site.Name + " as source");
					continue;
				}
				Section section = (Section)site.Sections[sectionName];
				if (section.AlwaysAllow != null) 
				{
					for (int j = 0; j < section.AlwaysAllow.Count; j++)
					{
						string s = ((string)section.AlwaysAllow[j]).ToLower();

						//Logger.Log("checking 'always allowed' on " + site.Name + " in " + sectionName + ": " + s);
						if (s.Length > 0 && releaseName.IndexOf(s) > -1)
						{
							
							//Logger.Log("Release always allowed on " + site.Name + " because of string '" + s + "'");
							alwaysAllow = true;
							break;
						}
					}
				}
				else
				{
					alwaysAllow = false;
				}
				// only continue if we're not already matched
				if (!alwaysAllow)
				{
					if (section.Banned != null) 
					{
						for (int j = 0; j < section.Banned.Count; j++)
						{
							string banned = ((string)section.Banned[j]).ToLower();
							//Logger.Log("checking 'banned' on " + site.Name + " in " + sectionName + ": " + banned);
							if (banned.Length > 0 && releaseName.IndexOf(banned) > -1)
							{
								// we have the banned string, dissallow
								Logger.Log("Release banned on " + site.Name + " becase of banned string '" + banned + "'");
								bannedAllow = false;
								break;
							}
						}
					}
					else
					{
						bannedAllow = true;
					}
					// only continue if we haven't already failed
					if (bannedAllow)
					{
						// check required allow
						if (section.Required.Count > 0)
						{
							for (int j = 0; j < section.Required.Count; j++)
							{
								string required = (string) section.Required[j];
								if (releaseName.IndexOf(required) == -1)
								{
									// as soon as one of them doesn't exist, we break and dissallow
									requiredAllow = false;
									Logger.Log("Dissallowing " + releaseName + " because it didn't contain required string " + required);
									break;
								}
							}
						}
						else
						{
							requiredAllow = true;
						}
						// check year allow
						if (requiredAllow)
						{
							Match m = year.Match(releaseName);
							if (m.Success)
							{
								// get the first match, we're going right to left
								int releaseYear = Convert.ToInt32(m.Groups[1].ToString());
								if (releaseYear >= section.AllowedYear)
								{
									yearAllow = true;
								}
								else
								{
									Logger.Log("Dissallowing " + releaseName + " on " + site.Name + " because of the wrong year. was " + releaseYear + " but we wanted at least " + section.AllowedYear);
									yearAllow = false;
								}
							}
							else
							{
								// we found no year at all, tha tmeans it's the current year, allow it
								yearAllow = true;
							}
						}
					}
				}

				if (alwaysAllow || (yearAllow && bannedAllow && requiredAllow))
				{
					//Logger.Log("Allowing site: " + site.Name);
					allowedDestinationSites.Add(site);
				}
			}
			return allowedDestinationSites;

		}
		public ArrayList GetDestinationSites(string sectionName)
		{
			// get all the sites that have a matching section
			ArrayList sites = new ArrayList();
			// values are the actual sites, the keys are just their names.
			foreach (Site site in Config.Sites.Values)
			{
				// don't return any sites that are flagged as dead
				if (site.AllowAsDestination && !ConnectionPool.IsDead(site.Name)) 
				{
					Section s = (Section)site.Sections[sectionName];
					if (s != null)
					{
						sites.Add(s.Site);
					}
				}
			}
			return sites;
		}


	}
}

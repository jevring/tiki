using System;

namespace Tiki
{
	/// <summary>
	/// Summary description for RaceStart.
	/// </summary>
	public class RaceStart
	{
		private RaceAnnounce announce;
		private ServerEngine engine;

		public RaceStart(RaceAnnounce announce, ServerEngine engine)
		{
			this.announce = announce;
			this.engine = engine;
		}
		public void Start()
		{
			engine.CreateNewRace(announce);
		}
	}
}

using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace Tiki
{
	/// <summary>
	/// Summary description for IdentServer.
	/// </summary>
	public class IdentServer
	{
		private TcpListener server;

		public IdentServer()
		{
			try
			{
				server = new TcpListener(System.Net.IPAddress.Any, 113);	
			}
			catch (SocketException se)
			{
				Logger.Log("ERROR: couldn't listen to port 113 " + se.Message);
				server = null;
			}
			
		}
		public void Run()
		{
			if (server != null) 
			{
				server.Start();
				while (true)
				{
					try 
					{

						TcpClient client = server.AcceptTcpClient();
						IdentRequestHandler irh = new IdentRequestHandler(client);
						Thread t = new Thread(new ThreadStart(irh.HandleIdentRequest));
						t.Start();
					}
					catch
					{
						// this happens if we terminate 
					}
					
				}
			}
			else
			{
				Logger.Log("ident server is not running.");
			}
		}

		public void HandleIdentRequest()
		{
			
		}
		public void Shutdown()
		{
			if (server != null) 
			{
				server.Stop();
			}
		}
	}
}

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Tiki.Gui
{
	/// <summary>
	/// Summary description for SectionSettings.
	/// </summary>
	public class SectionSettings : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.Label label36;
		private System.Windows.Forms.Label label37;
		private System.Windows.Forms.Label label38;
		private System.Windows.Forms.Label label39;
		private System.Windows.Forms.Label label40;
		private System.Windows.Forms.Button button31;
		private System.Windows.Forms.TextBox gbaPath;
		private System.Windows.Forms.TextBox doxPath;
		private System.Windows.Forms.TextBox gamesPath;
		private System.Windows.Forms.TextBox dvdrNtscPath;
		private System.Windows.Forms.TextBox dvdrPalDvdscrPath;
		private System.Windows.Forms.TextBox dvdrPalPath;
		private System.Windows.Forms.TextBox divxDvdscrPath;
		private System.Windows.Forms.TextBox divxDvdripPath;
		private System.Windows.Forms.TextBox svcdDvdscrPath;
		private System.Windows.Forms.TextBox vcdPath;
		private System.Windows.Forms.TextBox tvDvdrPath;
		private System.Windows.Forms.TextBox tvPath;
		private System.Windows.Forms.TextBox dvdrNtscDvdscrPath;
		private System.Windows.Forms.TextBox pdaPath;
		private System.Windows.Forms.TextBox bookwarePath;
		private System.Windows.Forms.TextBox samplecdPath;
		private System.Windows.Forms.TextBox isoappsPath;
		private System.Windows.Forms.Label label41;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label42;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.TextBox ps2DvdNtscPath;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.TextBox ps2DvdPalPath;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.TextBox ps2RipNtscPath;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.TextBox ps2RipPalPath;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.TextBox xboxDvdNtscPath;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.TextBox xboxDvdPalPath;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox xboxRipNtscPath;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox xboxRipPalPath;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox gamecubePath;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label43;
		private System.Windows.Forms.Label label44;
		private System.Windows.Forms.Label label45;
		private System.Windows.Forms.Label label46;
		private System.Windows.Forms.Label label47;
		private System.Windows.Forms.Label label48;
		private System.Windows.Forms.Label label49;
		private System.Windows.Forms.Label label50;
		private System.Windows.Forms.TextBox mvidsPath;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox mvidsBotname;
		private System.Windows.Forms.CheckBox mvidsActive;
		private System.Windows.Forms.TextBox pdaBotname;
		private System.Windows.Forms.TextBox bookwareBotname;
		private System.Windows.Forms.TextBox samplecdBotname;
		private System.Windows.Forms.TextBox isoappsBotname;
		private System.Windows.Forms.TextBox gbaBotname;
		private System.Windows.Forms.TextBox doxBotname;
		private System.Windows.Forms.TextBox gamesBotname;
		private System.Windows.Forms.TextBox odayBotname;
		private System.Windows.Forms.CheckBox pdaActive;
		private System.Windows.Forms.CheckBox bookwareActive;
		private System.Windows.Forms.CheckBox samplecdActive;
		private System.Windows.Forms.CheckBox isoappsActive;
		private System.Windows.Forms.CheckBox gbaActive;
		private System.Windows.Forms.CheckBox doxActive;
		private System.Windows.Forms.CheckBox gamesActive;
		private System.Windows.Forms.CheckBox odayActive;
		private System.Windows.Forms.TextBox odayPath;
		private System.Windows.Forms.TextBox tvDivxPath;
		private System.Windows.Forms.TextBox dvdrNtscDvdscrBotname;
		private System.Windows.Forms.TextBox dvdrNtscBotname;
		private System.Windows.Forms.TextBox dvdrPalDvdscrBotname;
		private System.Windows.Forms.TextBox dvdrPalBotname;
		private System.Windows.Forms.TextBox divxDvdscrBotname;
		private System.Windows.Forms.TextBox svcdDvdscrBotname;
		private System.Windows.Forms.TextBox svcdBotname;
		private System.Windows.Forms.TextBox vcdBotname;
		private System.Windows.Forms.TextBox tvDvdrBotname;
		private System.Windows.Forms.TextBox tvBotname;
		private System.Windows.Forms.CheckBox dvdrNtscDvdscrActive;
		private System.Windows.Forms.CheckBox dvdrNtscActive;
		private System.Windows.Forms.CheckBox dvdrPalDvdscrActive;
		private System.Windows.Forms.CheckBox dvdrPalActive;
		private System.Windows.Forms.CheckBox divxDvdscrActive;
		private System.Windows.Forms.CheckBox svcdDvdscrActive;
		private System.Windows.Forms.CheckBox svcdActive;
		private System.Windows.Forms.CheckBox vcdActive;
		private System.Windows.Forms.CheckBox tvDvdrActive;
		private System.Windows.Forms.CheckBox tvActive;
		private System.Windows.Forms.TextBox mp3LivePath;
		private System.Windows.Forms.TextBox mp3SinglesPath;
		private System.Windows.Forms.TextBox mp3Path;
		private System.Windows.Forms.TextBox mp3LiveBotname;
		private System.Windows.Forms.TextBox mp3SinglesBotname;
		private System.Windows.Forms.TextBox mp3Botname;
		private System.Windows.Forms.CheckBox mp3LiveActive;
		private System.Windows.Forms.CheckBox mp3SinglesActive;
		private System.Windows.Forms.CheckBox mp3Active;
		private System.Windows.Forms.TextBox xboxDvdPalBotname;
		private System.Windows.Forms.TextBox xboxRipNtscBotname;
		private System.Windows.Forms.TextBox xboxRipPalBotname;
		private System.Windows.Forms.TextBox gamecubeBotname;
		private System.Windows.Forms.CheckBox xboxDvdPalActive;
		private System.Windows.Forms.CheckBox xboxRipNtscActive;
		private System.Windows.Forms.CheckBox xboxRipPalActive;
		private System.Windows.Forms.CheckBox gamecubeActive;
		private System.Windows.Forms.TextBox ps2DvdNtscBotname;
		private System.Windows.Forms.TextBox ps2DvdPalBotname;
		private System.Windows.Forms.TextBox ps2RipNtscBotname;
		private System.Windows.Forms.TextBox ps2RipPalBotname;
		private System.Windows.Forms.TextBox xboxDvdNtscBotname;
		private System.Windows.Forms.CheckBox ps2DvdNtscActive;
		private System.Windows.Forms.CheckBox ps2DvdPalActive;
		private System.Windows.Forms.CheckBox ps2RipNtscActive;
		private System.Windows.Forms.CheckBox ps2RipPalActive;
		private System.Windows.Forms.CheckBox xboxDvdNtscActive;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Button cancel;
		private System.Windows.Forms.Button ok;

		private System.Windows.Forms.Button mp3Rules;
		private System.Windows.Forms.Button mp3LiveRules;
		private System.Windows.Forms.Button mp3SinglesRules;
		private System.Windows.Forms.Button ps2DvdNtscRules;
		private System.Windows.Forms.Button ps2DvdPalRules;
		private System.Windows.Forms.Button ps2RipNtscRules;
		private System.Windows.Forms.Button ps2RipPalRules;
		private System.Windows.Forms.Button xboxDvdNtscRules;
		private System.Windows.Forms.Button xboxRipNtscRules;
		private System.Windows.Forms.Button xboxRipPalRules;
		private System.Windows.Forms.Button gamecubeRules;
		private System.Windows.Forms.Button mvidsRules;
		private System.Windows.Forms.Button pdaRules;
		private System.Windows.Forms.Button bookwareRules;
		private System.Windows.Forms.Button samplecdRules;
		private System.Windows.Forms.Button isoappsRules;
		private System.Windows.Forms.Button gamesRules;
		private System.Windows.Forms.Button odayRules;
		private System.Windows.Forms.Button tvDivxRules;
		private System.Windows.Forms.Button svcdRules;
		private System.Windows.Forms.Button vcdRules;
		private System.Windows.Forms.Button tvDvdrRules;
		private System.Windows.Forms.Button tvRules;
		private System.Windows.Forms.TextBox svcdPath;
		private System.Windows.Forms.Button dvdrPalDvdscrRules;
		private System.Windows.Forms.Button dvdrPalRules;
		private System.Windows.Forms.Button dvdrNtscDvdscrRules;
		private System.Windows.Forms.Button dvdrNtscRules;
		private System.Windows.Forms.Button divxDvdscrRules;
		private System.Windows.Forms.Button divxDvdripRules;
		private System.Windows.Forms.Button svcdDvdscrRules;
		private System.Windows.Forms.TextBox tvDivxBotname;
		private System.Windows.Forms.CheckBox tvDivxActive;
		private System.Windows.Forms.TextBox divxDvdripBotname;
		private System.Windows.Forms.CheckBox divxDvdripActive;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox xxxDivxBotname;
		private System.Windows.Forms.TextBox xxxBotname;
		private System.Windows.Forms.CheckBox xxxDivxActive;
		private System.Windows.Forms.Button xxxDivxRules;
		private System.Windows.Forms.CheckBox xxxActive;
		private System.Windows.Forms.Button xxxRules;
		private System.Windows.Forms.TextBox xxxDivxPath;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox xxxPath;
		private System.Windows.Forms.Label label51;
		private System.Windows.Forms.Button xboxDvdPalRules;
		private Site site;

		public SectionSettings(Site site)
		{
			this.site = site;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SectionSettings));
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.mp3LiveBotname = new System.Windows.Forms.TextBox();
			this.mp3SinglesBotname = new System.Windows.Forms.TextBox();
			this.mp3Botname = new System.Windows.Forms.TextBox();
			this.label43 = new System.Windows.Forms.Label();
			this.mp3LiveActive = new System.Windows.Forms.CheckBox();
			this.mp3LiveRules = new System.Windows.Forms.Button();
			this.mp3SinglesActive = new System.Windows.Forms.CheckBox();
			this.mp3SinglesRules = new System.Windows.Forms.Button();
			this.mp3Active = new System.Windows.Forms.CheckBox();
			this.mp3Rules = new System.Windows.Forms.Button();
			this.label44 = new System.Windows.Forms.Label();
			this.label45 = new System.Windows.Forms.Label();
			this.label46 = new System.Windows.Forms.Label();
			this.label47 = new System.Windows.Forms.Label();
			this.mp3LivePath = new System.Windows.Forms.TextBox();
			this.label48 = new System.Windows.Forms.Label();
			this.mp3SinglesPath = new System.Windows.Forms.TextBox();
			this.label49 = new System.Windows.Forms.Label();
			this.mp3Path = new System.Windows.Forms.TextBox();
			this.label50 = new System.Windows.Forms.Label();
			this.ps2DvdNtscBotname = new System.Windows.Forms.TextBox();
			this.ps2DvdPalBotname = new System.Windows.Forms.TextBox();
			this.ps2RipNtscBotname = new System.Windows.Forms.TextBox();
			this.ps2RipPalBotname = new System.Windows.Forms.TextBox();
			this.xboxDvdNtscBotname = new System.Windows.Forms.TextBox();
			this.xboxDvdPalBotname = new System.Windows.Forms.TextBox();
			this.xboxRipNtscBotname = new System.Windows.Forms.TextBox();
			this.xboxRipPalBotname = new System.Windows.Forms.TextBox();
			this.gamecubeBotname = new System.Windows.Forms.TextBox();
			this.ps2DvdNtscActive = new System.Windows.Forms.CheckBox();
			this.ps2DvdNtscRules = new System.Windows.Forms.Button();
			this.ps2DvdNtscPath = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.ps2DvdPalActive = new System.Windows.Forms.CheckBox();
			this.ps2DvdPalRules = new System.Windows.Forms.Button();
			this.ps2DvdPalPath = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.ps2RipNtscActive = new System.Windows.Forms.CheckBox();
			this.ps2RipNtscRules = new System.Windows.Forms.Button();
			this.ps2RipNtscPath = new System.Windows.Forms.TextBox();
			this.label18 = new System.Windows.Forms.Label();
			this.ps2RipPalActive = new System.Windows.Forms.CheckBox();
			this.ps2RipPalRules = new System.Windows.Forms.Button();
			this.ps2RipPalPath = new System.Windows.Forms.TextBox();
			this.label19 = new System.Windows.Forms.Label();
			this.xboxDvdNtscActive = new System.Windows.Forms.CheckBox();
			this.xboxDvdNtscRules = new System.Windows.Forms.Button();
			this.xboxDvdNtscPath = new System.Windows.Forms.TextBox();
			this.label20 = new System.Windows.Forms.Label();
			this.xboxDvdPalActive = new System.Windows.Forms.CheckBox();
			this.xboxDvdPalRules = new System.Windows.Forms.Button();
			this.xboxDvdPalPath = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.xboxRipNtscActive = new System.Windows.Forms.CheckBox();
			this.xboxRipNtscRules = new System.Windows.Forms.Button();
			this.xboxRipNtscPath = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.xboxRipPalActive = new System.Windows.Forms.CheckBox();
			this.xboxRipPalRules = new System.Windows.Forms.Button();
			this.xboxRipPalPath = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.gamecubeActive = new System.Windows.Forms.CheckBox();
			this.gamecubeRules = new System.Windows.Forms.Button();
			this.gamecubePath = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.xxxDivxBotname = new System.Windows.Forms.TextBox();
			this.xxxBotname = new System.Windows.Forms.TextBox();
			this.xxxDivxActive = new System.Windows.Forms.CheckBox();
			this.xxxDivxRules = new System.Windows.Forms.Button();
			this.xxxActive = new System.Windows.Forms.CheckBox();
			this.xxxRules = new System.Windows.Forms.Button();
			this.xxxDivxPath = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.xxxPath = new System.Windows.Forms.TextBox();
			this.label51 = new System.Windows.Forms.Label();
			this.mvidsBotname = new System.Windows.Forms.TextBox();
			this.mvidsActive = new System.Windows.Forms.CheckBox();
			this.mvidsRules = new System.Windows.Forms.Button();
			this.mvidsPath = new System.Windows.Forms.TextBox();
			this.label32 = new System.Windows.Forms.Label();
			this.pdaBotname = new System.Windows.Forms.TextBox();
			this.bookwareBotname = new System.Windows.Forms.TextBox();
			this.samplecdBotname = new System.Windows.Forms.TextBox();
			this.isoappsBotname = new System.Windows.Forms.TextBox();
			this.gbaBotname = new System.Windows.Forms.TextBox();
			this.doxBotname = new System.Windows.Forms.TextBox();
			this.gamesBotname = new System.Windows.Forms.TextBox();
			this.odayBotname = new System.Windows.Forms.TextBox();
			this.label41 = new System.Windows.Forms.Label();
			this.pdaActive = new System.Windows.Forms.CheckBox();
			this.pdaRules = new System.Windows.Forms.Button();
			this.pdaPath = new System.Windows.Forms.TextBox();
			this.label22 = new System.Windows.Forms.Label();
			this.bookwareActive = new System.Windows.Forms.CheckBox();
			this.bookwareRules = new System.Windows.Forms.Button();
			this.bookwarePath = new System.Windows.Forms.TextBox();
			this.label23 = new System.Windows.Forms.Label();
			this.samplecdActive = new System.Windows.Forms.CheckBox();
			this.samplecdRules = new System.Windows.Forms.Button();
			this.samplecdPath = new System.Windows.Forms.TextBox();
			this.label24 = new System.Windows.Forms.Label();
			this.isoappsActive = new System.Windows.Forms.CheckBox();
			this.isoappsRules = new System.Windows.Forms.Button();
			this.isoappsPath = new System.Windows.Forms.TextBox();
			this.label25 = new System.Windows.Forms.Label();
			this.gbaActive = new System.Windows.Forms.CheckBox();
			this.gbaPath = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.doxActive = new System.Windows.Forms.CheckBox();
			this.doxPath = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.gamesActive = new System.Windows.Forms.CheckBox();
			this.gamesRules = new System.Windows.Forms.Button();
			this.gamesPath = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.odayActive = new System.Windows.Forms.CheckBox();
			this.odayRules = new System.Windows.Forms.Button();
			this.odayPath = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.tvDivxBotname = new System.Windows.Forms.TextBox();
			this.tvDivxActive = new System.Windows.Forms.CheckBox();
			this.tvDivxRules = new System.Windows.Forms.Button();
			this.tvDivxPath = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.dvdrPalDvdscrRules = new System.Windows.Forms.Button();
			this.dvdrPalRules = new System.Windows.Forms.Button();
			this.dvdrNtscDvdscrBotname = new System.Windows.Forms.TextBox();
			this.dvdrNtscBotname = new System.Windows.Forms.TextBox();
			this.dvdrPalDvdscrBotname = new System.Windows.Forms.TextBox();
			this.dvdrPalBotname = new System.Windows.Forms.TextBox();
			this.divxDvdscrBotname = new System.Windows.Forms.TextBox();
			this.divxDvdripBotname = new System.Windows.Forms.TextBox();
			this.svcdDvdscrBotname = new System.Windows.Forms.TextBox();
			this.svcdBotname = new System.Windows.Forms.TextBox();
			this.vcdBotname = new System.Windows.Forms.TextBox();
			this.tvDvdrBotname = new System.Windows.Forms.TextBox();
			this.tvBotname = new System.Windows.Forms.TextBox();
			this.label21 = new System.Windows.Forms.Label();
			this.dvdrNtscDvdscrActive = new System.Windows.Forms.CheckBox();
			this.dvdrNtscDvdscrRules = new System.Windows.Forms.Button();
			this.dvdrNtscActive = new System.Windows.Forms.CheckBox();
			this.dvdrNtscRules = new System.Windows.Forms.Button();
			this.dvdrPalDvdscrActive = new System.Windows.Forms.CheckBox();
			this.dvdrPalActive = new System.Windows.Forms.CheckBox();
			this.divxDvdscrActive = new System.Windows.Forms.CheckBox();
			this.divxDvdscrRules = new System.Windows.Forms.Button();
			this.divxDvdripActive = new System.Windows.Forms.CheckBox();
			this.divxDvdripRules = new System.Windows.Forms.Button();
			this.svcdDvdscrActive = new System.Windows.Forms.CheckBox();
			this.svcdDvdscrRules = new System.Windows.Forms.Button();
			this.svcdActive = new System.Windows.Forms.CheckBox();
			this.svcdRules = new System.Windows.Forms.Button();
			this.vcdActive = new System.Windows.Forms.CheckBox();
			this.vcdRules = new System.Windows.Forms.Button();
			this.tvDvdrActive = new System.Windows.Forms.CheckBox();
			this.tvDvdrRules = new System.Windows.Forms.Button();
			this.tvActive = new System.Windows.Forms.CheckBox();
			this.tvRules = new System.Windows.Forms.Button();
			this.label26 = new System.Windows.Forms.Label();
			this.label42 = new System.Windows.Forms.Label();
			this.dvdrNtscDvdscrPath = new System.Windows.Forms.TextBox();
			this.label40 = new System.Windows.Forms.Label();
			this.dvdrNtscPath = new System.Windows.Forms.TextBox();
			this.label29 = new System.Windows.Forms.Label();
			this.dvdrPalDvdscrPath = new System.Windows.Forms.TextBox();
			this.label30 = new System.Windows.Forms.Label();
			this.dvdrPalPath = new System.Windows.Forms.TextBox();
			this.label31 = new System.Windows.Forms.Label();
			this.divxDvdscrPath = new System.Windows.Forms.TextBox();
			this.label33 = new System.Windows.Forms.Label();
			this.divxDvdripPath = new System.Windows.Forms.TextBox();
			this.label34 = new System.Windows.Forms.Label();
			this.svcdDvdscrPath = new System.Windows.Forms.TextBox();
			this.label35 = new System.Windows.Forms.Label();
			this.svcdPath = new System.Windows.Forms.TextBox();
			this.label36 = new System.Windows.Forms.Label();
			this.vcdPath = new System.Windows.Forms.TextBox();
			this.label37 = new System.Windows.Forms.Label();
			this.tvDvdrPath = new System.Windows.Forms.TextBox();
			this.label38 = new System.Windows.Forms.Label();
			this.tvPath = new System.Windows.Forms.TextBox();
			this.label39 = new System.Windows.Forms.Label();
			this.label27 = new System.Windows.Forms.Label();
			this.label28 = new System.Windows.Forms.Label();
			this.button31 = new System.Windows.Forms.Button();
			this.cancel = new System.Windows.Forms.Button();
			this.ok = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.tabControl1.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.tabPage3);
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.Location = new System.Drawing.Point(16, 32);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(472, 472);
			this.tabControl1.TabIndex = 0;
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.mp3LiveBotname);
			this.tabPage3.Controls.Add(this.mp3SinglesBotname);
			this.tabPage3.Controls.Add(this.mp3Botname);
			this.tabPage3.Controls.Add(this.label43);
			this.tabPage3.Controls.Add(this.mp3LiveActive);
			this.tabPage3.Controls.Add(this.mp3LiveRules);
			this.tabPage3.Controls.Add(this.mp3SinglesActive);
			this.tabPage3.Controls.Add(this.mp3SinglesRules);
			this.tabPage3.Controls.Add(this.mp3Active);
			this.tabPage3.Controls.Add(this.mp3Rules);
			this.tabPage3.Controls.Add(this.label44);
			this.tabPage3.Controls.Add(this.label45);
			this.tabPage3.Controls.Add(this.label46);
			this.tabPage3.Controls.Add(this.label47);
			this.tabPage3.Controls.Add(this.mp3LivePath);
			this.tabPage3.Controls.Add(this.label48);
			this.tabPage3.Controls.Add(this.mp3SinglesPath);
			this.tabPage3.Controls.Add(this.label49);
			this.tabPage3.Controls.Add(this.mp3Path);
			this.tabPage3.Controls.Add(this.label50);
			this.tabPage3.Controls.Add(this.ps2DvdNtscBotname);
			this.tabPage3.Controls.Add(this.ps2DvdPalBotname);
			this.tabPage3.Controls.Add(this.ps2RipNtscBotname);
			this.tabPage3.Controls.Add(this.ps2RipPalBotname);
			this.tabPage3.Controls.Add(this.xboxDvdNtscBotname);
			this.tabPage3.Controls.Add(this.xboxDvdPalBotname);
			this.tabPage3.Controls.Add(this.xboxRipNtscBotname);
			this.tabPage3.Controls.Add(this.xboxRipPalBotname);
			this.tabPage3.Controls.Add(this.gamecubeBotname);
			this.tabPage3.Controls.Add(this.ps2DvdNtscActive);
			this.tabPage3.Controls.Add(this.ps2DvdNtscRules);
			this.tabPage3.Controls.Add(this.ps2DvdNtscPath);
			this.tabPage3.Controls.Add(this.label16);
			this.tabPage3.Controls.Add(this.ps2DvdPalActive);
			this.tabPage3.Controls.Add(this.ps2DvdPalRules);
			this.tabPage3.Controls.Add(this.ps2DvdPalPath);
			this.tabPage3.Controls.Add(this.label17);
			this.tabPage3.Controls.Add(this.ps2RipNtscActive);
			this.tabPage3.Controls.Add(this.ps2RipNtscRules);
			this.tabPage3.Controls.Add(this.ps2RipNtscPath);
			this.tabPage3.Controls.Add(this.label18);
			this.tabPage3.Controls.Add(this.ps2RipPalActive);
			this.tabPage3.Controls.Add(this.ps2RipPalRules);
			this.tabPage3.Controls.Add(this.ps2RipPalPath);
			this.tabPage3.Controls.Add(this.label19);
			this.tabPage3.Controls.Add(this.xboxDvdNtscActive);
			this.tabPage3.Controls.Add(this.xboxDvdNtscRules);
			this.tabPage3.Controls.Add(this.xboxDvdNtscPath);
			this.tabPage3.Controls.Add(this.label20);
			this.tabPage3.Controls.Add(this.xboxDvdPalActive);
			this.tabPage3.Controls.Add(this.xboxDvdPalRules);
			this.tabPage3.Controls.Add(this.xboxDvdPalPath);
			this.tabPage3.Controls.Add(this.label9);
			this.tabPage3.Controls.Add(this.xboxRipNtscActive);
			this.tabPage3.Controls.Add(this.xboxRipNtscRules);
			this.tabPage3.Controls.Add(this.xboxRipNtscPath);
			this.tabPage3.Controls.Add(this.label10);
			this.tabPage3.Controls.Add(this.xboxRipPalActive);
			this.tabPage3.Controls.Add(this.xboxRipPalRules);
			this.tabPage3.Controls.Add(this.xboxRipPalPath);
			this.tabPage3.Controls.Add(this.label11);
			this.tabPage3.Controls.Add(this.gamecubeActive);
			this.tabPage3.Controls.Add(this.gamecubeRules);
			this.tabPage3.Controls.Add(this.gamecubePath);
			this.tabPage3.Controls.Add(this.label12);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Size = new System.Drawing.Size(464, 446);
			this.tabPage3.TabIndex = 2;
			this.tabPage3.Text = "Console / mp3";
			// 
			// mp3LiveBotname
			// 
			this.mp3LiveBotname.Location = new System.Drawing.Point(224, 104);
			this.mp3LiveBotname.Name = "mp3LiveBotname";
			this.mp3LiveBotname.Size = new System.Drawing.Size(96, 20);
			this.mp3LiveBotname.TabIndex = 10;
			this.mp3LiveBotname.Text = "";
			// 
			// mp3SinglesBotname
			// 
			this.mp3SinglesBotname.Location = new System.Drawing.Point(224, 72);
			this.mp3SinglesBotname.Name = "mp3SinglesBotname";
			this.mp3SinglesBotname.Size = new System.Drawing.Size(96, 20);
			this.mp3SinglesBotname.TabIndex = 6;
			this.mp3SinglesBotname.Text = "";
			// 
			// mp3Botname
			// 
			this.mp3Botname.Location = new System.Drawing.Point(224, 40);
			this.mp3Botname.Name = "mp3Botname";
			this.mp3Botname.Size = new System.Drawing.Size(96, 20);
			this.mp3Botname.TabIndex = 2;
			this.mp3Botname.Text = "";
			// 
			// label43
			// 
			this.label43.Location = new System.Drawing.Point(240, 16);
			this.label43.Name = "label43";
			this.label43.Size = new System.Drawing.Size(72, 16);
			this.label43.TabIndex = 164;
			this.label43.Text = "Botname";
			this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// mp3LiveActive
			// 
			this.mp3LiveActive.Location = new System.Drawing.Point(424, 112);
			this.mp3LiveActive.Name = "mp3LiveActive";
			this.mp3LiveActive.Size = new System.Drawing.Size(24, 16);
			this.mp3LiveActive.TabIndex = 12;
			// 
			// mp3LiveRules
			// 
			this.mp3LiveRules.Location = new System.Drawing.Point(328, 104);
			this.mp3LiveRules.Name = "mp3LiveRules";
			this.mp3LiveRules.Size = new System.Drawing.Size(64, 24);
			this.mp3LiveRules.TabIndex = 11;
			this.mp3LiveRules.Text = "Rules";
			this.mp3LiveRules.Click += new System.EventHandler(this.mp3LiveRules_Click);
			// 
			// mp3SinglesActive
			// 
			this.mp3SinglesActive.Location = new System.Drawing.Point(424, 80);
			this.mp3SinglesActive.Name = "mp3SinglesActive";
			this.mp3SinglesActive.Size = new System.Drawing.Size(24, 16);
			this.mp3SinglesActive.TabIndex = 8;
			// 
			// mp3SinglesRules
			// 
			this.mp3SinglesRules.Location = new System.Drawing.Point(328, 72);
			this.mp3SinglesRules.Name = "mp3SinglesRules";
			this.mp3SinglesRules.Size = new System.Drawing.Size(64, 24);
			this.mp3SinglesRules.TabIndex = 7;
			this.mp3SinglesRules.Text = "Rules";
			this.mp3SinglesRules.Click += new System.EventHandler(this.mp3SinglesRules_Click);
			// 
			// mp3Active
			// 
			this.mp3Active.Location = new System.Drawing.Point(424, 48);
			this.mp3Active.Name = "mp3Active";
			this.mp3Active.Size = new System.Drawing.Size(24, 16);
			this.mp3Active.TabIndex = 4;
			// 
			// mp3Rules
			// 
			this.mp3Rules.Location = new System.Drawing.Point(328, 40);
			this.mp3Rules.Name = "mp3Rules";
			this.mp3Rules.Size = new System.Drawing.Size(64, 24);
			this.mp3Rules.TabIndex = 3;
			this.mp3Rules.Text = "Rules";
			this.mp3Rules.Click += new System.EventHandler(this.mp3Rules_Click);
			// 
			// label44
			// 
			this.label44.Location = new System.Drawing.Point(408, 16);
			this.label44.Name = "label44";
			this.label44.Size = new System.Drawing.Size(48, 16);
			this.label44.TabIndex = 157;
			this.label44.Text = "Active";
			this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label45
			// 
			this.label45.Location = new System.Drawing.Point(320, 16);
			this.label45.Name = "label45";
			this.label45.Size = new System.Drawing.Size(72, 16);
			this.label45.TabIndex = 156;
			this.label45.Text = "Rules";
			this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label46
			// 
			this.label46.Location = new System.Drawing.Point(120, 16);
			this.label46.Name = "label46";
			this.label46.Size = new System.Drawing.Size(72, 16);
			this.label46.TabIndex = 155;
			this.label46.Text = "Path";
			this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label47
			// 
			this.label47.Location = new System.Drawing.Point(16, 16);
			this.label47.Name = "label47";
			this.label47.Size = new System.Drawing.Size(72, 16);
			this.label47.TabIndex = 154;
			this.label47.Text = "Section";
			this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// mp3LivePath
			// 
			this.mp3LivePath.Location = new System.Drawing.Point(104, 104);
			this.mp3LivePath.Name = "mp3LivePath";
			this.mp3LivePath.Size = new System.Drawing.Size(112, 20);
			this.mp3LivePath.TabIndex = 9;
			this.mp3LivePath.Text = "";
			// 
			// label48
			// 
			this.label48.Location = new System.Drawing.Point(16, 104);
			this.label48.Name = "label48";
			this.label48.Size = new System.Drawing.Size(72, 24);
			this.label48.TabIndex = 152;
			this.label48.Text = "mp3.live";
			this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// mp3SinglesPath
			// 
			this.mp3SinglesPath.Location = new System.Drawing.Point(104, 72);
			this.mp3SinglesPath.Name = "mp3SinglesPath";
			this.mp3SinglesPath.Size = new System.Drawing.Size(112, 20);
			this.mp3SinglesPath.TabIndex = 5;
			this.mp3SinglesPath.Text = "";
			// 
			// label49
			// 
			this.label49.Location = new System.Drawing.Point(16, 72);
			this.label49.Name = "label49";
			this.label49.Size = new System.Drawing.Size(72, 24);
			this.label49.TabIndex = 150;
			this.label49.Text = "mp3.singles";
			this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// mp3Path
			// 
			this.mp3Path.Location = new System.Drawing.Point(104, 40);
			this.mp3Path.Name = "mp3Path";
			this.mp3Path.Size = new System.Drawing.Size(112, 20);
			this.mp3Path.TabIndex = 1;
			this.mp3Path.Text = "";
			// 
			// label50
			// 
			this.label50.Location = new System.Drawing.Point(16, 40);
			this.label50.Name = "label50";
			this.label50.Size = new System.Drawing.Size(72, 24);
			this.label50.TabIndex = 148;
			this.label50.Text = "mp3";
			this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// ps2DvdNtscBotname
			// 
			this.ps2DvdNtscBotname.Location = new System.Drawing.Point(224, 392);
			this.ps2DvdNtscBotname.Name = "ps2DvdNtscBotname";
			this.ps2DvdNtscBotname.Size = new System.Drawing.Size(96, 20);
			this.ps2DvdNtscBotname.TabIndex = 46;
			this.ps2DvdNtscBotname.Text = "";
			// 
			// ps2DvdPalBotname
			// 
			this.ps2DvdPalBotname.Location = new System.Drawing.Point(224, 360);
			this.ps2DvdPalBotname.Name = "ps2DvdPalBotname";
			this.ps2DvdPalBotname.Size = new System.Drawing.Size(96, 20);
			this.ps2DvdPalBotname.TabIndex = 42;
			this.ps2DvdPalBotname.Text = "";
			// 
			// ps2RipNtscBotname
			// 
			this.ps2RipNtscBotname.Location = new System.Drawing.Point(224, 328);
			this.ps2RipNtscBotname.Name = "ps2RipNtscBotname";
			this.ps2RipNtscBotname.Size = new System.Drawing.Size(96, 20);
			this.ps2RipNtscBotname.TabIndex = 38;
			this.ps2RipNtscBotname.Text = "";
			// 
			// ps2RipPalBotname
			// 
			this.ps2RipPalBotname.Location = new System.Drawing.Point(224, 296);
			this.ps2RipPalBotname.Name = "ps2RipPalBotname";
			this.ps2RipPalBotname.Size = new System.Drawing.Size(96, 20);
			this.ps2RipPalBotname.TabIndex = 34;
			this.ps2RipPalBotname.Text = "";
			// 
			// xboxDvdNtscBotname
			// 
			this.xboxDvdNtscBotname.Location = new System.Drawing.Point(224, 264);
			this.xboxDvdNtscBotname.Name = "xboxDvdNtscBotname";
			this.xboxDvdNtscBotname.Size = new System.Drawing.Size(96, 20);
			this.xboxDvdNtscBotname.TabIndex = 30;
			this.xboxDvdNtscBotname.Text = "";
			// 
			// xboxDvdPalBotname
			// 
			this.xboxDvdPalBotname.Location = new System.Drawing.Point(224, 232);
			this.xboxDvdPalBotname.Name = "xboxDvdPalBotname";
			this.xboxDvdPalBotname.Size = new System.Drawing.Size(96, 20);
			this.xboxDvdPalBotname.TabIndex = 26;
			this.xboxDvdPalBotname.Text = "";
			// 
			// xboxRipNtscBotname
			// 
			this.xboxRipNtscBotname.Location = new System.Drawing.Point(224, 200);
			this.xboxRipNtscBotname.Name = "xboxRipNtscBotname";
			this.xboxRipNtscBotname.Size = new System.Drawing.Size(96, 20);
			this.xboxRipNtscBotname.TabIndex = 22;
			this.xboxRipNtscBotname.Text = "";
			// 
			// xboxRipPalBotname
			// 
			this.xboxRipPalBotname.Location = new System.Drawing.Point(224, 168);
			this.xboxRipPalBotname.Name = "xboxRipPalBotname";
			this.xboxRipPalBotname.Size = new System.Drawing.Size(96, 20);
			this.xboxRipPalBotname.TabIndex = 18;
			this.xboxRipPalBotname.Text = "";
			// 
			// gamecubeBotname
			// 
			this.gamecubeBotname.Location = new System.Drawing.Point(224, 136);
			this.gamecubeBotname.Name = "gamecubeBotname";
			this.gamecubeBotname.Size = new System.Drawing.Size(96, 20);
			this.gamecubeBotname.TabIndex = 14;
			this.gamecubeBotname.Text = "";
			// 
			// ps2DvdNtscActive
			// 
			this.ps2DvdNtscActive.Location = new System.Drawing.Point(424, 400);
			this.ps2DvdNtscActive.Name = "ps2DvdNtscActive";
			this.ps2DvdNtscActive.Size = new System.Drawing.Size(24, 16);
			this.ps2DvdNtscActive.TabIndex = 48;
			// 
			// ps2DvdNtscRules
			// 
			this.ps2DvdNtscRules.Location = new System.Drawing.Point(328, 392);
			this.ps2DvdNtscRules.Name = "ps2DvdNtscRules";
			this.ps2DvdNtscRules.Size = new System.Drawing.Size(64, 24);
			this.ps2DvdNtscRules.TabIndex = 47;
			this.ps2DvdNtscRules.Text = "Rules";
			this.ps2DvdNtscRules.Click += new System.EventHandler(this.ps2DvdNtscRules_Click);
			// 
			// ps2DvdNtscPath
			// 
			this.ps2DvdNtscPath.Location = new System.Drawing.Point(104, 392);
			this.ps2DvdNtscPath.Name = "ps2DvdNtscPath";
			this.ps2DvdNtscPath.Size = new System.Drawing.Size(112, 20);
			this.ps2DvdNtscPath.TabIndex = 45;
			this.ps2DvdNtscPath.Text = "";
			// 
			// label16
			// 
			this.label16.Location = new System.Drawing.Point(16, 392);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(72, 24);
			this.label16.TabIndex = 131;
			this.label16.Text = "ps2.dvd.ntsc";
			this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// ps2DvdPalActive
			// 
			this.ps2DvdPalActive.Location = new System.Drawing.Point(424, 368);
			this.ps2DvdPalActive.Name = "ps2DvdPalActive";
			this.ps2DvdPalActive.Size = new System.Drawing.Size(24, 16);
			this.ps2DvdPalActive.TabIndex = 44;
			// 
			// ps2DvdPalRules
			// 
			this.ps2DvdPalRules.Location = new System.Drawing.Point(328, 360);
			this.ps2DvdPalRules.Name = "ps2DvdPalRules";
			this.ps2DvdPalRules.Size = new System.Drawing.Size(64, 24);
			this.ps2DvdPalRules.TabIndex = 43;
			this.ps2DvdPalRules.Text = "Rules";
			this.ps2DvdPalRules.Click += new System.EventHandler(this.ps2DvdPalRules_Click);
			// 
			// ps2DvdPalPath
			// 
			this.ps2DvdPalPath.Location = new System.Drawing.Point(104, 360);
			this.ps2DvdPalPath.Name = "ps2DvdPalPath";
			this.ps2DvdPalPath.Size = new System.Drawing.Size(112, 20);
			this.ps2DvdPalPath.TabIndex = 41;
			this.ps2DvdPalPath.Text = "";
			// 
			// label17
			// 
			this.label17.Location = new System.Drawing.Point(16, 360);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(72, 24);
			this.label17.TabIndex = 127;
			this.label17.Text = "ps2.dvd.pal";
			this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// ps2RipNtscActive
			// 
			this.ps2RipNtscActive.Location = new System.Drawing.Point(424, 336);
			this.ps2RipNtscActive.Name = "ps2RipNtscActive";
			this.ps2RipNtscActive.Size = new System.Drawing.Size(24, 16);
			this.ps2RipNtscActive.TabIndex = 40;
			// 
			// ps2RipNtscRules
			// 
			this.ps2RipNtscRules.Location = new System.Drawing.Point(328, 328);
			this.ps2RipNtscRules.Name = "ps2RipNtscRules";
			this.ps2RipNtscRules.Size = new System.Drawing.Size(64, 24);
			this.ps2RipNtscRules.TabIndex = 39;
			this.ps2RipNtscRules.Text = "Rules";
			this.ps2RipNtscRules.Click += new System.EventHandler(this.ps2RipNtscRules_Click);
			// 
			// ps2RipNtscPath
			// 
			this.ps2RipNtscPath.Location = new System.Drawing.Point(104, 328);
			this.ps2RipNtscPath.Name = "ps2RipNtscPath";
			this.ps2RipNtscPath.Size = new System.Drawing.Size(112, 20);
			this.ps2RipNtscPath.TabIndex = 37;
			this.ps2RipNtscPath.Text = "";
			// 
			// label18
			// 
			this.label18.Location = new System.Drawing.Point(16, 328);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(72, 24);
			this.label18.TabIndex = 123;
			this.label18.Text = "ps2.rip.ntsc";
			this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// ps2RipPalActive
			// 
			this.ps2RipPalActive.Location = new System.Drawing.Point(424, 304);
			this.ps2RipPalActive.Name = "ps2RipPalActive";
			this.ps2RipPalActive.Size = new System.Drawing.Size(24, 16);
			this.ps2RipPalActive.TabIndex = 36;
			// 
			// ps2RipPalRules
			// 
			this.ps2RipPalRules.Location = new System.Drawing.Point(328, 296);
			this.ps2RipPalRules.Name = "ps2RipPalRules";
			this.ps2RipPalRules.Size = new System.Drawing.Size(64, 24);
			this.ps2RipPalRules.TabIndex = 35;
			this.ps2RipPalRules.Text = "Rules";
			this.ps2RipPalRules.Click += new System.EventHandler(this.ps2RipPalRules_Click);
			// 
			// ps2RipPalPath
			// 
			this.ps2RipPalPath.Location = new System.Drawing.Point(104, 296);
			this.ps2RipPalPath.Name = "ps2RipPalPath";
			this.ps2RipPalPath.Size = new System.Drawing.Size(112, 20);
			this.ps2RipPalPath.TabIndex = 33;
			this.ps2RipPalPath.Text = "";
			// 
			// label19
			// 
			this.label19.Location = new System.Drawing.Point(16, 296);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(72, 24);
			this.label19.TabIndex = 119;
			this.label19.Text = "ps2.rip.pal";
			this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// xboxDvdNtscActive
			// 
			this.xboxDvdNtscActive.Location = new System.Drawing.Point(424, 272);
			this.xboxDvdNtscActive.Name = "xboxDvdNtscActive";
			this.xboxDvdNtscActive.Size = new System.Drawing.Size(24, 16);
			this.xboxDvdNtscActive.TabIndex = 32;
			// 
			// xboxDvdNtscRules
			// 
			this.xboxDvdNtscRules.Location = new System.Drawing.Point(328, 264);
			this.xboxDvdNtscRules.Name = "xboxDvdNtscRules";
			this.xboxDvdNtscRules.Size = new System.Drawing.Size(64, 24);
			this.xboxDvdNtscRules.TabIndex = 31;
			this.xboxDvdNtscRules.Text = "Rules";
			this.xboxDvdNtscRules.Click += new System.EventHandler(this.xboxDvdNtscRules_Click);
			// 
			// xboxDvdNtscPath
			// 
			this.xboxDvdNtscPath.Location = new System.Drawing.Point(104, 264);
			this.xboxDvdNtscPath.Name = "xboxDvdNtscPath";
			this.xboxDvdNtscPath.Size = new System.Drawing.Size(112, 20);
			this.xboxDvdNtscPath.TabIndex = 29;
			this.xboxDvdNtscPath.Text = "";
			// 
			// label20
			// 
			this.label20.Location = new System.Drawing.Point(8, 264);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(88, 24);
			this.label20.TabIndex = 115;
			this.label20.Text = "xbox.dvd.ntsc";
			this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// xboxDvdPalActive
			// 
			this.xboxDvdPalActive.Location = new System.Drawing.Point(424, 240);
			this.xboxDvdPalActive.Name = "xboxDvdPalActive";
			this.xboxDvdPalActive.Size = new System.Drawing.Size(24, 16);
			this.xboxDvdPalActive.TabIndex = 28;
			// 
			// xboxDvdPalRules
			// 
			this.xboxDvdPalRules.Location = new System.Drawing.Point(328, 232);
			this.xboxDvdPalRules.Name = "xboxDvdPalRules";
			this.xboxDvdPalRules.Size = new System.Drawing.Size(64, 24);
			this.xboxDvdPalRules.TabIndex = 27;
			this.xboxDvdPalRules.Text = "Rules";
			this.xboxDvdPalRules.Click += new System.EventHandler(this.xboxDvdPalRules_Click);
			// 
			// xboxDvdPalPath
			// 
			this.xboxDvdPalPath.Location = new System.Drawing.Point(104, 232);
			this.xboxDvdPalPath.Name = "xboxDvdPalPath";
			this.xboxDvdPalPath.Size = new System.Drawing.Size(112, 20);
			this.xboxDvdPalPath.TabIndex = 25;
			this.xboxDvdPalPath.Text = "";
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(16, 232);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(72, 24);
			this.label9.TabIndex = 111;
			this.label9.Text = "xbox.dvd.pal";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// xboxRipNtscActive
			// 
			this.xboxRipNtscActive.Location = new System.Drawing.Point(424, 208);
			this.xboxRipNtscActive.Name = "xboxRipNtscActive";
			this.xboxRipNtscActive.Size = new System.Drawing.Size(24, 16);
			this.xboxRipNtscActive.TabIndex = 24;
			// 
			// xboxRipNtscRules
			// 
			this.xboxRipNtscRules.Location = new System.Drawing.Point(328, 200);
			this.xboxRipNtscRules.Name = "xboxRipNtscRules";
			this.xboxRipNtscRules.Size = new System.Drawing.Size(64, 24);
			this.xboxRipNtscRules.TabIndex = 23;
			this.xboxRipNtscRules.Text = "Rules";
			this.xboxRipNtscRules.Click += new System.EventHandler(this.xboxRipNtscRules_Click);
			// 
			// xboxRipNtscPath
			// 
			this.xboxRipNtscPath.Location = new System.Drawing.Point(104, 200);
			this.xboxRipNtscPath.Name = "xboxRipNtscPath";
			this.xboxRipNtscPath.Size = new System.Drawing.Size(112, 20);
			this.xboxRipNtscPath.TabIndex = 21;
			this.xboxRipNtscPath.Text = "";
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(16, 200);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(72, 24);
			this.label10.TabIndex = 107;
			this.label10.Text = "xbox.rip.ntsc";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// xboxRipPalActive
			// 
			this.xboxRipPalActive.Location = new System.Drawing.Point(424, 176);
			this.xboxRipPalActive.Name = "xboxRipPalActive";
			this.xboxRipPalActive.Size = new System.Drawing.Size(24, 16);
			this.xboxRipPalActive.TabIndex = 20;
			// 
			// xboxRipPalRules
			// 
			this.xboxRipPalRules.Location = new System.Drawing.Point(328, 168);
			this.xboxRipPalRules.Name = "xboxRipPalRules";
			this.xboxRipPalRules.Size = new System.Drawing.Size(64, 24);
			this.xboxRipPalRules.TabIndex = 19;
			this.xboxRipPalRules.Text = "Rules";
			this.xboxRipPalRules.Click += new System.EventHandler(this.xboxRipPalRules_Click);
			// 
			// xboxRipPalPath
			// 
			this.xboxRipPalPath.Location = new System.Drawing.Point(104, 168);
			this.xboxRipPalPath.Name = "xboxRipPalPath";
			this.xboxRipPalPath.Size = new System.Drawing.Size(112, 20);
			this.xboxRipPalPath.TabIndex = 17;
			this.xboxRipPalPath.Text = "";
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(16, 168);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(72, 24);
			this.label11.TabIndex = 103;
			this.label11.Text = "xbox.rip.pal";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// gamecubeActive
			// 
			this.gamecubeActive.Location = new System.Drawing.Point(424, 144);
			this.gamecubeActive.Name = "gamecubeActive";
			this.gamecubeActive.Size = new System.Drawing.Size(24, 16);
			this.gamecubeActive.TabIndex = 16;
			// 
			// gamecubeRules
			// 
			this.gamecubeRules.Location = new System.Drawing.Point(328, 136);
			this.gamecubeRules.Name = "gamecubeRules";
			this.gamecubeRules.Size = new System.Drawing.Size(64, 24);
			this.gamecubeRules.TabIndex = 15;
			this.gamecubeRules.Text = "Rules";
			this.gamecubeRules.Click += new System.EventHandler(this.gamecubeRules_Click);
			// 
			// gamecubePath
			// 
			this.gamecubePath.Location = new System.Drawing.Point(104, 136);
			this.gamecubePath.Name = "gamecubePath";
			this.gamecubePath.Size = new System.Drawing.Size(112, 20);
			this.gamecubePath.TabIndex = 13;
			this.gamecubePath.Text = "";
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(16, 136);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(72, 24);
			this.label12.TabIndex = 99;
			this.label12.Text = "game cube";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.xxxDivxBotname);
			this.tabPage1.Controls.Add(this.xxxBotname);
			this.tabPage1.Controls.Add(this.xxxDivxActive);
			this.tabPage1.Controls.Add(this.xxxDivxRules);
			this.tabPage1.Controls.Add(this.xxxActive);
			this.tabPage1.Controls.Add(this.xxxRules);
			this.tabPage1.Controls.Add(this.xxxDivxPath);
			this.tabPage1.Controls.Add(this.label8);
			this.tabPage1.Controls.Add(this.xxxPath);
			this.tabPage1.Controls.Add(this.label51);
			this.tabPage1.Controls.Add(this.mvidsBotname);
			this.tabPage1.Controls.Add(this.mvidsActive);
			this.tabPage1.Controls.Add(this.mvidsRules);
			this.tabPage1.Controls.Add(this.mvidsPath);
			this.tabPage1.Controls.Add(this.label32);
			this.tabPage1.Controls.Add(this.pdaBotname);
			this.tabPage1.Controls.Add(this.bookwareBotname);
			this.tabPage1.Controls.Add(this.samplecdBotname);
			this.tabPage1.Controls.Add(this.isoappsBotname);
			this.tabPage1.Controls.Add(this.gbaBotname);
			this.tabPage1.Controls.Add(this.doxBotname);
			this.tabPage1.Controls.Add(this.gamesBotname);
			this.tabPage1.Controls.Add(this.odayBotname);
			this.tabPage1.Controls.Add(this.label41);
			this.tabPage1.Controls.Add(this.pdaActive);
			this.tabPage1.Controls.Add(this.pdaRules);
			this.tabPage1.Controls.Add(this.pdaPath);
			this.tabPage1.Controls.Add(this.label22);
			this.tabPage1.Controls.Add(this.bookwareActive);
			this.tabPage1.Controls.Add(this.bookwareRules);
			this.tabPage1.Controls.Add(this.bookwarePath);
			this.tabPage1.Controls.Add(this.label23);
			this.tabPage1.Controls.Add(this.samplecdActive);
			this.tabPage1.Controls.Add(this.samplecdRules);
			this.tabPage1.Controls.Add(this.samplecdPath);
			this.tabPage1.Controls.Add(this.label24);
			this.tabPage1.Controls.Add(this.isoappsActive);
			this.tabPage1.Controls.Add(this.isoappsRules);
			this.tabPage1.Controls.Add(this.isoappsPath);
			this.tabPage1.Controls.Add(this.label25);
			this.tabPage1.Controls.Add(this.gbaActive);
			this.tabPage1.Controls.Add(this.gbaPath);
			this.tabPage1.Controls.Add(this.label13);
			this.tabPage1.Controls.Add(this.doxActive);
			this.tabPage1.Controls.Add(this.doxPath);
			this.tabPage1.Controls.Add(this.label14);
			this.tabPage1.Controls.Add(this.gamesActive);
			this.tabPage1.Controls.Add(this.gamesRules);
			this.tabPage1.Controls.Add(this.gamesPath);
			this.tabPage1.Controls.Add(this.label15);
			this.tabPage1.Controls.Add(this.odayActive);
			this.tabPage1.Controls.Add(this.odayRules);
			this.tabPage1.Controls.Add(this.odayPath);
			this.tabPage1.Controls.Add(this.label7);
			this.tabPage1.Controls.Add(this.label4);
			this.tabPage1.Controls.Add(this.label3);
			this.tabPage1.Controls.Add(this.label2);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(464, 446);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Iso";
			// 
			// xxxDivxBotname
			// 
			this.xxxDivxBotname.Location = new System.Drawing.Point(224, 360);
			this.xxxDivxBotname.Name = "xxxDivxBotname";
			this.xxxDivxBotname.Size = new System.Drawing.Size(96, 20);
			this.xxxDivxBotname.TabIndex = 164;
			this.xxxDivxBotname.Text = "";
			// 
			// xxxBotname
			// 
			this.xxxBotname.Location = new System.Drawing.Point(224, 328);
			this.xxxBotname.Name = "xxxBotname";
			this.xxxBotname.Size = new System.Drawing.Size(96, 20);
			this.xxxBotname.TabIndex = 160;
			this.xxxBotname.Text = "";
			// 
			// xxxDivxActive
			// 
			this.xxxDivxActive.Location = new System.Drawing.Point(424, 368);
			this.xxxDivxActive.Name = "xxxDivxActive";
			this.xxxDivxActive.Size = new System.Drawing.Size(24, 16);
			this.xxxDivxActive.TabIndex = 166;
			// 
			// xxxDivxRules
			// 
			this.xxxDivxRules.Location = new System.Drawing.Point(328, 360);
			this.xxxDivxRules.Name = "xxxDivxRules";
			this.xxxDivxRules.Size = new System.Drawing.Size(64, 24);
			this.xxxDivxRules.TabIndex = 165;
			this.xxxDivxRules.Text = "Rules";
			this.xxxDivxRules.Click += new System.EventHandler(this.xxxDivxRules_Click);
			// 
			// xxxActive
			// 
			this.xxxActive.Location = new System.Drawing.Point(424, 336);
			this.xxxActive.Name = "xxxActive";
			this.xxxActive.Size = new System.Drawing.Size(24, 16);
			this.xxxActive.TabIndex = 162;
			// 
			// xxxRules
			// 
			this.xxxRules.Location = new System.Drawing.Point(328, 328);
			this.xxxRules.Name = "xxxRules";
			this.xxxRules.Size = new System.Drawing.Size(64, 24);
			this.xxxRules.TabIndex = 161;
			this.xxxRules.Text = "Rules";
			this.xxxRules.Click += new System.EventHandler(this.xxxRules_Click);
			// 
			// xxxDivxPath
			// 
			this.xxxDivxPath.Location = new System.Drawing.Point(104, 360);
			this.xxxDivxPath.Name = "xxxDivxPath";
			this.xxxDivxPath.Size = new System.Drawing.Size(112, 20);
			this.xxxDivxPath.TabIndex = 163;
			this.xxxDivxPath.Text = "";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(16, 360);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(72, 24);
			this.label8.TabIndex = 158;
			this.label8.Text = "xxx.divx";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// xxxPath
			// 
			this.xxxPath.Location = new System.Drawing.Point(104, 328);
			this.xxxPath.Name = "xxxPath";
			this.xxxPath.Size = new System.Drawing.Size(112, 20);
			this.xxxPath.TabIndex = 159;
			this.xxxPath.Text = "";
			// 
			// label51
			// 
			this.label51.Location = new System.Drawing.Point(16, 328);
			this.label51.Name = "label51";
			this.label51.Size = new System.Drawing.Size(72, 24);
			this.label51.TabIndex = 157;
			this.label51.Text = "xxx";
			this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// mvidsBotname
			// 
			this.mvidsBotname.Location = new System.Drawing.Point(224, 296);
			this.mvidsBotname.Name = "mvidsBotname";
			this.mvidsBotname.Size = new System.Drawing.Size(96, 20);
			this.mvidsBotname.TabIndex = 80;
			this.mvidsBotname.Text = "";
			// 
			// mvidsActive
			// 
			this.mvidsActive.Location = new System.Drawing.Point(424, 304);
			this.mvidsActive.Name = "mvidsActive";
			this.mvidsActive.Size = new System.Drawing.Size(24, 16);
			this.mvidsActive.TabIndex = 82;
			// 
			// mvidsRules
			// 
			this.mvidsRules.Location = new System.Drawing.Point(328, 296);
			this.mvidsRules.Name = "mvidsRules";
			this.mvidsRules.Size = new System.Drawing.Size(64, 24);
			this.mvidsRules.TabIndex = 81;
			this.mvidsRules.Text = "Rules";
			this.mvidsRules.Click += new System.EventHandler(this.mvidsRules_Click);
			// 
			// mvidsPath
			// 
			this.mvidsPath.Location = new System.Drawing.Point(104, 296);
			this.mvidsPath.Name = "mvidsPath";
			this.mvidsPath.Size = new System.Drawing.Size(112, 20);
			this.mvidsPath.TabIndex = 79;
			this.mvidsPath.Text = "";
			// 
			// label32
			// 
			this.label32.Location = new System.Drawing.Point(16, 296);
			this.label32.Name = "label32";
			this.label32.Size = new System.Drawing.Size(72, 24);
			this.label32.TabIndex = 156;
			this.label32.Text = "mvids";
			this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// pdaBotname
			// 
			this.pdaBotname.Enabled = false;
			this.pdaBotname.Location = new System.Drawing.Point(224, 264);
			this.pdaBotname.Name = "pdaBotname";
			this.pdaBotname.Size = new System.Drawing.Size(96, 20);
			this.pdaBotname.TabIndex = 76;
			this.pdaBotname.Text = "";
			// 
			// bookwareBotname
			// 
			this.bookwareBotname.Enabled = false;
			this.bookwareBotname.Location = new System.Drawing.Point(224, 232);
			this.bookwareBotname.Name = "bookwareBotname";
			this.bookwareBotname.Size = new System.Drawing.Size(96, 20);
			this.bookwareBotname.TabIndex = 72;
			this.bookwareBotname.Text = "";
			// 
			// samplecdBotname
			// 
			this.samplecdBotname.Location = new System.Drawing.Point(224, 200);
			this.samplecdBotname.Name = "samplecdBotname";
			this.samplecdBotname.Size = new System.Drawing.Size(96, 20);
			this.samplecdBotname.TabIndex = 68;
			this.samplecdBotname.Text = "";
			// 
			// isoappsBotname
			// 
			this.isoappsBotname.Location = new System.Drawing.Point(224, 168);
			this.isoappsBotname.Name = "isoappsBotname";
			this.isoappsBotname.Size = new System.Drawing.Size(96, 20);
			this.isoappsBotname.TabIndex = 64;
			this.isoappsBotname.Text = "";
			// 
			// gbaBotname
			// 
			this.gbaBotname.Enabled = false;
			this.gbaBotname.Location = new System.Drawing.Point(224, 136);
			this.gbaBotname.Name = "gbaBotname";
			this.gbaBotname.Size = new System.Drawing.Size(96, 20);
			this.gbaBotname.TabIndex = 61;
			this.gbaBotname.Text = "";
			// 
			// doxBotname
			// 
			this.doxBotname.Enabled = false;
			this.doxBotname.Location = new System.Drawing.Point(224, 104);
			this.doxBotname.Name = "doxBotname";
			this.doxBotname.Size = new System.Drawing.Size(96, 20);
			this.doxBotname.TabIndex = 58;
			this.doxBotname.Text = "";
			// 
			// gamesBotname
			// 
			this.gamesBotname.Location = new System.Drawing.Point(224, 72);
			this.gamesBotname.Name = "gamesBotname";
			this.gamesBotname.Size = new System.Drawing.Size(96, 20);
			this.gamesBotname.TabIndex = 54;
			this.gamesBotname.Text = "";
			// 
			// odayBotname
			// 
			this.odayBotname.Enabled = false;
			this.odayBotname.Location = new System.Drawing.Point(224, 40);
			this.odayBotname.Name = "odayBotname";
			this.odayBotname.Size = new System.Drawing.Size(96, 20);
			this.odayBotname.TabIndex = 50;
			this.odayBotname.Text = "";
			// 
			// label41
			// 
			this.label41.Location = new System.Drawing.Point(232, 16);
			this.label41.Name = "label41";
			this.label41.Size = new System.Drawing.Size(72, 16);
			this.label41.TabIndex = 88;
			this.label41.Text = "Botname";
			this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// pdaActive
			// 
			this.pdaActive.Enabled = false;
			this.pdaActive.Location = new System.Drawing.Point(424, 272);
			this.pdaActive.Name = "pdaActive";
			this.pdaActive.Size = new System.Drawing.Size(24, 16);
			this.pdaActive.TabIndex = 78;
			// 
			// pdaRules
			// 
			this.pdaRules.Location = new System.Drawing.Point(328, 264);
			this.pdaRules.Name = "pdaRules";
			this.pdaRules.Size = new System.Drawing.Size(64, 24);
			this.pdaRules.TabIndex = 77;
			this.pdaRules.Text = "Rules";
			this.pdaRules.Click += new System.EventHandler(this.pdaRules_Click);
			// 
			// pdaPath
			// 
			this.pdaPath.Enabled = false;
			this.pdaPath.Location = new System.Drawing.Point(104, 264);
			this.pdaPath.Name = "pdaPath";
			this.pdaPath.Size = new System.Drawing.Size(112, 20);
			this.pdaPath.TabIndex = 75;
			this.pdaPath.Text = "";
			// 
			// label22
			// 
			this.label22.Location = new System.Drawing.Point(16, 264);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(72, 24);
			this.label22.TabIndex = 84;
			this.label22.Text = "pda";
			this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// bookwareActive
			// 
			this.bookwareActive.Enabled = false;
			this.bookwareActive.Location = new System.Drawing.Point(424, 240);
			this.bookwareActive.Name = "bookwareActive";
			this.bookwareActive.Size = new System.Drawing.Size(24, 16);
			this.bookwareActive.TabIndex = 74;
			// 
			// bookwareRules
			// 
			this.bookwareRules.Location = new System.Drawing.Point(328, 232);
			this.bookwareRules.Name = "bookwareRules";
			this.bookwareRules.Size = new System.Drawing.Size(64, 24);
			this.bookwareRules.TabIndex = 73;
			this.bookwareRules.Text = "Rules";
			this.bookwareRules.Click += new System.EventHandler(this.bookwareRules_Click);
			// 
			// bookwarePath
			// 
			this.bookwarePath.Enabled = false;
			this.bookwarePath.Location = new System.Drawing.Point(104, 232);
			this.bookwarePath.Name = "bookwarePath";
			this.bookwarePath.Size = new System.Drawing.Size(112, 20);
			this.bookwarePath.TabIndex = 71;
			this.bookwarePath.Text = "";
			// 
			// label23
			// 
			this.label23.Location = new System.Drawing.Point(16, 232);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(72, 24);
			this.label23.TabIndex = 80;
			this.label23.Text = "bookware";
			this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// samplecdActive
			// 
			this.samplecdActive.Location = new System.Drawing.Point(424, 208);
			this.samplecdActive.Name = "samplecdActive";
			this.samplecdActive.Size = new System.Drawing.Size(24, 16);
			this.samplecdActive.TabIndex = 70;
			// 
			// samplecdRules
			// 
			this.samplecdRules.Location = new System.Drawing.Point(328, 200);
			this.samplecdRules.Name = "samplecdRules";
			this.samplecdRules.Size = new System.Drawing.Size(64, 24);
			this.samplecdRules.TabIndex = 69;
			this.samplecdRules.Text = "Rules";
			this.samplecdRules.Click += new System.EventHandler(this.samplecdRules_Click);
			// 
			// samplecdPath
			// 
			this.samplecdPath.Location = new System.Drawing.Point(104, 200);
			this.samplecdPath.Name = "samplecdPath";
			this.samplecdPath.Size = new System.Drawing.Size(112, 20);
			this.samplecdPath.TabIndex = 67;
			this.samplecdPath.Text = "";
			// 
			// label24
			// 
			this.label24.Location = new System.Drawing.Point(16, 200);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(72, 24);
			this.label24.TabIndex = 76;
			this.label24.Text = "samplecd";
			this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// isoappsActive
			// 
			this.isoappsActive.Location = new System.Drawing.Point(424, 176);
			this.isoappsActive.Name = "isoappsActive";
			this.isoappsActive.Size = new System.Drawing.Size(24, 16);
			this.isoappsActive.TabIndex = 66;
			// 
			// isoappsRules
			// 
			this.isoappsRules.Location = new System.Drawing.Point(328, 168);
			this.isoappsRules.Name = "isoappsRules";
			this.isoappsRules.Size = new System.Drawing.Size(64, 24);
			this.isoappsRules.TabIndex = 65;
			this.isoappsRules.Text = "Rules";
			this.isoappsRules.Click += new System.EventHandler(this.isoappsRules_Click);
			// 
			// isoappsPath
			// 
			this.isoappsPath.Location = new System.Drawing.Point(104, 168);
			this.isoappsPath.Name = "isoappsPath";
			this.isoappsPath.Size = new System.Drawing.Size(112, 20);
			this.isoappsPath.TabIndex = 63;
			this.isoappsPath.Text = "";
			// 
			// label25
			// 
			this.label25.Location = new System.Drawing.Point(16, 168);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(72, 24);
			this.label25.TabIndex = 72;
			this.label25.Text = "isoapps";
			this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// gbaActive
			// 
			this.gbaActive.Enabled = false;
			this.gbaActive.Location = new System.Drawing.Point(424, 144);
			this.gbaActive.Name = "gbaActive";
			this.gbaActive.Size = new System.Drawing.Size(24, 16);
			this.gbaActive.TabIndex = 62;
			// 
			// gbaPath
			// 
			this.gbaPath.Enabled = false;
			this.gbaPath.Location = new System.Drawing.Point(104, 136);
			this.gbaPath.Name = "gbaPath";
			this.gbaPath.Size = new System.Drawing.Size(112, 20);
			this.gbaPath.TabIndex = 60;
			this.gbaPath.Text = "";
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(16, 136);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(72, 24);
			this.label13.TabIndex = 68;
			this.label13.Text = "gba";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// doxActive
			// 
			this.doxActive.Enabled = false;
			this.doxActive.Location = new System.Drawing.Point(424, 112);
			this.doxActive.Name = "doxActive";
			this.doxActive.Size = new System.Drawing.Size(24, 16);
			this.doxActive.TabIndex = 59;
			// 
			// doxPath
			// 
			this.doxPath.Enabled = false;
			this.doxPath.Location = new System.Drawing.Point(104, 104);
			this.doxPath.Name = "doxPath";
			this.doxPath.Size = new System.Drawing.Size(112, 20);
			this.doxPath.TabIndex = 57;
			this.doxPath.Text = "";
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(16, 104);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(72, 24);
			this.label14.TabIndex = 64;
			this.label14.Text = "dox";
			this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// gamesActive
			// 
			this.gamesActive.Location = new System.Drawing.Point(424, 80);
			this.gamesActive.Name = "gamesActive";
			this.gamesActive.Size = new System.Drawing.Size(24, 16);
			this.gamesActive.TabIndex = 56;
			// 
			// gamesRules
			// 
			this.gamesRules.Location = new System.Drawing.Point(328, 72);
			this.gamesRules.Name = "gamesRules";
			this.gamesRules.Size = new System.Drawing.Size(64, 24);
			this.gamesRules.TabIndex = 55;
			this.gamesRules.Text = "Rules";
			this.gamesRules.Click += new System.EventHandler(this.gamesRules_Click);
			// 
			// gamesPath
			// 
			this.gamesPath.Location = new System.Drawing.Point(104, 72);
			this.gamesPath.Name = "gamesPath";
			this.gamesPath.Size = new System.Drawing.Size(112, 20);
			this.gamesPath.TabIndex = 53;
			this.gamesPath.Text = "";
			// 
			// label15
			// 
			this.label15.Location = new System.Drawing.Point(16, 72);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(72, 24);
			this.label15.TabIndex = 60;
			this.label15.Text = "games";
			this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// odayActive
			// 
			this.odayActive.Enabled = false;
			this.odayActive.Location = new System.Drawing.Point(424, 48);
			this.odayActive.Name = "odayActive";
			this.odayActive.Size = new System.Drawing.Size(24, 16);
			this.odayActive.TabIndex = 52;
			// 
			// odayRules
			// 
			this.odayRules.Location = new System.Drawing.Point(328, 40);
			this.odayRules.Name = "odayRules";
			this.odayRules.Size = new System.Drawing.Size(64, 24);
			this.odayRules.TabIndex = 51;
			this.odayRules.Text = "Rules";
			this.odayRules.Click += new System.EventHandler(this.odayRules_Click);
			// 
			// odayPath
			// 
			this.odayPath.Enabled = false;
			this.odayPath.Location = new System.Drawing.Point(104, 40);
			this.odayPath.Name = "odayPath";
			this.odayPath.Size = new System.Drawing.Size(112, 20);
			this.odayPath.TabIndex = 49;
			this.odayPath.Text = "";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(16, 40);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(72, 24);
			this.label7.TabIndex = 20;
			this.label7.Text = "0day";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(408, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(48, 16);
			this.label4.TabIndex = 7;
			this.label4.Text = "Active";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(320, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(72, 16);
			this.label3.TabIndex = 6;
			this.label3.Text = "Rules";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(120, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(72, 16);
			this.label2.TabIndex = 5;
			this.label2.Text = "Path";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 16);
			this.label1.TabIndex = 4;
			this.label1.Text = "Section";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.tvDivxBotname);
			this.tabPage2.Controls.Add(this.tvDivxActive);
			this.tabPage2.Controls.Add(this.tvDivxRules);
			this.tabPage2.Controls.Add(this.tvDivxPath);
			this.tabPage2.Controls.Add(this.label5);
			this.tabPage2.Controls.Add(this.dvdrPalDvdscrRules);
			this.tabPage2.Controls.Add(this.dvdrPalRules);
			this.tabPage2.Controls.Add(this.dvdrNtscDvdscrBotname);
			this.tabPage2.Controls.Add(this.dvdrNtscBotname);
			this.tabPage2.Controls.Add(this.dvdrPalDvdscrBotname);
			this.tabPage2.Controls.Add(this.dvdrPalBotname);
			this.tabPage2.Controls.Add(this.divxDvdscrBotname);
			this.tabPage2.Controls.Add(this.divxDvdripBotname);
			this.tabPage2.Controls.Add(this.svcdDvdscrBotname);
			this.tabPage2.Controls.Add(this.svcdBotname);
			this.tabPage2.Controls.Add(this.vcdBotname);
			this.tabPage2.Controls.Add(this.tvDvdrBotname);
			this.tabPage2.Controls.Add(this.tvBotname);
			this.tabPage2.Controls.Add(this.label21);
			this.tabPage2.Controls.Add(this.dvdrNtscDvdscrActive);
			this.tabPage2.Controls.Add(this.dvdrNtscDvdscrRules);
			this.tabPage2.Controls.Add(this.dvdrNtscActive);
			this.tabPage2.Controls.Add(this.dvdrNtscRules);
			this.tabPage2.Controls.Add(this.dvdrPalDvdscrActive);
			this.tabPage2.Controls.Add(this.dvdrPalActive);
			this.tabPage2.Controls.Add(this.divxDvdscrActive);
			this.tabPage2.Controls.Add(this.divxDvdscrRules);
			this.tabPage2.Controls.Add(this.divxDvdripActive);
			this.tabPage2.Controls.Add(this.divxDvdripRules);
			this.tabPage2.Controls.Add(this.svcdDvdscrActive);
			this.tabPage2.Controls.Add(this.svcdDvdscrRules);
			this.tabPage2.Controls.Add(this.svcdActive);
			this.tabPage2.Controls.Add(this.svcdRules);
			this.tabPage2.Controls.Add(this.vcdActive);
			this.tabPage2.Controls.Add(this.vcdRules);
			this.tabPage2.Controls.Add(this.tvDvdrActive);
			this.tabPage2.Controls.Add(this.tvDvdrRules);
			this.tabPage2.Controls.Add(this.tvActive);
			this.tabPage2.Controls.Add(this.tvRules);
			this.tabPage2.Controls.Add(this.label26);
			this.tabPage2.Controls.Add(this.label42);
			this.tabPage2.Controls.Add(this.dvdrNtscDvdscrPath);
			this.tabPage2.Controls.Add(this.label40);
			this.tabPage2.Controls.Add(this.dvdrNtscPath);
			this.tabPage2.Controls.Add(this.label29);
			this.tabPage2.Controls.Add(this.dvdrPalDvdscrPath);
			this.tabPage2.Controls.Add(this.label30);
			this.tabPage2.Controls.Add(this.dvdrPalPath);
			this.tabPage2.Controls.Add(this.label31);
			this.tabPage2.Controls.Add(this.divxDvdscrPath);
			this.tabPage2.Controls.Add(this.label33);
			this.tabPage2.Controls.Add(this.divxDvdripPath);
			this.tabPage2.Controls.Add(this.label34);
			this.tabPage2.Controls.Add(this.svcdDvdscrPath);
			this.tabPage2.Controls.Add(this.label35);
			this.tabPage2.Controls.Add(this.svcdPath);
			this.tabPage2.Controls.Add(this.label36);
			this.tabPage2.Controls.Add(this.vcdPath);
			this.tabPage2.Controls.Add(this.label37);
			this.tabPage2.Controls.Add(this.tvDvdrPath);
			this.tabPage2.Controls.Add(this.label38);
			this.tabPage2.Controls.Add(this.tvPath);
			this.tabPage2.Controls.Add(this.label39);
			this.tabPage2.Controls.Add(this.label27);
			this.tabPage2.Controls.Add(this.label28);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Size = new System.Drawing.Size(464, 446);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Movies";
			// 
			// tvDivxBotname
			// 
			this.tvDivxBotname.Location = new System.Drawing.Point(224, 72);
			this.tvDivxBotname.Name = "tvDivxBotname";
			this.tvDivxBotname.Size = new System.Drawing.Size(96, 20);
			this.tvDivxBotname.TabIndex = 88;
			this.tvDivxBotname.Text = "";
			// 
			// tvDivxActive
			// 
			this.tvDivxActive.Location = new System.Drawing.Point(424, 80);
			this.tvDivxActive.Name = "tvDivxActive";
			this.tvDivxActive.Size = new System.Drawing.Size(24, 16);
			this.tvDivxActive.TabIndex = 90;
			// 
			// tvDivxRules
			// 
			this.tvDivxRules.Location = new System.Drawing.Point(328, 72);
			this.tvDivxRules.Name = "tvDivxRules";
			this.tvDivxRules.Size = new System.Drawing.Size(64, 24);
			this.tvDivxRules.TabIndex = 89;
			this.tvDivxRules.Text = "Rules";
			this.tvDivxRules.Click += new System.EventHandler(this.tvDivxRules_Click);
			// 
			// tvDivxPath
			// 
			this.tvDivxPath.Location = new System.Drawing.Point(104, 72);
			this.tvDivxPath.Name = "tvDivxPath";
			this.tvDivxPath.Size = new System.Drawing.Size(112, 20);
			this.tvDivxPath.TabIndex = 87;
			this.tvDivxPath.Text = "";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(16, 72);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(72, 24);
			this.label5.TabIndex = 162;
			this.label5.Text = "tv.divx";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// dvdrPalDvdscrRules
			// 
			this.dvdrPalDvdscrRules.Location = new System.Drawing.Point(328, 328);
			this.dvdrPalDvdscrRules.Name = "dvdrPalDvdscrRules";
			this.dvdrPalDvdscrRules.Size = new System.Drawing.Size(64, 24);
			this.dvdrPalDvdscrRules.TabIndex = 121;
			this.dvdrPalDvdscrRules.Text = "Rules";
			this.dvdrPalDvdscrRules.Click += new System.EventHandler(this.dvdrPalDvdscrRules_Click);
			// 
			// dvdrPalRules
			// 
			this.dvdrPalRules.Location = new System.Drawing.Point(328, 296);
			this.dvdrPalRules.Name = "dvdrPalRules";
			this.dvdrPalRules.Size = new System.Drawing.Size(64, 24);
			this.dvdrPalRules.TabIndex = 117;
			this.dvdrPalRules.Text = "Rules";
			this.dvdrPalRules.Click += new System.EventHandler(this.dvdrPalRules_Click);
			// 
			// dvdrNtscDvdscrBotname
			// 
			this.dvdrNtscDvdscrBotname.Location = new System.Drawing.Point(224, 392);
			this.dvdrNtscDvdscrBotname.Name = "dvdrNtscDvdscrBotname";
			this.dvdrNtscDvdscrBotname.Size = new System.Drawing.Size(96, 20);
			this.dvdrNtscDvdscrBotname.TabIndex = 128;
			this.dvdrNtscDvdscrBotname.Text = "";
			// 
			// dvdrNtscBotname
			// 
			this.dvdrNtscBotname.Location = new System.Drawing.Point(224, 360);
			this.dvdrNtscBotname.Name = "dvdrNtscBotname";
			this.dvdrNtscBotname.Size = new System.Drawing.Size(96, 20);
			this.dvdrNtscBotname.TabIndex = 124;
			this.dvdrNtscBotname.Text = "";
			// 
			// dvdrPalDvdscrBotname
			// 
			this.dvdrPalDvdscrBotname.Location = new System.Drawing.Point(224, 328);
			this.dvdrPalDvdscrBotname.Name = "dvdrPalDvdscrBotname";
			this.dvdrPalDvdscrBotname.Size = new System.Drawing.Size(96, 20);
			this.dvdrPalDvdscrBotname.TabIndex = 120;
			this.dvdrPalDvdscrBotname.Text = "";
			// 
			// dvdrPalBotname
			// 
			this.dvdrPalBotname.Location = new System.Drawing.Point(224, 296);
			this.dvdrPalBotname.Name = "dvdrPalBotname";
			this.dvdrPalBotname.Size = new System.Drawing.Size(96, 20);
			this.dvdrPalBotname.TabIndex = 116;
			this.dvdrPalBotname.Text = "";
			// 
			// divxDvdscrBotname
			// 
			this.divxDvdscrBotname.Location = new System.Drawing.Point(224, 264);
			this.divxDvdscrBotname.Name = "divxDvdscrBotname";
			this.divxDvdscrBotname.Size = new System.Drawing.Size(96, 20);
			this.divxDvdscrBotname.TabIndex = 112;
			this.divxDvdscrBotname.Text = "";
			// 
			// divxDvdripBotname
			// 
			this.divxDvdripBotname.Location = new System.Drawing.Point(224, 232);
			this.divxDvdripBotname.Name = "divxDvdripBotname";
			this.divxDvdripBotname.Size = new System.Drawing.Size(96, 20);
			this.divxDvdripBotname.TabIndex = 108;
			this.divxDvdripBotname.Text = "";
			// 
			// svcdDvdscrBotname
			// 
			this.svcdDvdscrBotname.Location = new System.Drawing.Point(224, 200);
			this.svcdDvdscrBotname.Name = "svcdDvdscrBotname";
			this.svcdDvdscrBotname.Size = new System.Drawing.Size(96, 20);
			this.svcdDvdscrBotname.TabIndex = 104;
			this.svcdDvdscrBotname.Text = "";
			// 
			// svcdBotname
			// 
			this.svcdBotname.Location = new System.Drawing.Point(224, 168);
			this.svcdBotname.Name = "svcdBotname";
			this.svcdBotname.Size = new System.Drawing.Size(96, 20);
			this.svcdBotname.TabIndex = 100;
			this.svcdBotname.Text = "";
			// 
			// vcdBotname
			// 
			this.vcdBotname.Location = new System.Drawing.Point(224, 136);
			this.vcdBotname.Name = "vcdBotname";
			this.vcdBotname.Size = new System.Drawing.Size(96, 20);
			this.vcdBotname.TabIndex = 96;
			this.vcdBotname.Text = "";
			// 
			// tvDvdrBotname
			// 
			this.tvDvdrBotname.Location = new System.Drawing.Point(224, 104);
			this.tvDvdrBotname.Name = "tvDvdrBotname";
			this.tvDvdrBotname.Size = new System.Drawing.Size(96, 20);
			this.tvDvdrBotname.TabIndex = 92;
			this.tvDvdrBotname.Text = "";
			// 
			// tvBotname
			// 
			this.tvBotname.Location = new System.Drawing.Point(224, 40);
			this.tvBotname.Name = "tvBotname";
			this.tvBotname.Size = new System.Drawing.Size(96, 20);
			this.tvBotname.TabIndex = 84;
			this.tvBotname.Text = "";
			// 
			// label21
			// 
			this.label21.Location = new System.Drawing.Point(232, 16);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(72, 16);
			this.label21.TabIndex = 144;
			this.label21.Text = "Botname";
			this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// dvdrNtscDvdscrActive
			// 
			this.dvdrNtscDvdscrActive.Location = new System.Drawing.Point(424, 400);
			this.dvdrNtscDvdscrActive.Name = "dvdrNtscDvdscrActive";
			this.dvdrNtscDvdscrActive.Size = new System.Drawing.Size(24, 16);
			this.dvdrNtscDvdscrActive.TabIndex = 130;
			// 
			// dvdrNtscDvdscrRules
			// 
			this.dvdrNtscDvdscrRules.Location = new System.Drawing.Point(328, 392);
			this.dvdrNtscDvdscrRules.Name = "dvdrNtscDvdscrRules";
			this.dvdrNtscDvdscrRules.Size = new System.Drawing.Size(64, 24);
			this.dvdrNtscDvdscrRules.TabIndex = 129;
			this.dvdrNtscDvdscrRules.Text = "Rules";
			this.dvdrNtscDvdscrRules.Click += new System.EventHandler(this.dvdrNtscDvdscrRules_Click);
			// 
			// dvdrNtscActive
			// 
			this.dvdrNtscActive.Location = new System.Drawing.Point(424, 368);
			this.dvdrNtscActive.Name = "dvdrNtscActive";
			this.dvdrNtscActive.Size = new System.Drawing.Size(24, 16);
			this.dvdrNtscActive.TabIndex = 126;
			// 
			// dvdrNtscRules
			// 
			this.dvdrNtscRules.Location = new System.Drawing.Point(328, 360);
			this.dvdrNtscRules.Name = "dvdrNtscRules";
			this.dvdrNtscRules.Size = new System.Drawing.Size(64, 24);
			this.dvdrNtscRules.TabIndex = 125;
			this.dvdrNtscRules.Text = "Rules";
			this.dvdrNtscRules.Click += new System.EventHandler(this.dvdrNtscRules_Click);
			// 
			// dvdrPalDvdscrActive
			// 
			this.dvdrPalDvdscrActive.Location = new System.Drawing.Point(424, 336);
			this.dvdrPalDvdscrActive.Name = "dvdrPalDvdscrActive";
			this.dvdrPalDvdscrActive.Size = new System.Drawing.Size(24, 16);
			this.dvdrPalDvdscrActive.TabIndex = 122;
			// 
			// dvdrPalActive
			// 
			this.dvdrPalActive.Location = new System.Drawing.Point(424, 304);
			this.dvdrPalActive.Name = "dvdrPalActive";
			this.dvdrPalActive.Size = new System.Drawing.Size(24, 16);
			this.dvdrPalActive.TabIndex = 118;
			// 
			// divxDvdscrActive
			// 
			this.divxDvdscrActive.Location = new System.Drawing.Point(424, 272);
			this.divxDvdscrActive.Name = "divxDvdscrActive";
			this.divxDvdscrActive.Size = new System.Drawing.Size(24, 16);
			this.divxDvdscrActive.TabIndex = 114;
			// 
			// divxDvdscrRules
			// 
			this.divxDvdscrRules.Location = new System.Drawing.Point(328, 264);
			this.divxDvdscrRules.Name = "divxDvdscrRules";
			this.divxDvdscrRules.Size = new System.Drawing.Size(64, 24);
			this.divxDvdscrRules.TabIndex = 113;
			this.divxDvdscrRules.Text = "Rules";
			this.divxDvdscrRules.Click += new System.EventHandler(this.divxDvdscrRules_Click);
			// 
			// divxDvdripActive
			// 
			this.divxDvdripActive.Location = new System.Drawing.Point(424, 240);
			this.divxDvdripActive.Name = "divxDvdripActive";
			this.divxDvdripActive.Size = new System.Drawing.Size(24, 16);
			this.divxDvdripActive.TabIndex = 110;
			// 
			// divxDvdripRules
			// 
			this.divxDvdripRules.Location = new System.Drawing.Point(328, 232);
			this.divxDvdripRules.Name = "divxDvdripRules";
			this.divxDvdripRules.Size = new System.Drawing.Size(64, 24);
			this.divxDvdripRules.TabIndex = 109;
			this.divxDvdripRules.Text = "Rules";
			this.divxDvdripRules.Click += new System.EventHandler(this.divxDvdripRules_Click);
			// 
			// svcdDvdscrActive
			// 
			this.svcdDvdscrActive.Location = new System.Drawing.Point(424, 208);
			this.svcdDvdscrActive.Name = "svcdDvdscrActive";
			this.svcdDvdscrActive.Size = new System.Drawing.Size(24, 16);
			this.svcdDvdscrActive.TabIndex = 106;
			// 
			// svcdDvdscrRules
			// 
			this.svcdDvdscrRules.Location = new System.Drawing.Point(328, 200);
			this.svcdDvdscrRules.Name = "svcdDvdscrRules";
			this.svcdDvdscrRules.Size = new System.Drawing.Size(64, 24);
			this.svcdDvdscrRules.TabIndex = 104;
			this.svcdDvdscrRules.Text = "Rules";
			this.svcdDvdscrRules.Click += new System.EventHandler(this.svcdDvdscrRules_Click);
			// 
			// svcdActive
			// 
			this.svcdActive.Location = new System.Drawing.Point(424, 176);
			this.svcdActive.Name = "svcdActive";
			this.svcdActive.Size = new System.Drawing.Size(24, 16);
			this.svcdActive.TabIndex = 102;
			// 
			// svcdRules
			// 
			this.svcdRules.Location = new System.Drawing.Point(328, 168);
			this.svcdRules.Name = "svcdRules";
			this.svcdRules.Size = new System.Drawing.Size(64, 24);
			this.svcdRules.TabIndex = 101;
			this.svcdRules.Text = "Rules";
			this.svcdRules.Click += new System.EventHandler(this.svcdRules_Click);
			// 
			// vcdActive
			// 
			this.vcdActive.Location = new System.Drawing.Point(424, 144);
			this.vcdActive.Name = "vcdActive";
			this.vcdActive.Size = new System.Drawing.Size(24, 16);
			this.vcdActive.TabIndex = 98;
			// 
			// vcdRules
			// 
			this.vcdRules.Location = new System.Drawing.Point(328, 136);
			this.vcdRules.Name = "vcdRules";
			this.vcdRules.Size = new System.Drawing.Size(64, 24);
			this.vcdRules.TabIndex = 97;
			this.vcdRules.Text = "Rules";
			this.vcdRules.Click += new System.EventHandler(this.vcdRules_Click);
			// 
			// tvDvdrActive
			// 
			this.tvDvdrActive.Location = new System.Drawing.Point(424, 112);
			this.tvDvdrActive.Name = "tvDvdrActive";
			this.tvDvdrActive.Size = new System.Drawing.Size(24, 16);
			this.tvDvdrActive.TabIndex = 94;
			// 
			// tvDvdrRules
			// 
			this.tvDvdrRules.Location = new System.Drawing.Point(328, 104);
			this.tvDvdrRules.Name = "tvDvdrRules";
			this.tvDvdrRules.Size = new System.Drawing.Size(64, 24);
			this.tvDvdrRules.TabIndex = 93;
			this.tvDvdrRules.Text = "Rules";
			this.tvDvdrRules.Click += new System.EventHandler(this.tvDvdrRules_Click);
			// 
			// tvActive
			// 
			this.tvActive.Location = new System.Drawing.Point(424, 48);
			this.tvActive.Name = "tvActive";
			this.tvActive.Size = new System.Drawing.Size(24, 16);
			this.tvActive.TabIndex = 86;
			// 
			// tvRules
			// 
			this.tvRules.Location = new System.Drawing.Point(328, 40);
			this.tvRules.Name = "tvRules";
			this.tvRules.Size = new System.Drawing.Size(64, 24);
			this.tvRules.TabIndex = 85;
			this.tvRules.Text = "Rules";
			this.tvRules.Click += new System.EventHandler(this.tvRules_Click);
			// 
			// label26
			// 
			this.label26.Location = new System.Drawing.Point(408, 16);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(48, 16);
			this.label26.TabIndex = 111;
			this.label26.Text = "Active";
			this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label42
			// 
			this.label42.Location = new System.Drawing.Point(320, 16);
			this.label42.Name = "label42";
			this.label42.Size = new System.Drawing.Size(72, 16);
			this.label42.TabIndex = 110;
			this.label42.Text = "Rules";
			this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// dvdrNtscDvdscrPath
			// 
			this.dvdrNtscDvdscrPath.Location = new System.Drawing.Point(104, 392);
			this.dvdrNtscDvdscrPath.Name = "dvdrNtscDvdscrPath";
			this.dvdrNtscDvdscrPath.Size = new System.Drawing.Size(112, 20);
			this.dvdrNtscDvdscrPath.TabIndex = 127;
			this.dvdrNtscDvdscrPath.Text = "";
			// 
			// label40
			// 
			this.label40.Location = new System.Drawing.Point(8, 392);
			this.label40.Name = "label40";
			this.label40.Size = new System.Drawing.Size(96, 24);
			this.label40.TabIndex = 108;
			this.label40.Text = "dvdr.ntsc.dvdscr";
			this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// dvdrNtscPath
			// 
			this.dvdrNtscPath.Location = new System.Drawing.Point(104, 360);
			this.dvdrNtscPath.Name = "dvdrNtscPath";
			this.dvdrNtscPath.Size = new System.Drawing.Size(112, 20);
			this.dvdrNtscPath.TabIndex = 123;
			this.dvdrNtscPath.Text = "";
			// 
			// label29
			// 
			this.label29.Location = new System.Drawing.Point(16, 360);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(72, 24);
			this.label29.TabIndex = 104;
			this.label29.Text = "dvdr.ntsc";
			this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// dvdrPalDvdscrPath
			// 
			this.dvdrPalDvdscrPath.Location = new System.Drawing.Point(104, 328);
			this.dvdrPalDvdscrPath.Name = "dvdrPalDvdscrPath";
			this.dvdrPalDvdscrPath.Size = new System.Drawing.Size(112, 20);
			this.dvdrPalDvdscrPath.TabIndex = 119;
			this.dvdrPalDvdscrPath.Text = "";
			// 
			// label30
			// 
			this.label30.Location = new System.Drawing.Point(8, 328);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(88, 24);
			this.label30.TabIndex = 100;
			this.label30.Text = "dvdr.pal.dvdscr";
			this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// dvdrPalPath
			// 
			this.dvdrPalPath.Location = new System.Drawing.Point(104, 296);
			this.dvdrPalPath.Name = "dvdrPalPath";
			this.dvdrPalPath.Size = new System.Drawing.Size(112, 20);
			this.dvdrPalPath.TabIndex = 115;
			this.dvdrPalPath.Text = "";
			// 
			// label31
			// 
			this.label31.Location = new System.Drawing.Point(16, 296);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(72, 24);
			this.label31.TabIndex = 96;
			this.label31.Text = "dvdr.pal";
			this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// divxDvdscrPath
			// 
			this.divxDvdscrPath.Location = new System.Drawing.Point(104, 264);
			this.divxDvdscrPath.Name = "divxDvdscrPath";
			this.divxDvdscrPath.Size = new System.Drawing.Size(112, 20);
			this.divxDvdscrPath.TabIndex = 111;
			this.divxDvdscrPath.Text = "";
			// 
			// label33
			// 
			this.label33.Location = new System.Drawing.Point(8, 264);
			this.label33.Name = "label33";
			this.label33.Size = new System.Drawing.Size(88, 24);
			this.label33.TabIndex = 88;
			this.label33.Text = "divx.dvdscr";
			this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// divxDvdripPath
			// 
			this.divxDvdripPath.Location = new System.Drawing.Point(104, 232);
			this.divxDvdripPath.Name = "divxDvdripPath";
			this.divxDvdripPath.Size = new System.Drawing.Size(112, 20);
			this.divxDvdripPath.TabIndex = 107;
			this.divxDvdripPath.Text = "";
			// 
			// label34
			// 
			this.label34.Location = new System.Drawing.Point(8, 232);
			this.label34.Name = "label34";
			this.label34.Size = new System.Drawing.Size(88, 24);
			this.label34.TabIndex = 84;
			this.label34.Text = "divx";
			this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// svcdDvdscrPath
			// 
			this.svcdDvdscrPath.Location = new System.Drawing.Point(104, 200);
			this.svcdDvdscrPath.Name = "svcdDvdscrPath";
			this.svcdDvdscrPath.Size = new System.Drawing.Size(112, 20);
			this.svcdDvdscrPath.TabIndex = 103;
			this.svcdDvdscrPath.Text = "";
			// 
			// label35
			// 
			this.label35.Location = new System.Drawing.Point(16, 200);
			this.label35.Name = "label35";
			this.label35.Size = new System.Drawing.Size(72, 24);
			this.label35.TabIndex = 80;
			this.label35.Text = "svcd.dvdscr";
			this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// svcdPath
			// 
			this.svcdPath.Location = new System.Drawing.Point(104, 168);
			this.svcdPath.Name = "svcdPath";
			this.svcdPath.Size = new System.Drawing.Size(112, 20);
			this.svcdPath.TabIndex = 99;
			this.svcdPath.Text = "";
			// 
			// label36
			// 
			this.label36.Location = new System.Drawing.Point(16, 168);
			this.label36.Name = "label36";
			this.label36.Size = new System.Drawing.Size(72, 24);
			this.label36.TabIndex = 76;
			this.label36.Text = "svcd";
			this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// vcdPath
			// 
			this.vcdPath.Location = new System.Drawing.Point(104, 136);
			this.vcdPath.Name = "vcdPath";
			this.vcdPath.Size = new System.Drawing.Size(112, 20);
			this.vcdPath.TabIndex = 95;
			this.vcdPath.Text = "";
			// 
			// label37
			// 
			this.label37.Location = new System.Drawing.Point(16, 136);
			this.label37.Name = "label37";
			this.label37.Size = new System.Drawing.Size(72, 24);
			this.label37.TabIndex = 72;
			this.label37.Text = "vcd";
			this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tvDvdrPath
			// 
			this.tvDvdrPath.Location = new System.Drawing.Point(104, 104);
			this.tvDvdrPath.Name = "tvDvdrPath";
			this.tvDvdrPath.Size = new System.Drawing.Size(112, 20);
			this.tvDvdrPath.TabIndex = 91;
			this.tvDvdrPath.Text = "";
			// 
			// label38
			// 
			this.label38.Location = new System.Drawing.Point(16, 104);
			this.label38.Name = "label38";
			this.label38.Size = new System.Drawing.Size(72, 24);
			this.label38.TabIndex = 68;
			this.label38.Text = "tv.dvdr";
			this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// tvPath
			// 
			this.tvPath.Location = new System.Drawing.Point(104, 40);
			this.tvPath.Name = "tvPath";
			this.tvPath.Size = new System.Drawing.Size(112, 20);
			this.tvPath.TabIndex = 83;
			this.tvPath.Text = "";
			// 
			// label39
			// 
			this.label39.Location = new System.Drawing.Point(16, 40);
			this.label39.Name = "label39";
			this.label39.Size = new System.Drawing.Size(72, 24);
			this.label39.TabIndex = 64;
			this.label39.Text = "tv.rip";
			this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label27
			// 
			this.label27.Location = new System.Drawing.Point(120, 16);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(72, 16);
			this.label27.TabIndex = 33;
			this.label27.Text = "Path";
			this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label28
			// 
			this.label28.Location = new System.Drawing.Point(16, 16);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(72, 16);
			this.label28.TabIndex = 32;
			this.label28.Text = "Section";
			this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// button31
			// 
			this.button31.Location = new System.Drawing.Point(376, 512);
			this.button31.Name = "button31";
			this.button31.Size = new System.Drawing.Size(112, 24);
			this.button31.TabIndex = 3;
			this.button31.Text = "Help";
			this.button31.Click += new System.EventHandler(this.button31_Click);
			// 
			// cancel
			// 
			this.cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancel.Location = new System.Drawing.Point(208, 512);
			this.cancel.Name = "cancel";
			this.cancel.Size = new System.Drawing.Size(112, 24);
			this.cancel.TabIndex = 2;
			this.cancel.Text = "Cancel";
			// 
			// ok
			// 
			this.ok.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.ok.Location = new System.Drawing.Point(16, 512);
			this.ok.Name = "ok";
			this.ok.Size = new System.Drawing.Size(112, 24);
			this.ok.TabIndex = 1;
			this.ok.Text = "Ok";
			this.ok.Click += new System.EventHandler(this.ok_Click);
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(24, 8);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(424, 16);
			this.label6.TabIndex = 4;
			this.label6.Text = "You need both PATH and BOTNAME, otherwise the section info will not be saved";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// SectionSettings
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(506, 549);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.ok);
			this.Controls.Add(this.cancel);
			this.Controls.Add(this.button31);
			this.Controls.Add(this.tabControl1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SectionSettings";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Section Settings";
			this.Load += new System.EventHandler(this.SectionSettings_Load);
			this.tabControl1.ResumeLayout(false);
			this.tabPage3.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void SectionSettings_Load(object sender, System.EventArgs e)
		{
			this.Text = "Section settings for " + site.Name;
			// put sitename in toolbar
			PopulateFields();
		}

		private void PopulateFields()
		{
			PopulateSection("mp3", "mp3");
			PopulateSection("mp3Singles", "mp3.singles");
			PopulateSection("mp3Live", "mp3.live");
			PopulateSection("gamecube", "gamecube");
			PopulateSection("xboxRipPal", "xbox.rip.pal");
			PopulateSection("xboxRipNtsc", "xbox.rip.ntsc");
			PopulateSection("xboxDvdPal", "xbox.dvd.pal");
			PopulateSection("xboxDvdNtsc", "xbox.dvd.ntsc");
			PopulateSection("ps2RipPal", "ps2.rip.pal");
			PopulateSection("ps2RipNtsc", "ps2.rip.ntsc");
			PopulateSection("ps2DvdPal", "ps2.dvd.pal");
			PopulateSection("ps2DvdNtsc", "ps2.dvd.ntsc");

// for some odd reason, 0day doen't work
			PopulateSection("oday", "0day");
			PopulateSection("games", "games");
			PopulateSection("dox", "dox");
			PopulateSection("gba", "gba");
			PopulateSection("isoapps", "isoapps");
			PopulateSection("samplecd", "samplecd");
			PopulateSection("bookware", "bookware");
			PopulateSection("pda", "pda");
			PopulateSection("mvids", "mvids");
			PopulateSection("xxx", "xxx");
			PopulateSection("xxxDivx", "xxx.divx");

			PopulateSection("tv", "tv");
			PopulateSection("tvDivx", "tv.divx");
			PopulateSection("tvDvdr", "tv.dvdr");
			PopulateSection("vcd", "vcd");
			PopulateSection("svcd", "svcd");
			PopulateSection("svcdDvdscr", "svcd.dvdscreener");
			PopulateSection("divxDvdrip", "divx");
			PopulateSection("divxDvdscr", "divx.dvdscreener");
			PopulateSection("dvdrPal", "dvdr.pal");
			PopulateSection("dvdrPalDvdscr", "dvdr.pal.dvdscreener");
			PopulateSection("dvdrNtsc", "dvdr.ntsc");
			PopulateSection("dvdrNtscDvdscr", "dvdr.ntsc.dvdscreener");
		}


		private void ok_Click(object sender, System.EventArgs e)
		{
			// if we have changed, go over the sections one by one and create those
			// that have a path (ignore the others)

			SaveSection("mp3", "mp3");
			SaveSection("mp3Singles", "mp3.singles");
			SaveSection("mp3Live", "mp3.live");
			SaveSection("gamecube", "gamecube");
			SaveSection("xboxRipPal", "xbox.rip.pal");
			SaveSection("xboxRipNtsc", "xbox.rip.ntsc");
			SaveSection("xboxDvdPal", "xbox.dvd.pal");
			SaveSection("xboxDvdNtsc", "xbox.dvd.ntsc");
			SaveSection("ps2RipPal", "ps2.rip.pal");
			SaveSection("ps2RipNtsc", "ps2.rip.ntsc");
			SaveSection("ps2DvdPal", "ps2.dvd.pal");
			SaveSection("ps2DvdNtsc", "ps2.dvd.ntsc");

			SaveSection("oday", "0day");
			SaveSection("games", "games");
			SaveSection("dox", "dox");
			SaveSection("gba", "gba");
			SaveSection("isoapps", "isoapps");
			SaveSection("samplecd", "samplecd");
			SaveSection("bookware", "bookware");
			SaveSection("pda", "pda");
			SaveSection("mvids", "mvids");
			SaveSection("xxx", "xxx");
			SaveSection("xxxDivx", "xxx.divx");

			SaveSection("tv", "tv");
			SaveSection("tvDivx", "tv.divx");
			SaveSection("tvDvdr", "tv.dvdr");
			SaveSection("vcd", "vcd");
			SaveSection("svcd", "svcd");
			SaveSection("svcdDvdscr", "svcd.dvdscreener");
			SaveSection("divxDvdrip", "divx");
			SaveSection("divxDvdscr", "divx.dvdscreener");
			SaveSection("dvdrPal", "dvdr.pal");
			SaveSection("dvdrPalDvdscr", "dvdr.pal.dvdscreener");
			SaveSection("dvdrNtsc", "dvdr.ntsc");
			SaveSection("dvdrNtscDvdscr", "dvdr.ntsc.dvdscreener");

			// todo: how do we determine what dialog result to return here. we don't want to accept unless we've verified all the data first.
		}
		private void PopulateSection(string fieldPrefix, string sectionName)
		{
			Section s = (Section)site.Sections[sectionName];
			if (s != null) 
			{
				GetControl(fieldPrefix + "Path").Text = s.Path;
				GetControl(fieldPrefix + "Botname").Text = s.Botname;
				((CheckBox)GetControl(fieldPrefix + "Active")).Checked = s.Active;
			}
		}

		private void SaveSection(string fieldPrefix, string sectionName)
		{
			string path = GetControl(fieldPrefix + "Path").Text;
			string botname  = GetControl(fieldPrefix + "Botname").Text;
			bool active = ((CheckBox)GetControl(fieldPrefix + "Active")).Checked;

			if (path.Length > 0 && botname.Length > 0)
			{
				// extract the section first, to see if we might already have it.
				Section oldS = (Section)site.Sections[sectionName];
				if (oldS == null) 
				{
					// this happens if we haven't edited the rules yet.
					// as soon as we edit the rules for a section, that section gets saved.
					Section s = new Section(sectionName, site, path, botname, active);
					site.Sections.Add(sectionName, s);
				}
				else
				{
					oldS.Path = path;
					oldS.Botname = botname;
					oldS.Active = active;
				}
				// note that this doesn't check if we're already using this botname for another site.
				// people will just have to be careful
				Config.Bots[botname.ToLower()] = site;
				// todo: maybe later, we'll check if this botname is already tied to another site...

				// if we have edited the rules, they are already set, and we don't need to worry
			}
			else
			{
				if (site.Sections.Contains(sectionName))
				{
					site.Sections.Remove(sectionName);
				}
			}
		}

		private Control GetControl(string controlName)
		{
			return EnumerateControls(controlName, this);
		}

		private Control EnumerateControls(string controlName, Control c)
		{
			for (int i = 0; i < c.Controls.Count; i++)
			{
				Control control = (Control) c.Controls[i];
				if (control.Name.Equals(controlName))
				{
					return control;
				}
				if (control.Controls.Count > 1)
				{
					Control xc = EnumerateControls(controlName, control);
					if (xc != null)
					{
						return xc;
					}
				}
			}
			return null;
		}
		private void SetRules(string fieldPrefix, string sectionName)
		{
			SaveSection(fieldPrefix, sectionName);
			Section section = (Section)site.Sections[sectionName];
			if (section != null) 
			{
				RulesGui rg = new RulesGui(section);
				rg.ShowDialog();
			} 
			else
			{
				MessageBox.Show("Can't set rules for a nonexistant section");
			}
		}

		private void button31_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("Help will be provided here later. Supply both path and botname, and check the 'active' box otherwise the section won't be saved, and you won't get the announces and you won't be able to trade (botname including psybnc network prefix, like 'e~botname'");
		}

		#region set rules
		private void mp3Rules_Click(object sender, System.EventArgs e)
		{
			SetRules("mp3", "mp3");
		}

		private void mp3SinglesRules_Click(object sender, System.EventArgs e)
		{
			SetRules("mp3Singles", "mp3.singles");
		}

		private void mp3LiveRules_Click(object sender, System.EventArgs e)
		{
			SetRules("mp3Live", "mp3.live");
		}

		private void ps2RipPalRules_Click(object sender, System.EventArgs e)
		{
			SetRules("ps2RipPal", "ps2.rip.pal");
		}

		private void xboxRipPalRules_Click(object sender, System.EventArgs e)
		{
			SetRules("xboxRipPal", "xbox.rip.pal");
		}

		private void ps2DvdNtscRules_Click(object sender, System.EventArgs e)
		{
			SetRules("ps2DvdNtsc", "ps2.dvd.ntsc");
		}

		private void xboxDvdNtscRules_Click(object sender, System.EventArgs e)
		{
			SetRules("xboxDvdNtsc", "xbox.dvd.ntsc");
		}

		private void gamecubeRules_Click(object sender, System.EventArgs e)
		{
			SetRules("gamecube", "gamecube");
		}

		private void ps2DvdPalRules_Click(object sender, System.EventArgs e)
		{
			SetRules("ps2DvdPal", "ps2.dvd.pal");
		}

		private void xboxDvdPalRules_Click(object sender, System.EventArgs e)
		{
			SetRules("xboxDvdPal", "xbox.dvd.pal");
		}

		private void ps2RipNtscRules_Click(object sender, System.EventArgs e)
		{
			SetRules("ps2RipNtsc", "ps2.rip.ntsc");
		}

		private void xboxRipNtscRules_Click(object sender, System.EventArgs e)
		{
			SetRules("xboxRipNtsc", "xboxRipNtsc");
		}

		private void xxxDivxRules_Click(object sender, System.EventArgs e)
		{
			SetRules("xxxDivx", "xxx.divx");
		}

		private void xxxRules_Click(object sender, System.EventArgs e)
		{
			SetRules("xxx", "xxx");
		}

		private void bookwareRules_Click(object sender, System.EventArgs e)
		{
			SetRules("bookware", "bookware");
		}

		private void gamesRules_Click(object sender, System.EventArgs e)
		{
			SetRules("games", "games");
		}

		private void samplecdRules_Click(object sender, System.EventArgs e)
		{
			SetRules("samplecd", "samplecd");
		}

		private void odayRules_Click(object sender, System.EventArgs e)
		{
			SetRules("oday", "0day");
		}

		private void isoappsRules_Click(object sender, System.EventArgs e)
		{
			SetRules("isoapps", "isoapps");
		}

		private void mvidsRules_Click(object sender, System.EventArgs e)
		{
			SetRules("mvids", "mvids");
		}

		private void pdaRules_Click(object sender, System.EventArgs e)
		{
			SetRules("pda", "pda");
		}

		private void dvdrNtscRules_Click(object sender, System.EventArgs e)
		{
			SetRules("dvdrNtsc", "dvdr.ntsc");
		}

		private void svcdRules_Click(object sender, System.EventArgs e)
		{
			SetRules("svcd", "svcd");
		}

		private void divxDvdscrRules_Click(object sender, System.EventArgs e)
		{
			SetRules("divxDvdscr", "divx.dvdscr");
		}

		private void tvDivxRules_Click(object sender, System.EventArgs e)
		{
			SetRules("tvDivx", "tv.divx");
		}

		private void divxDvdripRules_Click(object sender, System.EventArgs e)
		{
			SetRules("divxDvdrip", "divx");
		}

		private void svcdDvdscrRules_Click(object sender, System.EventArgs e)
		{
			SetRules("svcdDvdscr", "svcd.dvdscreener");
		}

		private void dvdrPalDvdscrRules_Click(object sender, System.EventArgs e)
		{
			SetRules("dvdrPalDvdscr", "dvdr.pal.dvdscr");
		}

		private void dvdrPalRules_Click(object sender, System.EventArgs e)
		{
			SetRules("dvdrPal", "dvdr.pal");
		}

		private void vcdRules_Click(object sender, System.EventArgs e)
		{
			SetRules("vcd", "vcd");
		}

		private void tvRules_Click(object sender, System.EventArgs e)
		{
			SetRules("tv", "tv");
		}

		private void tvDvdrRules_Click(object sender, System.EventArgs e)
		{
			SetRules("tvDvdr", "tv.dvdr");
		}

		private void dvdrNtscDvdscrRules_Click(object sender, System.EventArgs e)
		{
			SetRules("dvdrNtscDvdscr", "dvdr.ntsc.dvdscr");
		}
		#endregion
	}
}

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Tiki.Gui
{
	/// <summary>
	/// Summary description for GeneralSettings.
	/// </summary>
	public class GeneralSettings : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage network;
		private System.Windows.Forms.TextBox ident;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox sslProxyHostname;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox sslProxyPort;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button okButton;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.CheckBox useIdent;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown reconnectTimeout;
		private System.Windows.Forms.CheckBox useAlternateFxp;
		private System.Windows.Forms.CheckBox prefixUsernameWithHash;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public GeneralSettings()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(GeneralSettings));
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.network = new System.Windows.Forms.TabPage();
			this.reconnectTimeout = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.useIdent = new System.Windows.Forms.CheckBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.sslProxyPort = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.sslProxyHostname = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.ident = new System.Windows.Forms.TextBox();
			this.okButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			this.useAlternateFxp = new System.Windows.Forms.CheckBox();
			this.prefixUsernameWithHash = new System.Windows.Forms.CheckBox();
			this.tabControl1.SuspendLayout();
			this.network.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.reconnectTimeout)).BeginInit();
			this.SuspendLayout();
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.network);
			this.tabControl1.Location = new System.Drawing.Point(16, 24);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(568, 376);
			this.tabControl1.TabIndex = 0;
			// 
			// network
			// 
			this.network.Controls.Add(this.prefixUsernameWithHash);
			this.network.Controls.Add(this.useAlternateFxp);
			this.network.Controls.Add(this.reconnectTimeout);
			this.network.Controls.Add(this.label5);
			this.network.Controls.Add(this.useIdent);
			this.network.Controls.Add(this.label4);
			this.network.Controls.Add(this.label3);
			this.network.Controls.Add(this.sslProxyPort);
			this.network.Controls.Add(this.label2);
			this.network.Controls.Add(this.sslProxyHostname);
			this.network.Controls.Add(this.label1);
			this.network.Controls.Add(this.ident);
			this.network.Location = new System.Drawing.Point(4, 22);
			this.network.Name = "network";
			this.network.Size = new System.Drawing.Size(560, 350);
			this.network.TabIndex = 0;
			this.network.Text = "Network";
			// 
			// reconnectTimeout
			// 
			this.reconnectTimeout.Location = new System.Drawing.Point(72, 160);
			this.reconnectTimeout.Name = "reconnectTimeout";
			this.reconnectTimeout.Size = new System.Drawing.Size(56, 20);
			this.reconnectTimeout.TabIndex = 10;
			this.reconnectTimeout.Value = new System.Decimal(new int[] {
																		   30,
																		   0,
																		   0,
																		   0});
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(144, 160);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(184, 24);
			this.label5.TabIndex = 9;
			this.label5.Text = "Reconnect Timeout";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// useIdent
			// 
			this.useIdent.Location = new System.Drawing.Point(344, 16);
			this.useIdent.Name = "useIdent";
			this.useIdent.Size = new System.Drawing.Size(192, 24);
			this.useIdent.TabIndex = 7;
			this.useIdent.Text = "Use built-in ident";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(336, 64);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(200, 144);
			this.label4.TabIndex = 6;
			this.label4.Text = @"At the moment, the .net framework doesn't support ssl sockets in any kind of acceptable fashion, so we'll have to use a proxy engine for all our ssl needs. Same thing for irc right now. I recommend WinSSLWrap, bye hoe. It might be a bit hard to find, but there's an ""ssl pack"" on the linknet homepage that contains winsslwrap. Must support USER ftpuser@ftphost:ftpport";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(144, 104);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(184, 24);
			this.label3.TabIndex = 5;
			this.label3.Text = "SSL Proxy Port";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// sslProxyPort
			// 
			this.sslProxyPort.Location = new System.Drawing.Point(16, 104);
			this.sslProxyPort.Name = "sslProxyPort";
			this.sslProxyPort.Size = new System.Drawing.Size(112, 20);
			this.sslProxyPort.TabIndex = 4;
			this.sslProxyPort.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(144, 72);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(184, 24);
			this.label2.TabIndex = 3;
			this.label2.Text = "SSL Proxy hostname";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// sslProxyHostname
			// 
			this.sslProxyHostname.Location = new System.Drawing.Point(16, 72);
			this.sslProxyHostname.Name = "sslProxyHostname";
			this.sslProxyHostname.Size = new System.Drawing.Size(112, 20);
			this.sslProxyHostname.TabIndex = 2;
			this.sslProxyHostname.Text = "";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(144, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(184, 24);
			this.label1.TabIndex = 1;
			this.label1.Text = "Ident Reply";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// ident
			// 
			this.ident.Location = new System.Drawing.Point(16, 16);
			this.ident.Name = "ident";
			this.ident.Size = new System.Drawing.Size(112, 20);
			this.ident.TabIndex = 0;
			this.ident.Text = "";
			// 
			// okButton
			// 
			this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.okButton.Location = new System.Drawing.Point(152, 408);
			this.okButton.Name = "okButton";
			this.okButton.Size = new System.Drawing.Size(128, 24);
			this.okButton.TabIndex = 1;
			this.okButton.Text = "Ok";
			this.okButton.Click += new System.EventHandler(this.okButton_Click);
			// 
			// cancelButton
			// 
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Location = new System.Drawing.Point(288, 408);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(128, 24);
			this.cancelButton.TabIndex = 2;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
			// 
			// useAlternateFxp
			// 
			this.useAlternateFxp.Location = new System.Drawing.Point(112, 192);
			this.useAlternateFxp.Name = "useAlternateFxp";
			this.useAlternateFxp.Size = new System.Drawing.Size(192, 24);
			this.useAlternateFxp.TabIndex = 11;
			this.useAlternateFxp.Text = "Use Alternate FXP mode";
			// 
			// prefixUsernameWithHash
			// 
			this.prefixUsernameWithHash.Location = new System.Drawing.Point(112, 224);
			this.prefixUsernameWithHash.Name = "prefixUsernameWithHash";
			this.prefixUsernameWithHash.Size = new System.Drawing.Size(336, 24);
			this.prefixUsernameWithHash.TabIndex = 12;
			this.prefixUsernameWithHash.Text = "Prefix username with # (for use with TLSWrap)";
			// 
			// GeneralSettings
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(600, 437);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.okButton);
			this.Controls.Add(this.tabControl1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "GeneralSettings";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "GeneralSettings";
			this.Load += new System.EventHandler(this.GeneralSettings_Load);
			this.tabControl1.ResumeLayout(false);
			this.network.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.reconnectTimeout)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void okButton_Click(object sender, System.EventArgs e)
		{
			SaveSettings();
			this.Close();
		}

		private void SaveSettings()
		{
			Config.GeneralSettings["ident"] = this.ident.Text;
			Config.GeneralSettings["use_ident"] = this.useIdent.Checked;
			Config.GeneralSettings["proxy_host"] = this.sslProxyHostname.Text;
			Config.GeneralSettings["proxy_port"] = this.sslProxyPort.Text;
			Config.GeneralSettings["reconnect_timeout"] = Convert.ToInt32(this.reconnectTimeout.Text);
			Config.GeneralSettings["use_alternate_fxp"] = this.useAlternateFxp.Checked;
			Config.GeneralSettings["prefix_username_with_hash"] = this.prefixUsernameWithHash.Checked;

		}

		private void cancelButton_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		private void GeneralSettings_Load(object sender, System.EventArgs e)
		{
			this.ident.Text = (string)Config.GeneralSettings["ident"];
			this.useIdent.Checked = (bool)(Config.GeneralSettings["use_ident"] == null ? false : Config.GeneralSettings["use_ident"]);
			this.sslProxyHostname.Text = (string)Config.GeneralSettings["proxy_host"];
			this.sslProxyPort.Text = (string)Config.GeneralSettings["proxy_port"];
			this.reconnectTimeout.Text = (string)Config.GeneralSettings["reconnect_timeout"];
			this.useAlternateFxp.Checked = (bool)(Config.GeneralSettings["use_alternate_fxp"] == null ? false : Config.GeneralSettings["use_alternate_fxp"]);
			this.prefixUsernameWithHash.Checked = (bool)(Config.GeneralSettings["prefix_username_with_hash"] == null ? false : Config.GeneralSettings["prefix_username_with_hash"]);
		}
	}
}

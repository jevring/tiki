using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

namespace Tiki.Gui
{
	/// <summary>
	/// Summary description for TradeAnnounce.
	/// </summary>
	public class TradeAnnounce : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label destinationsLabel;
		private System.Windows.Forms.Label announceLabel;
		private System.Windows.Forms.Label releaseNameLabel;
		private System.Windows.Forms.Label sourceLabel;
		private System.Windows.Forms.ListBox pendingRaces;
		private System.Windows.Forms.Button trade;
		private System.Windows.Forms.Button skip;
		private System.Windows.Forms.Button close;
		private System.Windows.Forms.Button tradeAll;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.ComboBox changedSection;

		private ClientGui gui;
		private RaceAnnounce currentlySelectedAnnounce = null;
		private System.Windows.Forms.Label label7;
		private bool triggerSectionUpdate = true;

		public TradeAnnounce(ClientGui gui)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.gui = gui;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(TradeAnnounce));
			this.pendingRaces = new System.Windows.Forms.ListBox();
			this.label1 = new System.Windows.Forms.Label();
			this.trade = new System.Windows.Forms.Button();
			this.skip = new System.Windows.Forms.Button();
			this.close = new System.Windows.Forms.Button();
			this.tradeAll = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.destinationsLabel = new System.Windows.Forms.Label();
			this.announceLabel = new System.Windows.Forms.Label();
			this.releaseNameLabel = new System.Windows.Forms.Label();
			this.sourceLabel = new System.Windows.Forms.Label();
			this.changedSection = new System.Windows.Forms.ComboBox();
			this.label7 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// pendingRaces
			// 
			this.pendingRaces.Location = new System.Drawing.Point(8, 24);
			this.pendingRaces.Name = "pendingRaces";
			this.pendingRaces.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
			this.pendingRaces.Size = new System.Drawing.Size(592, 160);
			this.pendingRaces.TabIndex = 0;
			this.pendingRaces.SelectedIndexChanged += new System.EventHandler(this.pendingRaces_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(416, 16);
			this.label1.TabIndex = 1;
			this.label1.Text = "Pending races:";
			// 
			// trade
			// 
			this.trade.Location = new System.Drawing.Point(112, 352);
			this.trade.Name = "trade";
			this.trade.Size = new System.Drawing.Size(96, 24);
			this.trade.TabIndex = 1;
			this.trade.Text = "&Trade";
			this.trade.Click += new System.EventHandler(this.trade_Click);
			// 
			// skip
			// 
			this.skip.Location = new System.Drawing.Point(320, 352);
			this.skip.Name = "skip";
			this.skip.Size = new System.Drawing.Size(96, 24);
			this.skip.TabIndex = 3;
			this.skip.Text = "&Skip";
			this.skip.Click += new System.EventHandler(this.skip_Click);
			// 
			// close
			// 
			this.close.Location = new System.Drawing.Point(424, 352);
			this.close.Name = "close";
			this.close.Size = new System.Drawing.Size(96, 24);
			this.close.TabIndex = 4;
			this.close.Text = "&Close";
			this.close.Click += new System.EventHandler(this.close_Click);
			// 
			// tradeAll
			// 
			this.tradeAll.Location = new System.Drawing.Point(216, 352);
			this.tradeAll.Name = "tradeAll";
			this.tradeAll.Size = new System.Drawing.Size(96, 24);
			this.tradeAll.TabIndex = 2;
			this.tradeAll.Text = "Trade &All";
			this.tradeAll.Click += new System.EventHandler(this.tradeAll_Click);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 248);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(88, 16);
			this.label2.TabIndex = 6;
			this.label2.Text = "From: ";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 200);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(88, 16);
			this.label3.TabIndex = 7;
			this.label3.Text = "Release Name:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(16, 272);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(88, 16);
			this.label4.TabIndex = 9;
			this.label4.Text = "Destinations:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(16, 296);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(88, 16);
			this.label5.TabIndex = 8;
			this.label5.Text = "Announce:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(16, 224);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(88, 16);
			this.label6.TabIndex = 10;
			this.label6.Text = "Section:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// destinationsLabel
			// 
			this.destinationsLabel.Location = new System.Drawing.Point(112, 272);
			this.destinationsLabel.Name = "destinationsLabel";
			this.destinationsLabel.Size = new System.Drawing.Size(488, 16);
			this.destinationsLabel.TabIndex = 14;
			this.destinationsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// announceLabel
			// 
			this.announceLabel.Location = new System.Drawing.Point(112, 296);
			this.announceLabel.Name = "announceLabel";
			this.announceLabel.Size = new System.Drawing.Size(488, 48);
			this.announceLabel.TabIndex = 13;
			this.announceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// releaseNameLabel
			// 
			this.releaseNameLabel.Location = new System.Drawing.Point(112, 200);
			this.releaseNameLabel.Name = "releaseNameLabel";
			this.releaseNameLabel.Size = new System.Drawing.Size(488, 16);
			this.releaseNameLabel.TabIndex = 12;
			this.releaseNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// sourceLabel
			// 
			this.sourceLabel.Location = new System.Drawing.Point(112, 248);
			this.sourceLabel.Name = "sourceLabel";
			this.sourceLabel.Size = new System.Drawing.Size(488, 16);
			this.sourceLabel.TabIndex = 11;
			this.sourceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// changedSection
			// 
			this.changedSection.Items.AddRange(new object[] {
																"0day",
																"bookware",
																"divx",
																"divx.dvdscr",
																"dox",
																"dvdr.ntsc",
																"dvdr.ntsc.dvdscr",
																"dvdr.pal",
																"dvdr.pal.dvdscr",
																"gamecube",
																"games",
																"gba",
																"isoapps",
																"mp3",
																"mp3.live",
																"mp3.singles",
																"mvids",
																"mvids",
																"pda",
																"ps2.dvd.ntsc",
																"ps2.dvd.pal",
																"ps2.rip.ntsc",
																"ps2.rip.pal",
																"samplecd",
																"svcd",
																"svcd.dvdscreener",
																"tv",
																"tv.divx",
																"tv.dvdr",
																"vcd",
																"xbox.dvd.ntsc",
																"xbox.dvd.pal",
																"xbox.rip.ntsc",
																"xbox.rip.pal",
																"xxx",
																"xxx.divx"});
			this.changedSection.Location = new System.Drawing.Point(112, 216);
			this.changedSection.Name = "changedSection";
			this.changedSection.Size = new System.Drawing.Size(136, 21);
			this.changedSection.Sorted = true;
			this.changedSection.TabIndex = 0;
			this.changedSection.SelectedIndexChanged += new System.EventHandler(this.changedSection_SelectedIndexChanged);
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(256, 224);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(280, 16);
			this.label7.TabIndex = 15;
			this.label7.Text = "(If this is wrong, change the section and the click trade)";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// TradeAnnounce
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(608, 381);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.changedSection);
			this.Controls.Add(this.destinationsLabel);
			this.Controls.Add(this.announceLabel);
			this.Controls.Add(this.releaseNameLabel);
			this.Controls.Add(this.sourceLabel);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.tradeAll);
			this.Controls.Add(this.close);
			this.Controls.Add(this.skip);
			this.Controls.Add(this.trade);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.pendingRaces);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "TradeAnnounce";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Trade Announce";
			this.TopMost = true;
			this.ResumeLayout(false);

		}

		#endregion

		public void AddAnnounce(RaceAnnounce announce)
		{
			// for some fucking reason, this does not work: !pendingRaces.Items.Contains(announce)
			// we can't compare objects either, it's like we have to add the names only or something, which is stupid
			if (!AlreadyAdded(announce)) 
			{
				pendingRaces.Items.Add(announce);
				if (pendingRaces.Items.Count == 1)
				{
					pendingRaces.SelectedIndex = 0;
				}
				pendingRaces.Refresh();
			}
			else
			{
				// remove this else later
				Logger.Log("we already had this announce");
			}
			this.trade.Select();
		}

		private bool AlreadyAdded(RaceAnnounce announce)
		{
			for (int i = 0; i < pendingRaces.Items.Count; i++)
			{
				RaceAnnounce raceAnnounce = (RaceAnnounce) pendingRaces.Items[i];
				
				if (raceAnnounce.ReleaseName.Equals(announce.ReleaseName))
				{
					return true;
				}
			}
			return false;
		}


		private void pendingRaces_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DisplaySelectedRaceAnnounceInfo();
		}
		private void DisplaySelectedRaceAnnounceInfo()
		{
			// show the info for the race in the details pane
			RaceAnnounce announce = (RaceAnnounce)pendingRaces.SelectedItem;
			currentlySelectedAnnounce = announce;
			if (announce != null) 
			{
				this.Text = "Trade: " + announce.ReleaseName + "?";

				releaseNameLabel.Text = announce.ReleaseName.Trim();
				sourceLabel.Text = announce.Source.Name.Trim();
				destinationsLabel.Text = DisplaySiteArrayList(announce.Destinations).Trim();
				announceLabel.Text = announce.AnnounceLine.Trim();
				triggerSectionUpdate = false;
				changedSection.SelectedItem = announce.Section;
				triggerSectionUpdate = true;
				
			}
		}
		public string DisplaySiteArrayList(ArrayList al)
		{
			string s = "";
			for (int i = 0; i < al.Count; i++)
			{
				Site site = (Site) al[i];
				s += site.Name + " ";
			}
			return s.Trim();
		}


		private void trade_Click(object sender, System.EventArgs e)
		{
			RaceAnnounce announce = (RaceAnnounce)pendingRaces.SelectedItem;
			// tell the gui below that we're trading this one (which in turn tells the client engine, who tells the server engine)
			gui.InitiateRace(announce);
			// remove it from the list
			pendingRaces.Items.Remove(announce);
			// select the next one in the queue (should we list up or down?)
			if (pendingRaces.Items.Count != 0) 
			{
				pendingRaces.SelectedIndex = 0;
				// show the information for that race.
				DisplaySelectedRaceAnnounceInfo();
			}
			else
			{
				// if we have no more pending races, close the window	
				this.Close();
			}
		}

		private void tradeAll_Click(object sender, System.EventArgs e)
		{
			// go over all the trade in the list, and trade them
			foreach (RaceAnnounce announce in pendingRaces.Items)
			{
				gui.InitiateRace(announce);
			}
			// clear the list
			pendingRaces.Items.Clear();
			// close the window
			this.Close();
		}
		

		private void skip_Click(object sender, System.EventArgs e)
		{
			// remove this entry from the list 
			RaceAnnounce announce = (RaceAnnounce)pendingRaces.SelectedItem;
			pendingRaces.Items.Remove(announce);
			if (pendingRaces.Items.Count != 0) 
			{
				pendingRaces.SelectedIndex = 0;
				// show the information for that race.
				DisplaySelectedRaceAnnounceInfo();
			}
			else
			{
				// if we have no more pending races, close the window	
				this.Close();
			}
		}

		private void close_Click(object sender, System.EventArgs e)
		{
			// clear the list, close the window.
			pendingRaces.Items.Clear();
			this.Close();
		}

		private void changedSection_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (triggerSectionUpdate) 
			{
				MessageBox.Show("changing the section for this announce");
				// reparse the rules and whatnot and redisplay the proper destinations and such
				// this happens VERY seldom
				IrcParser parser = new IrcParser(null);
				if (currentlySelectedAnnounce != null)
				{
					currentlySelectedAnnounce.Section = changedSection.SelectedText;
					currentlySelectedAnnounce.Destinations = parser.GetAllowedDestinationSites(parser.GetDestinationSites(changedSection.SelectedText), currentlySelectedAnnounce.ReleaseName, changedSection.SelectedText, currentlySelectedAnnounce.Source.Name);
				}
				DisplaySelectedRaceAnnounceInfo();
			}
			
		}



	}
}

using System;
using System.Collections;
using System.Drawing;
using System.Net.Sockets;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace Tiki.Gui
{
	/// <summary>
	/// Summary description for ClientGui.
	/// </summary>
	public class ClientGui : System.Windows.Forms.Form
	{
		#region auto-generated declarations
		private System.Windows.Forms.MainMenu mainMenu;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem optionsMenuItem;
		private System.Windows.Forms.MenuItem optionsIrcMenuItem;
		private System.Windows.Forms.MenuItem optionSitesMenuItem;
		private System.Windows.Forms.ListView raceList;
		private System.Windows.Forms.ColumnHeader raceHeaderReleaseName;
		private System.Windows.Forms.ColumnHeader raceHeaderProgress;
		private System.Windows.Forms.ColumnHeader raceHeaderStartTime;
		private System.Windows.Forms.MenuItem menuOptionsSitePriority;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.TextBox log;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ListBox connectedSites;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ListBox checkedOutSites;
		private System.Windows.Forms.ColumnHeader raceHeaderState;
		private System.Windows.Forms.MenuItem menuFileSave;
		private System.Windows.Forms.MenuItem menuFileLoad;
		private System.Windows.Forms.MenuItem menuOptionsGeneralSettings;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.RadioButton fullAuto;
		private System.Windows.Forms.RadioButton semiAuto;
		public System.Windows.Forms.RadioButton dupeDB;
		private System.Windows.Forms.Button connectToIrcButton;
		private System.Windows.Forms.Button connectToSitesButton;
		private System.Windows.Forms.MenuItem optionsAlwaysOnTopMenuItem;
		private System.Windows.Forms.Button showLogButton;
		private System.Windows.Forms.MenuItem fileForceCloseMenuItem;
		private System.Windows.Forms.ContextMenu raceListContextMenu;
		private System.Windows.Forms.MenuItem rlcmDeleteRaceMenuItem;
		private System.Windows.Forms.MenuItem rlcmPromoteRaceMenuItem;
		private System.Windows.Forms.MenuItem rlcmDemoteRaceMenuItem;
		private System.Windows.Forms.MenuItem optionsReleaseGroupMenuItem;
		private System.Windows.Forms.MenuItem cmrlForcekillRace;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.MenuItem actionsExecuteOnSelected;
		private System.Windows.Forms.ListView transfersListView;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		#endregion
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private ClientEngine engine;
		private bool shuttingDown = false;
		private Thread connectedSitesChecker;
		private Thread updateTransferStatusTread;
		private TradeAnnounce tradeAnnounce = null;
		private Hashtable raceListings;
		private Hashtable announces;
		private string currentlySelectedRaceName = "";
		private static object raceUpdateLock = new object();
		private bool tradeAnnounceOpen = false;
		private FloatingLogger logger;
		private bool loggerInvokeNeeded = false;
		private DateTime lastIrcActivity = System.DateTime.Now;
		private bool serverEngineCreated = false;
		private System.Windows.Forms.MenuItem disconnectSelectedSitesMenuItem;
		private System.Windows.Forms.MenuItem actionSkipCurrentFileInSelectedTransfer;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.CheckedListBox joinedChannels;
		private System.Windows.Forms.Button joinedChannelsHelp;
		private System.Windows.Forms.StatusBar statusBar;
		private System.Windows.Forms.StatusBarPanel ircActivityIconPanel;
		private System.Windows.Forms.StatusBarPanel timePanel;
		private System.Windows.Forms.StatusBarPanel ircActivityTextPanel;
		private System.Windows.Forms.MenuItem actionCleanDeadSites;
		private System.Windows.Forms.ColumnHeader raceHeaderSectionName;
		private string title = "";
		private System.Windows.Forms.MenuItem optionsWeightRatio;
		private System.Windows.Forms.MenuItem cmrlPanicBackfillRemove;
		private string news = "";


		public ClientGui()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			logger = new FloatingLogger(this);
			//engine = new ClientEngine(this);
			tradeAnnounce = new TradeAnnounce(this);
			raceListings = new Hashtable();
			announces = new Hashtable();
			title = "Tiki - [Version: " + Application.ProductVersion + "]";
		}
		[STAThread]
		static void Main(string[] args)
		{
			Application.Run(new ClientGui());
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ClientGui));
			this.mainMenu = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuFileSave = new System.Windows.Forms.MenuItem();
			this.menuFileLoad = new System.Windows.Forms.MenuItem();
			this.fileForceCloseMenuItem = new System.Windows.Forms.MenuItem();
			this.optionsMenuItem = new System.Windows.Forms.MenuItem();
			this.optionSitesMenuItem = new System.Windows.Forms.MenuItem();
			this.optionsWeightRatio = new System.Windows.Forms.MenuItem();
			this.menuOptionsSitePriority = new System.Windows.Forms.MenuItem();
			this.optionsIrcMenuItem = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuOptionsGeneralSettings = new System.Windows.Forms.MenuItem();
			this.optionsReleaseGroupMenuItem = new System.Windows.Forms.MenuItem();
			this.optionsAlwaysOnTopMenuItem = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.actionsExecuteOnSelected = new System.Windows.Forms.MenuItem();
			this.disconnectSelectedSitesMenuItem = new System.Windows.Forms.MenuItem();
			this.actionSkipCurrentFileInSelectedTransfer = new System.Windows.Forms.MenuItem();
			this.actionCleanDeadSites = new System.Windows.Forms.MenuItem();
			this.raceList = new System.Windows.Forms.ListView();
			this.raceHeaderSectionName = new System.Windows.Forms.ColumnHeader();
			this.raceHeaderReleaseName = new System.Windows.Forms.ColumnHeader();
			this.raceHeaderProgress = new System.Windows.Forms.ColumnHeader();
			this.raceHeaderStartTime = new System.Windows.Forms.ColumnHeader();
			this.raceHeaderState = new System.Windows.Forms.ColumnHeader();
			this.raceListContextMenu = new System.Windows.Forms.ContextMenu();
			this.rlcmDeleteRaceMenuItem = new System.Windows.Forms.MenuItem();
			this.rlcmPromoteRaceMenuItem = new System.Windows.Forms.MenuItem();
			this.rlcmDemoteRaceMenuItem = new System.Windows.Forms.MenuItem();
			this.cmrlForcekillRace = new System.Windows.Forms.MenuItem();
			this.connectToIrcButton = new System.Windows.Forms.Button();
			this.connectToSitesButton = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.log = new System.Windows.Forms.TextBox();
			this.connectedSites = new System.Windows.Forms.ListBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.checkedOutSites = new System.Windows.Forms.ListBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.dupeDB = new System.Windows.Forms.RadioButton();
			this.semiAuto = new System.Windows.Forms.RadioButton();
			this.fullAuto = new System.Windows.Forms.RadioButton();
			this.showLogButton = new System.Windows.Forms.Button();
			this.transfersListView = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
			this.label5 = new System.Windows.Forms.Label();
			this.joinedChannels = new System.Windows.Forms.CheckedListBox();
			this.label6 = new System.Windows.Forms.Label();
			this.joinedChannelsHelp = new System.Windows.Forms.Button();
			this.statusBar = new System.Windows.Forms.StatusBar();
			this.ircActivityTextPanel = new System.Windows.Forms.StatusBarPanel();
			this.ircActivityIconPanel = new System.Windows.Forms.StatusBarPanel();
			this.timePanel = new System.Windows.Forms.StatusBarPanel();
			this.cmrlPanicBackfillRemove = new System.Windows.Forms.MenuItem();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ircActivityTextPanel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ircActivityIconPanel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.timePanel)).BeginInit();
			this.SuspendLayout();
			// 
			// mainMenu
			// 
			this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.menuItem1,
																					 this.optionsMenuItem,
																					 this.menuItem3});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuFileSave,
																					  this.menuFileLoad,
																					  this.fileForceCloseMenuItem});
			this.menuItem1.Text = "&File";
			// 
			// menuFileSave
			// 
			this.menuFileSave.Index = 0;
			this.menuFileSave.Text = "&Save";
			this.menuFileSave.Click += new System.EventHandler(this.menuFileSave_Click);
			// 
			// menuFileLoad
			// 
			this.menuFileLoad.Index = 1;
			this.menuFileLoad.Text = "&Load";
			this.menuFileLoad.Click += new System.EventHandler(this.menuFileLoad_Click);
			// 
			// fileForceCloseMenuItem
			// 
			this.fileForceCloseMenuItem.Index = 2;
			this.fileForceCloseMenuItem.Text = "&Force close!";
			this.fileForceCloseMenuItem.Click += new System.EventHandler(this.fileForceCloseMenuItem_Click);
			// 
			// optionsMenuItem
			// 
			this.optionsMenuItem.Index = 1;
			this.optionsMenuItem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																							this.optionSitesMenuItem,
																							this.optionsWeightRatio,
																							this.menuOptionsSitePriority,
																							this.optionsIrcMenuItem,
																							this.menuItem2,
																							this.menuOptionsGeneralSettings,
																							this.optionsReleaseGroupMenuItem,
																							this.optionsAlwaysOnTopMenuItem});
			this.optionsMenuItem.Text = "&Options";
			// 
			// optionSitesMenuItem
			// 
			this.optionSitesMenuItem.Index = 0;
			this.optionSitesMenuItem.Text = "&Site Settings";
			this.optionSitesMenuItem.Click += new System.EventHandler(this.optionSitesMenuItem_Click);
			// 
			// optionsWeightRatio
			// 
			this.optionsWeightRatio.Index = 1;
			this.optionsWeightRatio.Text = "Weight &Ratios";
			this.optionsWeightRatio.Click += new System.EventHandler(this.optionsWeightRatio_Click);
			// 
			// menuOptionsSitePriority
			// 
			this.menuOptionsSitePriority.Index = 2;
			this.menuOptionsSitePriority.Text = "Site &Priority";
			this.menuOptionsSitePriority.Click += new System.EventHandler(this.menuOptionsSitePriority_Click);
			// 
			// optionsIrcMenuItem
			// 
			this.optionsIrcMenuItem.Index = 3;
			this.optionsIrcMenuItem.Text = "&Irc Connection";
			this.optionsIrcMenuItem.Click += new System.EventHandler(this.optionsIrcMenuItem_Click);
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 4;
			this.menuItem2.Text = "Irc &Encryption";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// menuOptionsGeneralSettings
			// 
			this.menuOptionsGeneralSettings.Index = 5;
			this.menuOptionsGeneralSettings.Text = "&General settings";
			this.menuOptionsGeneralSettings.Click += new System.EventHandler(this.menuOptionsGeneralSettings_Click);
			// 
			// optionsReleaseGroupMenuItem
			// 
			this.optionsReleaseGroupMenuItem.Enabled = false;
			this.optionsReleaseGroupMenuItem.Index = 6;
			this.optionsReleaseGroupMenuItem.Text = "&Release Groups";
			this.optionsReleaseGroupMenuItem.Click += new System.EventHandler(this.optionsReleaseGroupMenuItem_Click);
			// 
			// optionsAlwaysOnTopMenuItem
			// 
			this.optionsAlwaysOnTopMenuItem.Index = 7;
			this.optionsAlwaysOnTopMenuItem.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
			this.optionsAlwaysOnTopMenuItem.Text = "&Always on top";
			this.optionsAlwaysOnTopMenuItem.Click += new System.EventHandler(this.optionsAlwaysOnTopMenuItem_Click);
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 2;
			this.menuItem3.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.actionsExecuteOnSelected,
																					  this.disconnectSelectedSitesMenuItem,
																					  this.actionSkipCurrentFileInSelectedTransfer,
																					  this.actionCleanDeadSites});
			this.menuItem3.Text = "&Actions";
			// 
			// actionsExecuteOnSelected
			// 
			this.actionsExecuteOnSelected.Index = 0;
			this.actionsExecuteOnSelected.Shortcut = System.Windows.Forms.Shortcut.CtrlX;
			this.actionsExecuteOnSelected.Text = "E&xecute Command On Selected Sites";
			this.actionsExecuteOnSelected.Click += new System.EventHandler(this.actionsExecuteOnSelected_Click);
			// 
			// disconnectSelectedSitesMenuItem
			// 
			this.disconnectSelectedSitesMenuItem.Index = 1;
			this.disconnectSelectedSitesMenuItem.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
			this.disconnectSelectedSitesMenuItem.Text = "&Disconnect Selected Sites";
			this.disconnectSelectedSitesMenuItem.Click += new System.EventHandler(this.disconnectSelectedSitesMenuItem_Click);
			// 
			// actionSkipCurrentFileInSelectedTransfer
			// 
			this.actionSkipCurrentFileInSelectedTransfer.Index = 2;
			this.actionSkipCurrentFileInSelectedTransfer.Text = "&Skip current file in selected transfer";
			this.actionSkipCurrentFileInSelectedTransfer.Click += new System.EventHandler(this.actionSkipCurrentFileInSelectedTransfer_Click);
			// 
			// actionCleanDeadSites
			// 
			this.actionCleanDeadSites.Index = 3;
			this.actionCleanDeadSites.Text = "Re-activate (retry) sites marked as \"dead\"";
			this.actionCleanDeadSites.Click += new System.EventHandler(this.actionCleanDeadSites_Click);
			// 
			// raceList
			// 
			this.raceList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.raceList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																					   this.raceHeaderSectionName,
																					   this.raceHeaderReleaseName,
																					   this.raceHeaderProgress,
																					   this.raceHeaderStartTime,
																					   this.raceHeaderState});
			this.raceList.ContextMenu = this.raceListContextMenu;
			this.raceList.FullRowSelect = true;
			this.raceList.GridLines = true;
			this.raceList.Location = new System.Drawing.Point(8, 8);
			this.raceList.MultiSelect = false;
			this.raceList.Name = "raceList";
			this.raceList.Size = new System.Drawing.Size(856, 144);
			this.raceList.TabIndex = 2;
			this.raceList.View = System.Windows.Forms.View.Details;
			this.raceList.KeyUp += new System.Windows.Forms.KeyEventHandler(this.raceList_KeyUp);
			this.raceList.SelectedIndexChanged += new System.EventHandler(this.raceList_SelectedIndexChanged);
			// 
			// raceHeaderSectionName
			// 
			this.raceHeaderSectionName.Text = "Section";
			this.raceHeaderSectionName.Width = 72;
			// 
			// raceHeaderReleaseName
			// 
			this.raceHeaderReleaseName.Text = "Release Name";
			this.raceHeaderReleaseName.Width = 411;
			// 
			// raceHeaderProgress
			// 
			this.raceHeaderProgress.Text = "Progress";
			this.raceHeaderProgress.Width = 68;
			// 
			// raceHeaderStartTime
			// 
			this.raceHeaderStartTime.Text = "StartTime";
			this.raceHeaderStartTime.Width = 70;
			// 
			// raceHeaderState
			// 
			this.raceHeaderState.Text = "State";
			this.raceHeaderState.Width = 202;
			// 
			// raceListContextMenu
			// 
			this.raceListContextMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																								this.rlcmDeleteRaceMenuItem,
																								this.rlcmPromoteRaceMenuItem,
																								this.rlcmDemoteRaceMenuItem,
																								this.cmrlForcekillRace,
																								this.cmrlPanicBackfillRemove});
			// 
			// rlcmDeleteRaceMenuItem
			// 
			this.rlcmDeleteRaceMenuItem.Index = 0;
			this.rlcmDeleteRaceMenuItem.Text = "Delete (After next file has completed transfer)";
			this.rlcmDeleteRaceMenuItem.Click += new System.EventHandler(this.rlcmDeleteRaceMenuItem_Click);
			// 
			// rlcmPromoteRaceMenuItem
			// 
			this.rlcmPromoteRaceMenuItem.Index = 1;
			this.rlcmPromoteRaceMenuItem.Text = "Promote";
			this.rlcmPromoteRaceMenuItem.Click += new System.EventHandler(this.rlcmPromoteRaceMenuItem_Click);
			// 
			// rlcmDemoteRaceMenuItem
			// 
			this.rlcmDemoteRaceMenuItem.Index = 2;
			this.rlcmDemoteRaceMenuItem.Text = "Demote";
			this.rlcmDemoteRaceMenuItem.Click += new System.EventHandler(this.rlcmDemoteRaceMenuItem_Click);
			// 
			// cmrlForcekillRace
			// 
			this.cmrlForcekillRace.Index = 3;
			this.cmrlForcekillRace.Text = "Forcekill race (terminating all transfers)";
			this.cmrlForcekillRace.Click += new System.EventHandler(this.cmrlForcekillRace_Click);
			// 
			// connectToIrcButton
			// 
			this.connectToIrcButton.Enabled = false;
			this.connectToIrcButton.Location = new System.Drawing.Point(8, 208);
			this.connectToIrcButton.Name = "connectToIrcButton";
			this.connectToIrcButton.Size = new System.Drawing.Size(104, 24);
			this.connectToIrcButton.TabIndex = 3;
			this.connectToIrcButton.Text = "Connect to irc";
			this.connectToIrcButton.Click += new System.EventHandler(this.button1_Click);
			// 
			// connectToSitesButton
			// 
			this.connectToSitesButton.Location = new System.Drawing.Point(8, 176);
			this.connectToSitesButton.Name = "connectToSitesButton";
			this.connectToSitesButton.Size = new System.Drawing.Size(104, 24);
			this.connectToSitesButton.TabIndex = 4;
			this.connectToSitesButton.Text = "Connect to sites";
			this.connectToSitesButton.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(8, 240);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(104, 24);
			this.button3.TabIndex = 5;
			this.button3.Text = "Save Settings";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// log
			// 
			this.log.Location = new System.Drawing.Point(120, 176);
			this.log.Multiline = true;
			this.log.Name = "log";
			this.log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.log.Size = new System.Drawing.Size(392, 200);
			this.log.TabIndex = 8;
			this.log.Text = "";
			// 
			// connectedSites
			// 
			this.connectedSites.IntegralHeight = false;
			this.connectedSites.Location = new System.Drawing.Point(672, 176);
			this.connectedSites.Name = "connectedSites";
			this.connectedSites.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this.connectedSites.Size = new System.Drawing.Size(104, 200);
			this.connectedSites.Sorted = true;
			this.connectedSites.TabIndex = 10;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(680, 152);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(88, 24);
			this.label2.TabIndex = 11;
			this.label2.Text = "Connected Sites";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(272, 152);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(72, 24);
			this.label1.TabIndex = 18;
			this.label1.Text = "Race details";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(768, 152);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(112, 24);
			this.label3.TabIndex = 20;
			this.label3.Text = "Checked-out Sites";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// checkedOutSites
			// 
			this.checkedOutSites.IntegralHeight = false;
			this.checkedOutSites.Location = new System.Drawing.Point(776, 176);
			this.checkedOutSites.Name = "checkedOutSites";
			this.checkedOutSites.SelectionMode = System.Windows.Forms.SelectionMode.None;
			this.checkedOutSites.Size = new System.Drawing.Size(88, 200);
			this.checkedOutSites.Sorted = true;
			this.checkedOutSites.TabIndex = 19;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.dupeDB);
			this.panel1.Controls.Add(this.semiAuto);
			this.panel1.Controls.Add(this.fullAuto);
			this.panel1.Location = new System.Drawing.Point(8, 304);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(104, 80);
			this.panel1.TabIndex = 21;
			// 
			// dupeDB
			// 
			this.dupeDB.Location = new System.Drawing.Point(8, 56);
			this.dupeDB.Name = "dupeDB";
			this.dupeDB.Size = new System.Drawing.Size(88, 24);
			this.dupeDB.TabIndex = 2;
			this.dupeDB.Text = "dupedb only";
			this.dupeDB.CheckedChanged += new System.EventHandler(this.dupeDB_CheckedChanged);
			// 
			// semiAuto
			// 
			this.semiAuto.Checked = true;
			this.semiAuto.Location = new System.Drawing.Point(8, 32);
			this.semiAuto.Name = "semiAuto";
			this.semiAuto.Size = new System.Drawing.Size(80, 24);
			this.semiAuto.TabIndex = 1;
			this.semiAuto.TabStop = true;
			this.semiAuto.Text = "ask";
			this.semiAuto.CheckedChanged += new System.EventHandler(this.semiAuto_CheckedChanged);
			// 
			// fullAuto
			// 
			this.fullAuto.Location = new System.Drawing.Point(8, 8);
			this.fullAuto.Name = "fullAuto";
			this.fullAuto.Size = new System.Drawing.Size(80, 24);
			this.fullAuto.TabIndex = 0;
			this.fullAuto.Text = "auto-trade";
			this.fullAuto.CheckedChanged += new System.EventHandler(this.fullAuto_CheckedChanged);
			// 
			// showLogButton
			// 
			this.showLogButton.Location = new System.Drawing.Point(8, 272);
			this.showLogButton.Name = "showLogButton";
			this.showLogButton.Size = new System.Drawing.Size(104, 24);
			this.showLogButton.TabIndex = 22;
			this.showLogButton.Text = "Show Log";
			this.showLogButton.Click += new System.EventHandler(this.showLogButton_Click);
			// 
			// transfersListView
			// 
			this.transfersListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.transfersListView.CausesValidation = false;
			this.transfersListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																								this.columnHeader1,
																								this.columnHeader3,
																								this.columnHeader2,
																								this.columnHeader4,
																								this.columnHeader5,
																								this.columnHeader6});
			this.transfersListView.FullRowSelect = true;
			this.transfersListView.GridLines = true;
			this.transfersListView.Location = new System.Drawing.Point(8, 392);
			this.transfersListView.MultiSelect = false;
			this.transfersListView.Name = "transfersListView";
			this.transfersListView.Size = new System.Drawing.Size(856, 112);
			this.transfersListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
			this.transfersListView.TabIndex = 25;
			this.transfersListView.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Source -> Destination";
			this.columnHeader1.Width = 114;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Filename";
			this.columnHeader3.Width = 337;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Filesize";
			this.columnHeader2.Width = 77;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "Last speed";
			this.columnHeader4.Width = 82;
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "Transfer ETA";
			this.columnHeader5.Width = 80;
			// 
			// columnHeader6
			// 
			this.columnHeader6.Text = "Files in Queue";
			this.columnHeader6.Width = 83;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(320, 376);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(80, 16);
			this.label5.TabIndex = 26;
			this.label5.Text = "Transfers";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// joinedChannels
			// 
			this.joinedChannels.CheckOnClick = true;
			this.joinedChannels.IntegralHeight = false;
			this.joinedChannels.Location = new System.Drawing.Point(512, 176);
			this.joinedChannels.Name = "joinedChannels";
			this.joinedChannels.Size = new System.Drawing.Size(160, 200);
			this.joinedChannels.Sorted = true;
			this.joinedChannels.TabIndex = 27;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(544, 152);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(96, 24);
			this.label6.TabIndex = 28;
			this.label6.Text = "Joined Channels";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// joinedChannelsHelp
			// 
			this.joinedChannelsHelp.Location = new System.Drawing.Point(648, 152);
			this.joinedChannelsHelp.Name = "joinedChannelsHelp";
			this.joinedChannelsHelp.Size = new System.Drawing.Size(16, 24);
			this.joinedChannelsHelp.TabIndex = 29;
			this.joinedChannelsHelp.Text = "?";
			this.joinedChannelsHelp.Click += new System.EventHandler(this.joinedChannelsHelp_Click);
			// 
			// statusBar
			// 
			this.statusBar.Location = new System.Drawing.Point(0, 511);
			this.statusBar.Name = "statusBar";
			this.statusBar.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
																						 this.ircActivityTextPanel,
																						 this.ircActivityIconPanel,
																						 this.timePanel});
			this.statusBar.ShowPanels = true;
			this.statusBar.Size = new System.Drawing.Size(872, 22);
			this.statusBar.SizingGrip = false;
			this.statusBar.TabIndex = 30;
			this.statusBar.PanelClick += new System.Windows.Forms.StatusBarPanelClickEventHandler(this.statusBar_PanelClick);
			// 
			// ircActivityTextPanel
			// 
			this.ircActivityTextPanel.Text = "  Irc Activity";
			this.ircActivityTextPanel.Width = 62;
			// 
			// ircActivityIconPanel
			// 
			this.ircActivityIconPanel.Icon = ((System.Drawing.Icon)(resources.GetObject("ircActivityIconPanel.Icon")));
			this.ircActivityIconPanel.Width = 25;
			// 
			// timePanel
			// 
			this.timePanel.Alignment = System.Windows.Forms.HorizontalAlignment.Right;
			this.timePanel.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
			this.timePanel.Text = "Time: 00:00:00  ";
			this.timePanel.Width = 785;
			// 
			// cmrlPanicBackfillRemove
			// 
			this.cmrlPanicBackfillRemove.Index = 4;
			this.cmrlPanicBackfillRemove.Text = "Panic remove (race was backfill, removes files and dir (if we can remove all the " +
				"files first))";
			this.cmrlPanicBackfillRemove.Click += new System.EventHandler(this.cmrlPanicBackfillRemove_Click);
			// 
			// ClientGui
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(872, 533);
			this.Controls.Add(this.statusBar);
			this.Controls.Add(this.joinedChannelsHelp);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.joinedChannels);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.transfersListView);
			this.Controls.Add(this.showLogButton);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.checkedOutSites);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.connectedSites);
			this.Controls.Add(this.log);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.connectToSitesButton);
			this.Controls.Add(this.connectToIrcButton);
			this.Controls.Add(this.raceList);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Location = new System.Drawing.Point(500, 500);
			this.Menu = this.mainMenu;
			this.Name = "ClientGui";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Tiki";
			this.Resize += new System.EventHandler(this.ClientGui_Resize);
			this.Closing += new System.ComponentModel.CancelEventHandler(this.ClientGui_Closing);
			this.Load += new System.EventHandler(this.ClientGui_Load);
			this.Move += new System.EventHandler(this.ClientGui_Move);
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ClientGui_KeyUp);
			this.Activated += new System.EventHandler(this.ClientGui_Activated);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.ircActivityTextPanel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ircActivityIconPanel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.timePanel)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion


		private void UpdateConnectedSitesListThread()
		{

			// todo: add some kind of irc status thing here aswell. like the channels joined or whatever

			int i = 0;
			while (!shuttingDown) 
			{
				i++;
				Thread.Sleep(1000);
				this.timePanel.Text = "Time: " + System.DateTime.Now.TimeOfDay.ToString().Substring(0,8) + "   ";
				if (i == 5) 
				{
					i = 0;
					foreach (FtpConnection s in ConnectionPool.Connections.Values)
					{
						if (s.Connected) 
						{
							if (!connectedSites.Items.Contains(s)) 
							{
								connectedSites.Items.Add(s);
							}
						}
						else
						{
							if (connectedSites.Items.Contains(s))
							{
								connectedSites.Items.Remove(s);
							}
						}
					}

					checkedOutSites.Items.Clear();
					foreach (string s in ConnectionPool.CheckedOutConnections.Keys)
					{
						checkedOutSites.Items.Add(s);
					}
				}
			}
		}

		private void UpdateTransferStatusThread()
		{
			while (!shuttingDown)
			{
				Thread.Sleep(1000);
				if (!"".Equals(currentlySelectedRaceName)) 
				{
					Race race = engine.GetRace(currentlySelectedRaceName);
					UpdateTransferListView(race);
				}
			}
		}

		private void ClientGui_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			Shutdown();
		}

		private void menuOptionsSitePriority_Click(object sender, System.EventArgs e)
		{
			// add thing here about using it or not /radio button (on or off)
			// and a list with arrows pointing up and down that moves the added sites within the list.
			// also enable dragging them up and down.
			SiteRankingGui srg = new SiteRankingGui(engine);
			// todo: srg.showDialog(); and such
			srg.ShowDialog();
		}

		private void optionsIrcMenuItem_Click(object sender, System.EventArgs e)
		{
			IrcSettings ircs = new IrcSettings(Config.IrcConnectionSettings);
			DialogResult dr = ircs.ShowDialog();
			/*if (dr == DialogResult.OK)
			{
				Config.IrcConnectionSettings = new IrcConnectionSettings(ircs.ServerHostName, Convert.ToInt32(ircs.ServerPort), ircs.Username, ircs.Nickname, ircs.Password, ircs.Ssl);
			}*/
		}

		private void optionSitesMenuItem_Click(object sender, System.EventArgs e)
		{
			SiteSettingsGui ssg = new SiteSettingsGui();
			DialogResult dr = ssg.ShowDialog();
			// check OK and such later

		}
		
		private void button1_Click(object sender, System.EventArgs e)
		{
			if (engine.ConnectedToIrc)
			{
				engine.ShutdownIrcConnection();
				connectToIrcButton.Text = "Connect to Irc";
			}
			else
			{
				Thread t = new Thread(new ThreadStart(engine.ConnectToIrc));
				t.Name = "ConnectToIrc";
				t.IsBackground = true;
				t.Start();
				connectToIrcButton.Text = "Disconnect Irc";	
			}
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			if (serverEngineCreated)
			{
				connectToSitesButton.Text = "Connect to Sites";
				engine.ShutdownServerEngine();
				serverEngineCreated = false;
			}
			else
			{
				Thread t = new Thread(new ThreadStart(engine.CreateServerEngine));
				t.Name = "CreateServerEngine";
				t.IsBackground = true;
				t.Start();
				this.connectToIrcButton.Enabled = true;
				serverEngineCreated = true;
				connectToSitesButton.Text = "Disconnect Sites";	
			}
			
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			IrcChannelBlowfishKeys irbk = new IrcChannelBlowfishKeys();
			irbk.ShowDialog();
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			Config.Save();	
		}

		private void ClientGui_Load(object sender, System.EventArgs e)
		{
			connectedSitesChecker = new Thread(new ThreadStart(this.UpdateConnectedSitesListThread));
			connectedSitesChecker.IsBackground = true;
			connectedSitesChecker.Start();

			updateTransferStatusTread = new Thread(new ThreadStart(this.UpdateTransferStatusThread));
			updateTransferStatusTread.IsBackground = true;
			updateTransferStatusTread.Start();

			engine = new ClientEngine(this);

			news =  "News: " + Environment.NewLine + 
					"force-kill works" + Environment.NewLine + 
					"normal-kill might not work (test it)" + Environment.NewLine + 
					"check out options->weight ratios" + Environment.NewLine + 
					"site ranking is now useable";

			log.Text = news;

			this.Text = title;
		}
		private void Shutdown()
		{
			// only give the engine a certain amount of time before we force-quit
			new Thread(new ThreadStart(this.ForceQuit)).Start();
			
			connectedSitesChecker.Abort();
			updateTransferStatusTread.Abort();
			engine.Shutdown();
			Environment.Exit(0);
		}

		private void ForceQuit()
		{
			Logger.Log("Force-quitting in 8 seconds!");
			Thread.Sleep(8000);
			Environment.Exit(0);
		}
		
		private void ShowTradeAnnounce()
		{
			tradeAnnounceOpen = true;
			tradeAnnounce.ShowDialog();
			tradeAnnounceOpen = false;
		}
		
		private void menuFileSave_Click(object sender, System.EventArgs e)
		{
			Config.Save();
		}

		private void menuFileLoad_Click(object sender, System.EventArgs e)
		{
			Config.Load();
		}

		private void raceList_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (raceList.SelectedItems.Count == 1) {
				currentlySelectedRaceName = raceList.SelectedItems[0].SubItems[1].Text;
				//currentlySelectedRaceName = raceList.SelectedItems[0].Text;
				Race race = engine.GetRace(currentlySelectedRaceName);
				ShowDetailedProgress(race);
			}
		}

		private void menuOptionsGeneralSettings_Click(object sender, System.EventArgs e)
		{
			GeneralSettings gs = new GeneralSettings();
			gs.ShowDialog();
		}

		private void dupeDB_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!this.connectToIrcButton.Enabled) 
			{
				this.connectToIrcButton.Enabled = this.dupeDB.Checked;
			}
		}

		private void optionsAlwaysOnTopMenuItem_Click(object sender, System.EventArgs e)
		{
			this.TopMost = !this.TopMost;
			optionsAlwaysOnTopMenuItem.Checked = !optionsAlwaysOnTopMenuItem.Checked;
		}

		private void showLogButton_Click(object sender, System.EventArgs e)
		{
			if (loggerInvokeNeeded)
			{
				loggerInvokeNeeded = false;
				logger = new FloatingLogger(this);
			}
			logger.Visible = !logger.Visible;
			showLogButton.Text = (logger.Visible ? "Hide" : "Show") + " Log";
			if (logger.Visible) 
			{
				logger.Width = this.Width;
				logger.Location = new Point(this.Location.X, this.Location.Y + this.Height + 5);
			}
		}

		private void ClientGui_Move(object sender, System.EventArgs e)
		{
			if (logger.Visible && this.WindowState != FormWindowState.Maximized)
			{
				logger.Location = new Point(this.Location.X, this.Location.Y + this.Height + 2);
			}
		}

		private void ClientGui_Resize(object sender, System.EventArgs e)
		{
			if (logger.Visible && this.WindowState != FormWindowState.Maximized)
			{
				logger.Location = new Point(this.Location.X, this.Location.Y + this.Height + 5);
			}
		}

		private void ClientGui_Activated(object sender, System.EventArgs e)
		{
		}

		private void fileForceCloseMenuItem_Click(object sender, System.EventArgs e)
		{
			// NOTE: this disregards all current site connections and transfers and such
			Environment.Exit(0);
		}

		private void RemoveSelectedRace(bool force) 
		{
			lock (raceUpdateLock) 
			{
				if (raceList.SelectedItems.Count == 1) 
				{
					currentlySelectedRaceName = raceList.SelectedItems[0].SubItems[1].Text;
					Race race = engine.GetRace(currentlySelectedRaceName);
					if (!race.InUse)
					{
						ListViewItem oldLvi = (ListViewItem)raceListings[race.ReleaseName];
						raceList.Items.Remove(oldLvi);
						raceListings.Remove(race.ReleaseName);
						log.Clear();
						engine.RemoveRaceFromQueue(race);
					}
					else
					{
						Logger.Log((force ? "Force " : "") + "terminating race from context menu in gui");
						RaceTerminator rt = new RaceTerminator(force, race);
						Thread t = new Thread(new ThreadStart(rt.Terminate));
						t.IsBackground = true;
						t.Name = "RaceTerminator:" + race.ReleaseName;
						t.Start();
					}
				}
			}
		}

		private void rlcmDeleteRaceMenuItem_Click(object sender, System.EventArgs e)
		{
			RemoveSelectedRace(false);
		}

		private void rlcmPromoteRaceMenuItem_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("Not Implemented Yet");
		}

		private void rlcmDemoteRaceMenuItem_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("Not Implemented Yet");
		}

		private void optionsReleaseGroupMenuItem_Click(object sender, System.EventArgs e)
		{
			// �ppna en dialog d�r man kan titta p� probability f�r olika releasegroups och v�lja om man alltid ska behandla en grupp som en viss sektion.
			MessageBox.Show("Not Implemented Yet");
		}

		private void semiAuto_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!serverEngineCreated)
			{
				MessageBox.Show("You must click the 'Connect to Sites' button first!");
			}
		}

		private void fullAuto_CheckedChanged(object sender, System.EventArgs e)
		{
			if (!serverEngineCreated)
			{
				MessageBox.Show("You must click the 'Connect to Sites' button first!");
			}
		}

		private void UpdateTransferListView(Race race)
		{
			lock (transfersListView)
			{
				
				if (race == null)
				{
					transfersListView.Items.Clear();
					return;
				}
			
				try
				{
					lock (race.Transfers.SyncRoot) 
					{
						// for some odd reason, this clear doesn't always work
						// even when we KNOW (because we've checked != null) that it's not null, we still get a null pointer on this very line!:
						transfersListView.Items.Clear();
						foreach (DictionaryEntry transfer in race.Transfers)
						{

							// NOTE:
							// we can get estimated time remaining for the entire queue, since we know the size of each file from the dir listing
							// todo: make it happen
							// also with the progress bars

							FxpTransfer fxpt = (FxpTransfer)transfer.Value;
							// we must check this here, since this is a clones version of the rable

							if (fxpt != null && fxpt.Filename != null && fxpt.Filename.Length > 0) 
							{
								string speed = (fxpt.SpeedOfLastTransfer / 1024.0).ToString();
								string speedSuffix = "k";
								if ((fxpt.SpeedOfLastTransfer / 1024.0) > 1024)
								{
									speed = ((fxpt.SpeedOfLastTransfer / 1024.0) / 1024).ToString();
									speedSuffix = "M";
								}
								string size = (fxpt.CurrentFileSize / 1024.0).ToString();
								string sizeSuffix = "k";
								if ((fxpt.CurrentFileSize / 1024.0) > 1024)
								{
									size = ((fxpt.CurrentFileSize / 1024.0) / 1024).ToString();
									sizeSuffix = "M";
								}
								double transferTime = Convert.ToDouble(fxpt.CurrentFileSize / fxpt.SpeedOfLastTransfer);
								string eta = "N/A";
								if (transferTime > 0 && !double.IsInfinity(transferTime) && !double.IsNaN(transferTime))
								{
									// endtime - now = time left of transfer
									TimeSpan timeLeft = fxpt.CurrentFileTransferStartTime.AddSeconds(transferTime).Subtract(System.DateTime.Now);
									if (timeLeft.Ticks > 0) 
									{
										// todo: pad with zeroes
										if (timeLeft.Hours > 0)
										{
											eta = (timeLeft.Hours < 10 ? "0" : "") + timeLeft.Hours + ":" + (timeLeft.Minutes < 10 ? "0" : "") + timeLeft.Minutes + ":" + (timeLeft.Seconds < 10 ? "0" : "") + timeLeft.Seconds;
										} 
										else 
										{
											eta = (timeLeft.Minutes < 10 ? "0" : "") + timeLeft.Minutes + ":" + (timeLeft.Seconds < 10 ? "0" : "") + timeLeft.Seconds;
										}
									}
								}
								// todo: change end time to time left, and update every second
								if (size.IndexOf('.') > -1)
								{
									size = size.Substring(0, size.IndexOf('.') + 2);

								}
								if (speed.IndexOf('.') > -1)
								{
									speed = speed.Substring(0, speed.IndexOf('.') + 2);
								}

								//
								// TODO: change this display to time remaining for all the time displays (there is a thread that will update this as time goes by)
								//

								// the queue eta needs more calculation

								ListViewItem lvi = new ListViewItem(new string[] {
																					 transfer.Key.ToString(),
																					 fxpt.Filename,
																					 size + " " + sizeSuffix + "B",
																					 speed + " " + speedSuffix + "B/s.",
																					 eta, 
/*																					 fxpt.QueueStartTime.TimeOfDay.ToString().Substring(0, 8), */
																					 fxpt.Queue.Count.ToString()
																				 });
								transfersListView.Items.Add(lvi);
								// queue end time is only available if we know all the file sizes, and we don't feel like counting al that up right now;

							}
						}
					}	
				}
				catch (Exception e)
				{
					// we shouldn't ever end up here now, but since we still might, we're putting this here
					Logger.Log(e.Message);
					Logger.Log(e.StackTrace);
				}
			}
		}
		private void ShowDetailedProgress(Race race)
		{
			// todo: put total trading speed for this race in the details box
			// also, the only detail in transfers that we care about is how long it takes until the current transfer finishes, so change it to that, and 
			// have it update every second
			if (race == null)
			{
				return;
			}
			if (race.Complete)
			{
				log.Clear();
				transfersListView.Items.Clear();
				return;
			}
			// todo: turn this into something better, like with a progress bar and stuff
			// todo: show how long is left, in seconds, of the current filee being transfered!
			log.Text = "";
			log.AppendText("Release name: " + race.ReleaseName + Environment.NewLine);
			log.AppendText("State: " + race.State + Environment.NewLine);
			log.AppendText("Sites: " + Environment.NewLine);
			foreach (SiteConnection sc in race.Connections.Values)
			{
				// todo: make these strings look alot nicer.
				// todo: make it get the higest number of available files as the complete number, otherwise it'll say 17/14 and stuff like that
			
				double completeness = 0.0d;
				if (race.NumberOfFilesInSfv != 0)
				{
					completeness = Convert.ToDouble(race.GetProgress(sc.Site.Name)) / (Convert.ToDouble(race.NumberOfFilesInSfv)) * 100.0;	
				}
				log.AppendText(sc.Site.Name + ": " + Math.Max(sc.Destination.FileList.Count, sc.Source.FileList.Count) + "/" + race.NumberOfFilesInSfv + " ( " + completeness.ToString().Substring(0, Math.Min(completeness.ToString().Length, 5)) + " % )" + Environment.NewLine);
			}
			//log.AppendText(Environment.NewLine + "Transfers: " + Environment.NewLine);
			UpdateTransferListView(race);
		
		}

		
		public void IrcActivity()
		{
			
			int blinkTimeout = 200;
			if ((lastIrcActivity.Millisecond - System.DateTime.Now.Millisecond) < 200)
			{
				blinkTimeout = 30;
			}
			lastIrcActivity = System.DateTime.Now;
			Icon i = ircActivityIconPanel.Icon;
			ircActivityIconPanel.Icon = null;
			Thread.Sleep(blinkTimeout);
			ircActivityIconPanel.Icon = i;
/*
			this.ircActivityImage.Hide();
			Thread.Sleep(blinkTimeout);
			this.ircActivityImage.Show();
*/
		}

		public void Announce(RaceAnnounce announce)
		{
			// check the announcetype
			// if it was a nuke, update the dupedb to reflect that state, and stop racing that release somehow (race.Terminate();)
			DupeEntry dupe = DupeDB.GetDupe(announce.ReleaseName);
			if (announce.AnnounceType.Equals("nuke"))
			{
				string reason = "unknown";
				// do something
				if (dupe != null)
				{
					if (!dupe.IsNuked)
					{
						int reasonIndex = announce.AnnounceLine.ToLower().IndexOf("reason:");
						if (reasonIndex > -1)
						{
							int nukeesIndex = announce.AnnounceLine.ToLower().LastIndexOf("nukees");
							/*
							 * An unhandled exception of type 'System.ArgumentOutOfRangeException' occurred in mscorlib.dll

								Additional information: Index and length must refer to a location within the string.
							*/
//							reason = announce.AnnounceLine.Substring(reasonIndex + 7, announce.AnnounceLine.Length - nukeesIndex);
							reason = announce.AnnounceLine.Substring(reasonIndex + 7);
						}
						dupe.Nuke(reason);
					}
				}
				Logger.Log("Release " + announce.ReleaseName + " was nuked on " + announce.Source.Name + " because: " + reason);
				// todo: ask if we want to abort the entire race
				// for now, only abort it on the site in question
				engine.Nuke(announce, false);
			}
			else
			{
				if (this.fullAuto.Checked)
				{
					InitiateRace(announce);
					Logger.Log("Autotrading " + announce.ReleaseName);
				}
				else if (this.semiAuto.Checked) 
				{
					
					Logger.Log("Annoucing: " + announce.ReleaseName + " from " + announce.Source.Name);
					// only announce it if we don't already have this release.
					if (!announces.Contains(announce.ReleaseName)) 
					{
						announces[announce.ReleaseName] = System.DateTime.Now;
						// NOTE: for some damn reason, this dialog box craps out when it is opened here. butnot where it is opened elsewhere
						// the refresh fixes the looks, however it still hangs. and if we know why it hangs, we don't need the refresh for the looks

						// todo: check if we need to re-create this object because of it having been closed bythe [x]
						if (!tradeAnnounceOpen) 
						{
							Thread t = new Thread(new ThreadStart(this.ShowTradeAnnounce));
							t.Start();
						}
						tradeAnnounce.AddAnnounce(announce);
						// this last one was just added. if it fucks things up, remove it.
						tradeAnnounce.Activate();
					}
				
				}
				else
				{
					// dupe db only, do thing here
				}
			}
		}
		public void InitiateRace(RaceAnnounce announce)
		{
			// todo: l�gg till racet i listan redan h�r


			// this is called by the TradeAnnounce class.
			// it starts and lists the race
			Logger.Log("starting race: " + announce.ReleaseName);
			Cursor.Current = Cursors.WaitCursor;
			engine.CreateNewRace(announce);
			Cursor.Current = Cursors.Default;

			// no need to list the race, the engine calls back with an UpdateRace(race)
			// when it's done, which takes ages!
			// todo: list it right away
			// fixa n�tt slags fusk-"waiting to start" entry
		}

		public void UpdateRace(Race race)
		{
			// todo: we might want to change this, the removing and adding is messing up the fact that we want the race to remain selected
			ListViewItem newLvi = new ListViewItem(new string[] {
				race.Announce.Section,
				race.ReleaseName,
				race.NumberOfCompleteSites + "/" + race.NumberOfSites,
				race.RaceStart.TimeOfDay.ToString().Substring(0, 8),
				race.State
			});
			// (race.RaceStart.Hour > 9 ? race.RaceStart.Hour.ToString() : "0" + race.RaceStart.Hour.ToString()) + ":" + (race.RaceStart.Minute > 9 ? race.RaceStart.Minute.ToString() : "0" + race.RaceStart.Minute.ToString()) + ":" + (race.RaceStart.Second > 9 ? race.RaceStart.Second.ToString() : "0" + race.RaceStart.Second.ToString()),
			lock (raceUpdateLock) 
			{

				ListViewItem oldLvi = (ListViewItem)raceListings[race.ReleaseName];
			
				int index = raceList.Items.IndexOf(oldLvi);
				if (index > -1)
				{
					// this means we have the item.
					if (race.Complete) 
					{
						// this fails fairly often for some reason, so lets jsut do it the hard way
						//raceList.Items.RemoveAt(index);
						// which probably does a lookup and removes at that index none the less.
						// we must remove the new one now, since weve updated.
						raceList.Items.Remove(oldLvi);
						raceListings.Remove(race.ReleaseName);
						log.Clear();
					}
					else
					{
						raceList.Items[index] = newLvi;
						raceListings[race.ReleaseName] = newLvi;
					}
				}
				else
				{
					// this means it's a new item
					raceListings[race.ReleaseName] = newLvi;
					raceList.Items.Add(newLvi);
				}
				// todo: check: do we need this refresh?
				//raceList.Refresh();
				if (race.ReleaseName.Equals(currentlySelectedRaceName) || "".Equals(currentlySelectedRaceName)) 
				{
					newLvi.Selected = true;
					ShowDetailedProgress(race);
				}
				
			}
		}

		private void cmrlForcekillRace_Click(object sender, System.EventArgs e)
		{
			RemoveSelectedRace(true);
		}

		private void raceList_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			// if delete, then delete the race
			// if F5, then refresh the detailed display
			if (e.KeyCode == Keys.Delete)
			{
				// force = true if shift is pressed =)
				RemoveSelectedRace(e.Shift);
			}
			else if (e.KeyCode == Keys.F5)
			{
				if (raceList.SelectedItems.Count == 1) 
				{
					currentlySelectedRaceName = raceList.SelectedItems[0].Text;
					Race race = engine.GetRace(currentlySelectedRaceName);
					ShowDetailedProgress(race);
				}				
			}
			//e.Handled = true;		
		}

		private void ClientGui_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			MessageBox.Show(e.KeyCode.ToString());
			if (e.KeyCode == Keys.F5)
			{
				if (raceList.SelectedItems.Count == 1) 
				{
					currentlySelectedRaceName = raceList.SelectedItems[0].Text;
					Race race = engine.GetRace(currentlySelectedRaceName);
					ShowDetailedProgress(race);
				}				
			}
			else if (e.KeyCode == Keys.F6)
			{
				SiteSettingsGui ssg = new SiteSettingsGui();
				DialogResult dr = ssg.ShowDialog();
			}
			//e.Handled = true;
		}

		private void actionsExecuteOnSelected_Click(object sender, System.EventArgs e)
		{
			Hashtable sites = new Hashtable();
			
			if (connectedSites.SelectedItems.Count >= 1) 
			{
				for (int i = 0; i < connectedSites.SelectedItems.Count; i++)
				{
					FtpConnection ftpConnection = (FtpConnection) connectedSites.SelectedItems[i];
					if (!ftpConnection.Transferring)
					{
						sites[ftpConnection.Site.Name] = ftpConnection;
					}
				}
			}
			SimpleInput si = new SimpleInput("What command do you want to issue?", "Enter command", "");
			DialogResult dr = si.ShowDialog();
			if (dr == DialogResult.OK)
			{
				string command = si.Value;
				FloatingLogger fl = new FloatingLogger(null);
				fl.Hide();
				fl.Size = new Size(600, 400);
				foreach (FtpConnection ftpConnection in sites.Values)
				{
					FtpConnection.CommandResponse cr = ftpConnection.Execute(command);
					fl.Log(Environment.NewLine + ftpConnection.Site.Name + " : " + command + " : ");
					fl.Log(cr.Response);
				}
				fl.Show();
			}
		}

		private void ShowPoem()
		{
			MessageBox.Show(	"My Tiki" + Environment.NewLine + Environment.NewLine +  

				"Tonight I fire you up again" + Environment.NewLine +  
				"My jet, my Porsche, my rocket." + Environment.NewLine + 
				"And dutyfully you connect" + Environment.NewLine + 
				"To every unwilling socket." + Environment.NewLine + Environment.NewLine +  

				"Tonight you make me hot again" + Environment.NewLine + 
				"The bots, they can't keep up" + Environment.NewLine + 
				"I fly so high in ranks, you at my side," + Environment.NewLine + 
				"Leave the rest of the traders in the grub." + Environment.NewLine + Environment.NewLine + 

				"Tonight we ride the chans my friend," + Environment.NewLine + 
				"A lonely, gloryful quest," + Environment.NewLine + 
				"Untouched, unbeaten, not understood," + Environment.NewLine + 
				"My tiki... you are the best!" + Environment.NewLine + Environment.NewLine + 
				"By: matter", "Tiki Poem Very Powerful!");
		}

		private void disconnectSelectedSitesMenuItem_Click(object sender, System.EventArgs e)
		{
			if (connectedSites.SelectedItems.Count >= 1) 
			{
				Cursor.Current = Cursors.WaitCursor;
				for (int i = 0; i < connectedSites.SelectedItems.Count; i++)
				{
					FtpConnection ftpConnection = (FtpConnection) connectedSites.SelectedItems[i];
					if (!ftpConnection.Transferring)
					{
						ftpConnection.Disconnect(false);
					}
				}
				Cursor.Current = Cursors.Default;
			}
		}

		private void actionSkipCurrentFileInSelectedTransfer_Click(object sender, System.EventArgs e)
		{
			if (transfersListView.SelectedItems.Count == 1) 
			{
				currentlySelectedRaceName = raceList.SelectedItems[0].Text;
				Race race = engine.GetRace(currentlySelectedRaceName);
				string transferId = transfersListView.SelectedItems[0].Text;
				MessageBox.Show("transferId: " + transferId);
				lock (race.Transfers.SyncRoot)
				{
					foreach (DictionaryEntry transfer in race.Transfers)
					{
						if (transferId.Equals(transfer.Key.ToString()))
						{
							FxpTransfer fxpt = (FxpTransfer)transfer.Value;
							fxpt.SkipCurrentFile();
						}
					}
				}
			}
		}


		public bool LoggerInvokeNeeded
		{
			set 
			{ 
				loggerInvokeNeeded = value;
				showLogButton.Text = "Show Log";
			}
		}

		public void Join(string channel)
		{
			if (!joinedChannels.Items.Contains(channel.Substring(1))) 
			{
				joinedChannels.Items.Add(channel.Substring(1));
			}
		}

		private void joinedChannelsHelp_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("Check the channels that are \"site channels\" and (later) we'll automate the configuration of the channels for you." + Environment.NewLine + "We're also working on a better way of doing section settings, with a name list from the channels, connecting bots to sites and whatnot, stay tuned." + Environment.NewLine + "This list isn't updated to reflect kicks and parts");
		}

		private void statusBar_PanelClick(object sender, System.Windows.Forms.StatusBarPanelClickEventArgs e)
		{
			ShowPoem();
		}

		private void actionCleanDeadSites_Click(object sender, System.EventArgs e)
		{
			ConnectionPool.ClearAutoDeadSites();
		}

		private void optionsWeightRatio_Click(object sender, System.EventArgs e)
		{
			WeightRatio wr = new WeightRatio();
			wr.ShowDialog();
		}

		private void cmrlPanicBackfillRemove_Click(object sender, System.EventArgs e)
		{
			// pause or kill the race/transfers.
			// try to remove all the files present in the dir (doesn't matter if they're ours or not)
			// try to remove the dir for the race
			// terminate
		}
	}
}

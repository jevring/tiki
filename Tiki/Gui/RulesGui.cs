using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Tiki.Gui
{
	/// <summary>
	/// Summary description for RulesGui.
	/// </summary>
	public class RulesGui : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.TextBox alwaysAllowed;
		private System.Windows.Forms.Label alwaysAllowedLabel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.TextBox banned;

		private Section section;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox required;
		private System.Windows.Forms.TextBox allowedYear;
		private System.Windows.Forms.Button help;
		private System.Windows.Forms.Label label4;
		public RulesGui(Section section)
		{
			this.section = section;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(RulesGui));
			this.alwaysAllowed = new System.Windows.Forms.TextBox();
			this.alwaysAllowedLabel = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.allowedYear = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.banned = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.required = new System.Windows.Forms.TextBox();
			this.help = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// alwaysAllowed
			// 
			this.alwaysAllowed.AcceptsReturn = true;
			this.alwaysAllowed.Location = new System.Drawing.Point(112, 72);
			this.alwaysAllowed.Multiline = true;
			this.alwaysAllowed.Name = "alwaysAllowed";
			this.alwaysAllowed.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.alwaysAllowed.Size = new System.Drawing.Size(96, 264);
			this.alwaysAllowed.TabIndex = 0;
			this.alwaysAllowed.Text = "";
			// 
			// alwaysAllowedLabel
			// 
			this.alwaysAllowedLabel.Location = new System.Drawing.Point(112, 48);
			this.alwaysAllowedLabel.Name = "alwaysAllowedLabel";
			this.alwaysAllowedLabel.Size = new System.Drawing.Size(96, 16);
			this.alwaysAllowedLabel.TabIndex = 1;
			this.alwaysAllowedLabel.Text = "Always Allowed";
			this.alwaysAllowedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 352);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(168, 24);
			this.label1.TabIndex = 3;
			this.label1.Text = "Releases Allowed From Year :";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// allowedYear
			// 
			this.allowedYear.AcceptsReturn = true;
			this.allowedYear.Location = new System.Drawing.Point(176, 352);
			this.allowedYear.Name = "allowedYear";
			this.allowedYear.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.allowedYear.Size = new System.Drawing.Size(48, 20);
			this.allowedYear.TabIndex = 2;
			this.allowedYear.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(216, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(96, 16);
			this.label2.TabIndex = 5;
			this.label2.Text = "Banned Strings";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// banned
			// 
			this.banned.AcceptsReturn = true;
			this.banned.Location = new System.Drawing.Point(216, 72);
			this.banned.Multiline = true;
			this.banned.Name = "banned";
			this.banned.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.banned.Size = new System.Drawing.Size(96, 264);
			this.banned.TabIndex = 4;
			this.banned.Text = "";
			// 
			// button1
			// 
			this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.button1.Location = new System.Drawing.Point(8, 384);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(96, 24);
			this.button1.TabIndex = 6;
			this.button1.Text = "Ok";
			// 
			// button2
			// 
			this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button2.Location = new System.Drawing.Point(112, 384);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(96, 24);
			this.button2.TabIndex = 7;
			this.button2.Text = "Cancel";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 8);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(304, 32);
			this.label4.TabIndex = 9;
			this.label4.Text = "Do NOT include affils in the BANNED list, otherwise they won\'t get traded on othe" +
				"r sites either";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(0, 48);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(120, 16);
			this.label3.TabIndex = 11;
			this.label3.Text = "Required Strings (OR)";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// required
			// 
			this.required.AcceptsReturn = true;
			this.required.Location = new System.Drawing.Point(8, 72);
			this.required.Multiline = true;
			this.required.Name = "required";
			this.required.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.required.Size = new System.Drawing.Size(96, 264);
			this.required.TabIndex = 10;
			this.required.Text = "";
			// 
			// help
			// 
			this.help.Location = new System.Drawing.Point(216, 384);
			this.help.Name = "help";
			this.help.Size = new System.Drawing.Size(96, 24);
			this.help.TabIndex = 12;
			this.help.Text = "Help";
			this.help.Click += new System.EventHandler(this.help_Click);
			// 
			// RulesGui
			// 
			this.AcceptButton = this.button1;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.button2;
			this.ClientSize = new System.Drawing.Size(322, 423);
			this.Controls.Add(this.help);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.required);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.banned);
			this.Controls.Add(this.allowedYear);
			this.Controls.Add(this.alwaysAllowed);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.alwaysAllowedLabel);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "RulesGui";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Rules";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.RulesGui_Closing);
			this.Load += new System.EventHandler(this.RulesGui_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void RulesGui_Load(object sender, System.EventArgs e)
		{
			// populate fields
			for (int i = 0; i < section.AlwaysAllow.Count; i++)
			{
				string alwaysAllow = (string) section.AlwaysAllow[i];
				this.alwaysAllowed.Text += alwaysAllow + Environment.NewLine;
			}
			for (int i = 0; i < section.Required.Count; i++)
			{
				string req = (string) section.Required[i];
				this.required.Text += req + Environment.NewLine;
			}

			for (int i = 0; i < section.Banned.Count; i++)
			{
				string banned = (string) section.Banned[i];
				this.banned.Text += banned + Environment.NewLine;
			}
			
			if (section.AllowedYear != 0) 
			{
				this.allowedYear.Text = section.AllowedYear.ToString();
			}
			else
			{
				section.AllowedYear = System.DateTime.Now.Year;
			}
			this.Text = "Rules for section: " + section.Name + " on site " + section.Site.Name;
		}

		private void RulesGui_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			section.AlwaysAllow.Clear();
			section.AlwaysAllow.AddRange(this.alwaysAllowed.Lines);

			section.Required.Clear();
			section.Required.AddRange(this.required.Lines);

			section.Banned.Clear();
			section.Banned.AddRange(this.banned.Lines);
			try 
			{
				section.AllowedYear = Convert.ToInt32(this.allowedYear.Text);
				if (section.AllowedYear < 1900)
				{
					section.AllowedYear = System.DateTime.Now.Year;
				}
			} 
			catch
			{
				section.AllowedYear = System.DateTime.Now.Year;
			}
		}

		private void help_Click(object sender, System.EventArgs e)
		{
			MessageBox.Show("The Required Strings are in OR mode");
		}
	}
}

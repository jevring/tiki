using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Tiki.Gui
{
	/// <summary>
	/// Summary description for WeightRation.
	/// </summary>
	public class WeightRatio : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TrackBar siteSpeedTrackbar;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TrackBar sitePriorityTrackbar;
		private System.Windows.Forms.TrackBar sectionWeightTrackbar;
		private System.Windows.Forms.TextBox siteSpeedValue;
		private System.Windows.Forms.TextBox sitePriorityValue;
		private System.Windows.Forms.TextBox sectionWeightValue;
		private System.Windows.Forms.Button ok;
		private System.Windows.Forms.Button cancel;
		private System.Windows.Forms.Label label4;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public WeightRatio()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.siteSpeedTrackbar = new System.Windows.Forms.TrackBar();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.sitePriorityTrackbar = new System.Windows.Forms.TrackBar();
			this.sectionWeightTrackbar = new System.Windows.Forms.TrackBar();
			this.siteSpeedValue = new System.Windows.Forms.TextBox();
			this.sitePriorityValue = new System.Windows.Forms.TextBox();
			this.sectionWeightValue = new System.Windows.Forms.TextBox();
			this.ok = new System.Windows.Forms.Button();
			this.cancel = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.siteSpeedTrackbar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.sitePriorityTrackbar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.sectionWeightTrackbar)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 72);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(152, 24);
			this.label1.TabIndex = 0;
			this.label1.Text = "Site speed (automatic)";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// siteSpeedTrackbar
			// 
			this.siteSpeedTrackbar.Location = new System.Drawing.Point(0, 88);
			this.siteSpeedTrackbar.Maximum = 100;
			this.siteSpeedTrackbar.Minimum = 1;
			this.siteSpeedTrackbar.Name = "siteSpeedTrackbar";
			this.siteSpeedTrackbar.Size = new System.Drawing.Size(264, 42);
			this.siteSpeedTrackbar.TabIndex = 1;
			this.siteSpeedTrackbar.TickFrequency = 5;
			this.siteSpeedTrackbar.Value = 33;
			this.siteSpeedTrackbar.Scroll += new System.EventHandler(this.siteSpeedTrackbar_Scroll);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 128);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(226, 24);
			this.label2.TabIndex = 2;
			this.label2.Text = "Site Priority (Set in main options menu)";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 184);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(184, 24);
			this.label3.TabIndex = 4;
			this.label3.Text = "Section weight (automatic)";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// sitePriorityTrackbar
			// 
			this.sitePriorityTrackbar.Location = new System.Drawing.Point(0, 144);
			this.sitePriorityTrackbar.Maximum = 100;
			this.sitePriorityTrackbar.Minimum = 1;
			this.sitePriorityTrackbar.Name = "sitePriorityTrackbar";
			this.sitePriorityTrackbar.Size = new System.Drawing.Size(264, 42);
			this.sitePriorityTrackbar.TabIndex = 5;
			this.sitePriorityTrackbar.TickFrequency = 5;
			this.sitePriorityTrackbar.Value = 33;
			this.sitePriorityTrackbar.Scroll += new System.EventHandler(this.sitePriorityTrackbar_Scroll);
			// 
			// sectionWeightTrackbar
			// 
			this.sectionWeightTrackbar.Enabled = false;
			this.sectionWeightTrackbar.Location = new System.Drawing.Point(0, 200);
			this.sectionWeightTrackbar.Maximum = 100;
			this.sectionWeightTrackbar.Minimum = 1;
			this.sectionWeightTrackbar.Name = "sectionWeightTrackbar";
			this.sectionWeightTrackbar.Size = new System.Drawing.Size(264, 42);
			this.sectionWeightTrackbar.TabIndex = 6;
			this.sectionWeightTrackbar.TickFrequency = 5;
			this.sectionWeightTrackbar.Value = 33;
			this.sectionWeightTrackbar.Scroll += new System.EventHandler(this.sectionWeightTrackbar_Scroll);
			// 
			// siteSpeedValue
			// 
			this.siteSpeedValue.Location = new System.Drawing.Point(272, 96);
			this.siteSpeedValue.Name = "siteSpeedValue";
			this.siteSpeedValue.Size = new System.Drawing.Size(32, 20);
			this.siteSpeedValue.TabIndex = 7;
			this.siteSpeedValue.Text = "0.33";
			this.siteSpeedValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.siteSpeedValue.TextChanged += new System.EventHandler(this.siteSpeedValue_TextChanged);
			// 
			// sitePriorityValue
			// 
			this.sitePriorityValue.Location = new System.Drawing.Point(272, 152);
			this.sitePriorityValue.Name = "sitePriorityValue";
			this.sitePriorityValue.Size = new System.Drawing.Size(32, 20);
			this.sitePriorityValue.TabIndex = 8;
			this.sitePriorityValue.Text = "0.33";
			this.sitePriorityValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.sitePriorityValue.TextChanged += new System.EventHandler(this.sitePriorityValue_TextChanged);
			// 
			// sectionWeightValue
			// 
			this.sectionWeightValue.Enabled = false;
			this.sectionWeightValue.Location = new System.Drawing.Point(272, 208);
			this.sectionWeightValue.Name = "sectionWeightValue";
			this.sectionWeightValue.Size = new System.Drawing.Size(32, 20);
			this.sectionWeightValue.TabIndex = 9;
			this.sectionWeightValue.Text = "0.33";
			this.sectionWeightValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			this.sectionWeightValue.TextChanged += new System.EventHandler(this.sectionWeightValue_TextChanged);
			// 
			// ok
			// 
			this.ok.Location = new System.Drawing.Point(8, 256);
			this.ok.Name = "ok";
			this.ok.Size = new System.Drawing.Size(56, 24);
			this.ok.TabIndex = 10;
			this.ok.Text = "Ok";
			this.ok.Click += new System.EventHandler(this.ok_Click);
			// 
			// cancel
			// 
			this.cancel.Location = new System.Drawing.Point(72, 256);
			this.cancel.Name = "cancel";
			this.cancel.Size = new System.Drawing.Size(64, 24);
			this.cancel.TabIndex = 11;
			this.cancel.Text = "Cancel";
			this.cancel.Click += new System.EventHandler(this.cancel_Click);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 8);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(296, 64);
			this.label4.TabIndex = 12;
			this.label4.Text = "Set the individual weight of the different categories. The higher weight compared" +
				" to the others, the more it will count for when finding the best suited destinat" +
				"ion for a transfer";
			// 
			// WeightRatio
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(320, 285);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.cancel);
			this.Controls.Add(this.ok);
			this.Controls.Add(this.sectionWeightValue);
			this.Controls.Add(this.sitePriorityValue);
			this.Controls.Add(this.siteSpeedValue);
			this.Controls.Add(this.sectionWeightTrackbar);
			this.Controls.Add(this.sitePriorityTrackbar);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.siteSpeedTrackbar);
			this.Controls.Add(this.label1);
			this.Name = "WeightRatio";
			this.Text = "WeightRation";
			this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.WeightRatio_KeyUp);
			((System.ComponentModel.ISupportInitialize)(this.siteSpeedTrackbar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.sitePriorityTrackbar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.sectionWeightTrackbar)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void siteSpeedValue_TextChanged(object sender, System.EventArgs e)
		{
			// s�tt v�rde p� trackbaren h�r, men se till att de ine triggar en snurr-call s� att en event triggar ns�ta som triggar n�sta osv.
			try
			{
				int value = Convert.ToInt32(Convert.ToDouble(this.siteSpeedValue.Text) * 100.0d);
				if (value > 0 && value < 101) 
				{
					this.siteSpeedTrackbar.Value = value;
				}
			}
			catch (FormatException ex)
			{
				
			}

		}

		private void sitePriorityValue_TextChanged(object sender, System.EventArgs e)
		{
			// s�tt v�rde p� trackbaren h�r, men se till att de ine triggar en snurr-call s� att en event triggar ns�ta som triggar n�sta osv.
			try
			{
				int value = Convert.ToInt32(Convert.ToDouble(this.sitePriorityValue.Text) * 100.0d);
				if (value > 0 && value < 101) 
				{
					this.sitePriorityTrackbar.Value = value;
				}
			}
			catch (FormatException ex)
			{
				
			}
		}

		private void sectionWeightValue_TextChanged(object sender, System.EventArgs e)
		{
			// s�tt v�rde p� trackbaren h�r, men se till att de ine triggar en snurr-call s� att en event triggar ns�ta som triggar n�sta osv.
			
			try
			{
				int value = Convert.ToInt32(Convert.ToDouble(this.sectionWeightValue.Text) * 100.0d);
				Logger.Log(Convert.ToString(value));
			}
			catch (FormatException ex)
			{
				
			}
			Logger.Log("changed");
		}

		private void sectionWeightTrackbar_Scroll(object sender, System.EventArgs e)
		{
			Logger.Log("scrolled");
			this.sectionWeightValue.Text = Convert.ToString( (double)this.sectionWeightTrackbar.Value / 100.0d );
			// plus annat
			// �ndra dom andra tv� s� att dom delar p� det som �r kvar, och uppdelningen sker med deras inb�rdes f�rh�llande
		}

		private void sitePriorityTrackbar_Scroll(object sender, System.EventArgs e)
		{
			this.sitePriorityValue.Text = Convert.ToString( (double)this.sitePriorityTrackbar.Value / 100.0d );
			// plus annat
			// �ndra dom andra tv� s� att dom delar p� det som �r kvar, och uppdelningen sker med deras inb�rdes f�rh�llande
		}

		private void siteSpeedTrackbar_Scroll(object sender, System.EventArgs e)
		{
			this.siteSpeedValue.Text = Convert.ToString( (double)this.siteSpeedTrackbar.Value / 100.0d );
			// plus annat
			// �ndra dom andra tv� s� att dom delar p� det som �r kvar, och uppdelningen sker med deras inb�rdes f�rh�llande
		}

		private void ok_Click(object sender, System.EventArgs e)
		{
			// save the values.
			Config.GeneralSettings["section_weight"] = (double)this.sectionWeightTrackbar.Value / 100.0d;
			Config.GeneralSettings["site_priority_weight"] = (double)this.sitePriorityTrackbar.Value / 100.0d;
			Config.GeneralSettings["site_speed_weight"] = (double)this.siteSpeedTrackbar.Value / 100.0d;
		}

		private void cancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void WeightRatio_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			Logger.Log(e.KeyCode + ":" + e.KeyData + ":" + e.KeyValue + ": ESC IS " + Keys.Escape);
		}
	}
}

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Tiki.Gui
{
	/// <summary>
	/// Summary description for FloatingLogger.
	/// </summary>
	public class FloatingLogger : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox log;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private ClientGui gui;
		public FloatingLogger(ClientGui gui)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.gui = gui;
			if (gui != null) 
			{
				Logger.Output += new Output(this.Log);
			}

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(FloatingLogger));
			this.log = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// log
			// 
			this.log.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.log.CausesValidation = false;
			this.log.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.log.Location = new System.Drawing.Point(0, 0);
			this.log.MaxLength = 32769;
			this.log.Multiline = true;
			this.log.Name = "log";
			this.log.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.log.Size = new System.Drawing.Size(664, 152);
			this.log.TabIndex = 0;
			this.log.Text = "";
			this.log.WordWrap = false;
			// 
			// FloatingLogger
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.AutoScroll = true;
			this.ClientSize = new System.Drawing.Size(664, 152);
			this.Controls.Add(this.log);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FloatingLogger";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Log";
			this.Closed += new System.EventHandler(this.FloatingLogger_Closed);
			this.ResumeLayout(false);

		}
		#endregion

		public void Log(string message)
		{
			// when the conrtl starts getting ful, clear a part of it
			if (log.TextLength > (log.MaxLength * 0.95d))
			{
				log.Text = log.Text.Substring(Convert.ToInt32(log.MaxLength * 0.8));
			}
			log.AppendText(message + Environment.NewLine);
			this.Text = "Log " + log.TextLength;
		}

		private void FloatingLogger_Closed(object sender, System.EventArgs e)
		{
			if (gui != null) 
			{
				gui.LoggerInvokeNeeded = true;
				Logger.Output -= new Output(this.Log);
			}
			
		}
	}
}

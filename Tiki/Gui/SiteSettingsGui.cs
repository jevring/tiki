using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Tiki.Gui
{
	/// <summary>
	/// Summary description for SiteSettingsGui.
	/// </summary>
	public class SiteSettingsGui : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ListBox sitesListBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox username;
		private System.Windows.Forms.CheckBox ssl;
		private System.Windows.Forms.CheckBox idle;
		private System.Windows.Forms.CheckBox leech;
		private System.Windows.Forms.TextBox password;
		private System.Windows.Forms.CheckBox reconnect;
		private System.Windows.Forms.Button OKButton;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button saveSiteButton;
		private System.Windows.Forms.Button deleteSiteButton;
		private System.Windows.Forms.Button newSiteButton;
		private System.Windows.Forms.CheckBox allowAsSource;
		private System.Windows.Forms.CheckBox allowAsDestination;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox usePassiveMode;
		private System.Windows.Forms.CheckBox statForList;
		private System.Windows.Forms.TextBox hostnamePortPairs;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage hostTab;
		private System.Windows.Forms.TabPage settingsTab;
		private System.Windows.Forms.TabPage sslTab;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox idleCommand;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.RadioButton ftps;
		private System.Windows.Forms.RadioButton authTls;
		private System.Windows.Forms.RadioButton authSsl;
		private System.Windows.Forms.TabPage statsTab;
		private System.Windows.Forms.NumericUpDown idleTime;
		private System.Windows.Forms.CheckBox sslList;
		private System.Windows.Forms.CheckBox sslTransfer;
		private System.Windows.Forms.TextBox name;
		private System.Windows.Forms.Panel panel1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.Button sections;
		private bool changed = false;
		private bool newSite = true;
		private Site currentSite = null;
		private System.Windows.Forms.CheckBox randomAntiIdle;

		public SiteSettingsGui()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SiteSettingsGui));
			this.sitesListBox = new System.Windows.Forms.ListBox();
			this.label1 = new System.Windows.Forms.Label();
			this.allowAsDestination = new System.Windows.Forms.CheckBox();
			this.allowAsSource = new System.Windows.Forms.CheckBox();
			this.newSiteButton = new System.Windows.Forms.Button();
			this.deleteSiteButton = new System.Windows.Forms.Button();
			this.saveSiteButton = new System.Windows.Forms.Button();
			this.reconnect = new System.Windows.Forms.CheckBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.password = new System.Windows.Forms.TextBox();
			this.leech = new System.Windows.Forms.CheckBox();
			this.idle = new System.Windows.Forms.CheckBox();
			this.sslList = new System.Windows.Forms.CheckBox();
			this.sslTransfer = new System.Windows.Forms.CheckBox();
			this.ssl = new System.Windows.Forms.CheckBox();
			this.username = new System.Windows.Forms.TextBox();
			this.name = new System.Windows.Forms.TextBox();
			this.OKButton = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.statForList = new System.Windows.Forms.CheckBox();
			this.usePassiveMode = new System.Windows.Forms.CheckBox();
			this.hostnamePortPairs = new System.Windows.Forms.TextBox();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.hostTab = new System.Windows.Forms.TabPage();
			this.sections = new System.Windows.Forms.Button();
			this.settingsTab = new System.Windows.Forms.TabPage();
			this.randomAntiIdle = new System.Windows.Forms.CheckBox();
			this.idleTime = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.idleCommand = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.sslTab = new System.Windows.Forms.TabPage();
			this.panel1 = new System.Windows.Forms.Panel();
			this.ftps = new System.Windows.Forms.RadioButton();
			this.authSsl = new System.Windows.Forms.RadioButton();
			this.authTls = new System.Windows.Forms.RadioButton();
			this.statsTab = new System.Windows.Forms.TabPage();
			this.tabControl1.SuspendLayout();
			this.hostTab.SuspendLayout();
			this.settingsTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.idleTime)).BeginInit();
			this.sslTab.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// sitesListBox
			// 
			this.sitesListBox.Location = new System.Drawing.Point(8, 32);
			this.sitesListBox.Name = "sitesListBox";
			this.sitesListBox.Size = new System.Drawing.Size(120, 251);
			this.sitesListBox.Sorted = true;
			this.sitesListBox.TabIndex = 1;
			this.sitesListBox.SelectedIndexChanged += new System.EventHandler(this.sitesListBox_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(88, 16);
			this.label1.TabIndex = 1;
			this.label1.Text = "Sites:";
			// 
			// allowAsDestination
			// 
			this.allowAsDestination.Checked = true;
			this.allowAsDestination.CheckState = System.Windows.Forms.CheckState.Checked;
			this.allowAsDestination.Location = new System.Drawing.Point(16, 160);
			this.allowAsDestination.Name = "allowAsDestination";
			this.allowAsDestination.Size = new System.Drawing.Size(128, 16);
			this.allowAsDestination.TabIndex = 19;
			this.allowAsDestination.Text = "Allow as Destination";
			this.allowAsDestination.CheckedChanged += new System.EventHandler(this.allowAsDestination_CheckedChanged);
			// 
			// allowAsSource
			// 
			this.allowAsSource.Checked = true;
			this.allowAsSource.CheckState = System.Windows.Forms.CheckState.Checked;
			this.allowAsSource.Location = new System.Drawing.Point(16, 136);
			this.allowAsSource.Name = "allowAsSource";
			this.allowAsSource.Size = new System.Drawing.Size(104, 16);
			this.allowAsSource.TabIndex = 18;
			this.allowAsSource.Text = "Allow as Source";
			this.allowAsSource.CheckedChanged += new System.EventHandler(this.allowAsSource_CheckedChanged);
			// 
			// newSiteButton
			// 
			this.newSiteButton.Location = new System.Drawing.Point(384, 296);
			this.newSiteButton.Name = "newSiteButton";
			this.newSiteButton.Size = new System.Drawing.Size(56, 24);
			this.newSiteButton.TabIndex = 14;
			this.newSiteButton.Text = "New";
			this.newSiteButton.Click += new System.EventHandler(this.newSiteButton_Click);
			// 
			// deleteSiteButton
			// 
			this.deleteSiteButton.Location = new System.Drawing.Point(320, 296);
			this.deleteSiteButton.Name = "deleteSiteButton";
			this.deleteSiteButton.Size = new System.Drawing.Size(56, 24);
			this.deleteSiteButton.TabIndex = 15;
			this.deleteSiteButton.Text = "Delete";
			this.deleteSiteButton.Click += new System.EventHandler(this.deleteSiteButton_Click);
			// 
			// saveSiteButton
			// 
			this.saveSiteButton.Location = new System.Drawing.Point(256, 296);
			this.saveSiteButton.Name = "saveSiteButton";
			this.saveSiteButton.Size = new System.Drawing.Size(56, 24);
			this.saveSiteButton.TabIndex = 6;
			this.saveSiteButton.Text = "Save";
			this.saveSiteButton.Click += new System.EventHandler(this.saveSiteButton_Click);
			// 
			// reconnect
			// 
			this.reconnect.Checked = true;
			this.reconnect.CheckState = System.Windows.Forms.CheckState.Checked;
			this.reconnect.Location = new System.Drawing.Point(16, 112);
			this.reconnect.Name = "reconnect";
			this.reconnect.Size = new System.Drawing.Size(112, 16);
			this.reconnect.TabIndex = 13;
			this.reconnect.Text = "Auto-reconnect";
			this.reconnect.CheckedChanged += new System.EventHandler(this.reconnect_CheckedChanged);
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(16, 80);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(64, 24);
			this.label7.TabIndex = 15;
			this.label7.Text = "Password";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(16, 48);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(64, 24);
			this.label6.TabIndex = 14;
			this.label6.Text = "Username";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 24);
			this.label2.TabIndex = 10;
			this.label2.Text = "Name";
			// 
			// password
			// 
			this.password.Location = new System.Drawing.Point(88, 80);
			this.password.Name = "password";
			this.password.Size = new System.Drawing.Size(96, 20);
			this.password.TabIndex = 4;
			this.password.Text = "";
			this.password.TextChanged += new System.EventHandler(this.password_TextChanged);
			// 
			// leech
			// 
			this.leech.Location = new System.Drawing.Point(16, 64);
			this.leech.Name = "leech";
			this.leech.Size = new System.Drawing.Size(112, 16);
			this.leech.TabIndex = 11;
			this.leech.Text = "Leech";
			this.leech.CheckedChanged += new System.EventHandler(this.leech_CheckedChanged);
			// 
			// idle
			// 
			this.idle.Checked = true;
			this.idle.CheckState = System.Windows.Forms.CheckState.Checked;
			this.idle.Location = new System.Drawing.Point(16, 88);
			this.idle.Name = "idle";
			this.idle.Size = new System.Drawing.Size(112, 16);
			this.idle.TabIndex = 12;
			this.idle.Text = "Allows idle";
			this.idle.CheckedChanged += new System.EventHandler(this.idle_CheckedChanged);
			// 
			// sslList
			// 
			this.sslList.Enabled = false;
			this.sslList.Location = new System.Drawing.Point(48, 128);
			this.sslList.Name = "sslList";
			this.sslList.Size = new System.Drawing.Size(136, 16);
			this.sslList.TabIndex = 9;
			this.sslList.Text = "Encrypted dirlistings";
			this.sslList.CheckedChanged += new System.EventHandler(this.sslList_CheckedChanged);
			// 
			// sslTransfer
			// 
			this.sslTransfer.Enabled = false;
			this.sslTransfer.Location = new System.Drawing.Point(48, 152);
			this.sslTransfer.Name = "sslTransfer";
			this.sslTransfer.Size = new System.Drawing.Size(136, 16);
			this.sslTransfer.TabIndex = 10;
			this.sslTransfer.Text = "Encrypted transfers";
			this.sslTransfer.CheckedChanged += new System.EventHandler(this.sslTransfer_CheckedChanged);
			// 
			// ssl
			// 
			this.ssl.Checked = true;
			this.ssl.CheckState = System.Windows.Forms.CheckState.Checked;
			this.ssl.Location = new System.Drawing.Point(24, 24);
			this.ssl.Name = "ssl";
			this.ssl.Size = new System.Drawing.Size(112, 16);
			this.ssl.TabIndex = 8;
			this.ssl.Text = "Use SSL";
			this.ssl.CheckedChanged += new System.EventHandler(this.ssl_CheckedChanged);
			// 
			// username
			// 
			this.username.Location = new System.Drawing.Point(88, 48);
			this.username.Name = "username";
			this.username.Size = new System.Drawing.Size(96, 20);
			this.username.TabIndex = 3;
			this.username.Text = "";
			this.username.TextChanged += new System.EventHandler(this.username_TextChanged);
			// 
			// name
			// 
			this.name.Location = new System.Drawing.Point(88, 16);
			this.name.Name = "name";
			this.name.Size = new System.Drawing.Size(96, 20);
			this.name.TabIndex = 2;
			this.name.Text = "";
			this.name.TextChanged += new System.EventHandler(this.name_TextChanged);
			// 
			// OKButton
			// 
			this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.OKButton.Location = new System.Drawing.Point(8, 296);
			this.OKButton.Name = "OKButton";
			this.OKButton.Size = new System.Drawing.Size(56, 24);
			this.OKButton.TabIndex = 9;
			this.OKButton.Text = "OK";
			// 
			// button1
			// 
			this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button1.Location = new System.Drawing.Point(72, 296);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(56, 24);
			this.button1.TabIndex = 10;
			this.button1.Text = "Cancel";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 112);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(168, 24);
			this.label3.TabIndex = 20;
			this.label3.Text = "Hostname:Port pairs";
			// 
			// statForList
			// 
			this.statForList.Checked = true;
			this.statForList.CheckState = System.Windows.Forms.CheckState.Checked;
			this.statForList.Enabled = false;
			this.statForList.Location = new System.Drawing.Point(16, 40);
			this.statForList.Name = "statForList";
			this.statForList.Size = new System.Drawing.Size(144, 16);
			this.statForList.TabIndex = 23;
			this.statForList.Text = "Use \"stat -l\" to list dirs";
			this.statForList.CheckedChanged += new System.EventHandler(this.statForList_CheckedChanged);
			// 
			// usePassiveMode
			// 
			this.usePassiveMode.Checked = true;
			this.usePassiveMode.CheckState = System.Windows.Forms.CheckState.Checked;
			this.usePassiveMode.Enabled = false;
			this.usePassiveMode.Location = new System.Drawing.Point(16, 16);
			this.usePassiveMode.Name = "usePassiveMode";
			this.usePassiveMode.Size = new System.Drawing.Size(112, 16);
			this.usePassiveMode.TabIndex = 22;
			this.usePassiveMode.Text = "Passive mode";
			this.usePassiveMode.CheckedChanged += new System.EventHandler(this.usePassiveMode_CheckedChanged);
			// 
			// hostnamePortPairs
			// 
			this.hostnamePortPairs.Location = new System.Drawing.Point(16, 136);
			this.hostnamePortPairs.Multiline = true;
			this.hostnamePortPairs.Name = "hostnamePortPairs";
			this.hostnamePortPairs.Size = new System.Drawing.Size(168, 80);
			this.hostnamePortPairs.TabIndex = 5;
			this.hostnamePortPairs.Text = "";
			this.hostnamePortPairs.TextChanged += new System.EventHandler(this.hostnamePortPairs_TextChanged);
			// 
			// tabControl1
			// 
			this.tabControl1.Controls.Add(this.hostTab);
			this.tabControl1.Controls.Add(this.settingsTab);
			this.tabControl1.Controls.Add(this.sslTab);
			this.tabControl1.Controls.Add(this.statsTab);
			this.tabControl1.Location = new System.Drawing.Point(136, 8);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(304, 280);
			this.tabControl1.TabIndex = 8;
			// 
			// hostTab
			// 
			this.hostTab.Controls.Add(this.sections);
			this.hostTab.Controls.Add(this.label7);
			this.hostTab.Controls.Add(this.label6);
			this.hostTab.Controls.Add(this.label2);
			this.hostTab.Controls.Add(this.password);
			this.hostTab.Controls.Add(this.username);
			this.hostTab.Controls.Add(this.name);
			this.hostTab.Controls.Add(this.label3);
			this.hostTab.Controls.Add(this.hostnamePortPairs);
			this.hostTab.Location = new System.Drawing.Point(4, 22);
			this.hostTab.Name = "hostTab";
			this.hostTab.Size = new System.Drawing.Size(296, 254);
			this.hostTab.TabIndex = 0;
			this.hostTab.Text = "Host";
			// 
			// sections
			// 
			this.sections.Location = new System.Drawing.Point(16, 224);
			this.sections.Name = "sections";
			this.sections.Size = new System.Drawing.Size(168, 24);
			this.sections.TabIndex = 7;
			this.sections.Text = "Sections";
			this.sections.Click += new System.EventHandler(this.sections_Click);
			// 
			// settingsTab
			// 
			this.settingsTab.Controls.Add(this.randomAntiIdle);
			this.settingsTab.Controls.Add(this.idleTime);
			this.settingsTab.Controls.Add(this.label5);
			this.settingsTab.Controls.Add(this.idleCommand);
			this.settingsTab.Controls.Add(this.label4);
			this.settingsTab.Controls.Add(this.reconnect);
			this.settingsTab.Controls.Add(this.leech);
			this.settingsTab.Controls.Add(this.idle);
			this.settingsTab.Controls.Add(this.statForList);
			this.settingsTab.Controls.Add(this.usePassiveMode);
			this.settingsTab.Controls.Add(this.allowAsDestination);
			this.settingsTab.Controls.Add(this.allowAsSource);
			this.settingsTab.Location = new System.Drawing.Point(4, 22);
			this.settingsTab.Name = "settingsTab";
			this.settingsTab.Size = new System.Drawing.Size(296, 254);
			this.settingsTab.TabIndex = 1;
			this.settingsTab.Text = "Settings";
			// 
			// randomAntiIdle
			// 
			this.randomAntiIdle.Location = new System.Drawing.Point(216, 184);
			this.randomAntiIdle.Name = "randomAntiIdle";
			this.randomAntiIdle.Size = new System.Drawing.Size(72, 24);
			this.randomAntiIdle.TabIndex = 32;
			this.randomAntiIdle.Text = "Random";
			this.randomAntiIdle.CheckedChanged += new System.EventHandler(this.randomAntiIdle_CheckedChanged);
			// 
			// idleTime
			// 
			this.idleTime.Location = new System.Drawing.Point(136, 208);
			this.idleTime.Maximum = new System.Decimal(new int[] {
																	 300,
																	 0,
																	 0,
																	 0});
			this.idleTime.Name = "idleTime";
			this.idleTime.Size = new System.Drawing.Size(40, 20);
			this.idleTime.TabIndex = 31;
			this.idleTime.Value = new System.Decimal(new int[] {
																   30,
																   0,
																   0,
																   0});
			this.idleTime.ValueChanged += new System.EventHandler(this.idleTime_ValueChanged);
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(16, 208);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(112, 24);
			this.label5.TabIndex = 29;
			this.label5.Text = "Anti-Idle Interval";
			// 
			// idleCommand
			// 
			this.idleCommand.Location = new System.Drawing.Point(136, 184);
			this.idleCommand.Name = "idleCommand";
			this.idleCommand.Size = new System.Drawing.Size(72, 20);
			this.idleCommand.TabIndex = 28;
			this.idleCommand.Text = "NOOP";
			this.idleCommand.TextChanged += new System.EventHandler(this.idleCommand_TextChanged);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(16, 184);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(112, 24);
			this.label4.TabIndex = 27;
			this.label4.Text = "Anti-Idle command";
			// 
			// sslTab
			// 
			this.sslTab.Controls.Add(this.panel1);
			this.sslTab.Controls.Add(this.ssl);
			this.sslTab.Controls.Add(this.sslList);
			this.sslTab.Controls.Add(this.sslTransfer);
			this.sslTab.Location = new System.Drawing.Point(4, 22);
			this.sslTab.Name = "sslTab";
			this.sslTab.Size = new System.Drawing.Size(296, 254);
			this.sslTab.TabIndex = 2;
			this.sslTab.Text = "Ssl";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.ftps);
			this.panel1.Controls.Add(this.authSsl);
			this.panel1.Controls.Add(this.authTls);
			this.panel1.Enabled = false;
			this.panel1.Location = new System.Drawing.Point(48, 48);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(160, 72);
			this.panel1.TabIndex = 12;
			// 
			// ftps
			// 
			this.ftps.Location = new System.Drawing.Point(0, 0);
			this.ftps.Name = "ftps";
			this.ftps.Size = new System.Drawing.Size(136, 24);
			this.ftps.TabIndex = 9;
			this.ftps.Text = "ftps://";
			this.ftps.CheckedChanged += new System.EventHandler(this.ftps_CheckedChanged);
			// 
			// authSsl
			// 
			this.authSsl.Location = new System.Drawing.Point(0, 48);
			this.authSsl.Name = "authSsl";
			this.authSsl.Size = new System.Drawing.Size(136, 24);
			this.authSsl.TabIndex = 11;
			this.authSsl.Text = "AUTH SSL";
			// 
			// authTls
			// 
			this.authTls.Checked = true;
			this.authTls.Location = new System.Drawing.Point(0, 24);
			this.authTls.Name = "authTls";
			this.authTls.Size = new System.Drawing.Size(136, 24);
			this.authTls.TabIndex = 10;
			this.authTls.TabStop = true;
			this.authTls.Text = "AUTH TLS";
			// 
			// statsTab
			// 
			this.statsTab.Location = new System.Drawing.Point(4, 22);
			this.statsTab.Name = "statsTab";
			this.statsTab.Size = new System.Drawing.Size(296, 254);
			this.statsTab.TabIndex = 3;
			this.statsTab.Text = "Statistics";
			// 
			// SiteSettingsGui
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(450, 327);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.OKButton);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.sitesListBox);
			this.Controls.Add(this.newSiteButton);
			this.Controls.Add(this.deleteSiteButton);
			this.Controls.Add(this.saveSiteButton);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SiteSettingsGui";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Settings: Site";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.SiteSettingsGui_Closing);
			this.Load += new System.EventHandler(this.SiteSettingsGui_Load);
			this.tabControl1.ResumeLayout(false);
			this.hostTab.ResumeLayout(false);
			this.settingsTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.idleTime)).EndInit();
			this.sslTab.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void sitesListBox_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// if we're adding a new one, and the user clicks in the list, ask him/her if we want to abort adding the new one
			// rather, if we have unsaved changes...
			Site site = (Site)sitesListBox.SelectedItem;
			// handles asking about saving
			VerifySave();
			currentSite = site;
			if (site != null) 
			{
				PopulateForm(site);
			}
		}

		private void PopulateForm(Site site)
		{
			newSite = false;
			this.name.Text = site.Name;
			for (int i = 0; i < site.HostnamePortPairs.Length; i++)
			{
				string portPair = site.HostnamePortPairs[i];
				this.hostnamePortPairs.Text += portPair + Environment.NewLine;
			}
			this.hostnamePortPairs.Text = this.hostnamePortPairs.Text.Substring(0, this.hostnamePortPairs.Text.Length - 2);
			this.username.Text = site.Username;
			this.password.Text = site.Password;

			this.usePassiveMode.Checked = site.UsePassiveMode;
			this.statForList.Checked = site.StatForList;
			this.leech.Checked = site.Leech;
			this.allowAsDestination.Checked = site.AllowAsDestination;
			this.allowAsSource.Checked = site.AllowAsSource;
			this.idle.Checked = site.AllowsIdle;
			this.reconnect.Checked = site.Reconnect;
			this.idleCommand.Text = site.IdleCommand;
			if (site.IdleCommand.Length == 0)
			{
				this.idleCommand.Enabled = false;
				this.randomAntiIdle.Checked = true;
			}
			this.idleTime.Text = Convert.ToString(site.IdleInterval);
			this.idleTime.Value = site.IdleInterval;
			this.ssl.Checked = site.Ssl;
			switch (site.SslMode)
			{
				case 1: this.ftps.Checked = true;break;
				case 2: this.authTls.Checked = true;break;
				case 3: this.authSsl.Checked = true;break;
			}
			this.sslList.Checked = site.SslDirlistings;
			this.sslTransfer.Checked = site.SslTransfers;
			changed = false;
		}

		private void ssl_CheckedChanged(object sender, System.EventArgs e)
		{
			changed = true;
			// always do this when we're switching modes.
			authTls.Checked = false;
			ftps.Checked = false;
			sslTransfer.Checked = false;
			sslList.Checked = false;
			authTls.Checked = false;

			CheckBox ssl = (CheckBox)sender;
			if (ssl.Checked)
			{
				// make sure this does what we want, namely to disable all ssl related boxes when we unckech it, and vice versa.
				SslSettings(true);
			}
			else
			{
				SslSettings(false);
			}
		}

		private void SslSettings(bool on)
		{
			authTls.Enabled = on;
			authSsl.Enabled = on;
			ftps.Enabled = on;
			sslTransfer.Enabled = on;
			sslList.Enabled = on;
		}

		private void newSiteButton_Click(object sender, System.EventArgs e)
		{
			VerifySave();
			newSite = true;
		}

		private void VerifySave()
		{
			// check if there have been any changes to the current site.
			if (changed)
			{
				// ask if we want to save the current settings first.
				// make this nicer with better caption and such

				DialogResult dr = MessageBox.Show("Save this site first?", "save", MessageBoxButtons.YesNo);
				if (dr == DialogResult.Yes)
				{
					SaveSite();
				}
				ClearFields();
			}
			else
			{
				// clear all the fields in all the tabs
				ClearFields();
			}
			changed = false;
		}

		private void ClearFields()
		{
			this.name.Text = "";
			this.hostnamePortPairs.Text = "";
			this.username.Text = "";
			this.password.Text = "";

			this.usePassiveMode.Checked = true;
			this.statForList.Checked = true;
			this.leech.Checked = false;
			this.allowAsDestination.Checked = true;
			this.allowAsSource.Checked = true;
			this.idle.Checked = true;
			this.reconnect.Checked = true;
			this.idleCommand.Text = "NOOP";
			this.randomAntiIdle.Checked = false;
			this.idleTime.Text = "30";
			this.idleTime.Value = 30;
			SslSettings(false);
			changed = false;
		}

		private void SaveSite()
		{
			if (newSite)
			{
				NewSite();
			}
			else
			{
				UpdateSite();
			}
		}
		private void UpdateSite()
		{
			//Site site = (Site)sitesListBox.SelectedItem;
			Site site = currentSite;
			// NOTE: this WILL case trouble when we select another site in the list before having saved this one.
			string oldName = site.Name;
			site.Name = this.name.Text;
			site.Username = this.username.Text;
			site.Password = this.password.Text;

			site.HostnamePortPairs = this.hostnamePortPairs.Lines;

			site.IdleCommand = (this.randomAntiIdle.Checked ? "" : this.idleCommand.Text);
			site.IdleInterval = Convert.ToInt32(this.idleTime.Value);
			site.StatForList = this.statForList.Checked;
			site.UsePassiveMode = this.usePassiveMode.Checked;
			site.AllowsIdle = this.idle.Checked;
			site.Ssl = this.ssl.Checked;
			int sslMode = 2;
			if (this.ftps.Checked)
			{
				sslMode = 1;
			}
			else if (this.authTls.Checked)
			{
				sslMode = 2;
			}
			else if (this.authSsl.Checked)
			{
				sslMode = 3;
			}
			site.SslMode = sslMode;
			site.SslDirlistings = this.sslList.Checked;
			site.SslTransfers = this.sslTransfer.Checked;
			site.Reconnect = this.reconnect.Checked;
			site.Leech = this.leech.Checked;
			site.AllowAsSource = this.allowAsSource.Checked;
			site.AllowAsDestination = this.allowAsDestination.Checked;

			if (site.Name.Equals(oldName))
			{
				Config.Sites[site.Name] = site;
			}
			else
			{
				// the name has been changed.
				Config.Sites.Remove(oldName);
				Config.Sites.Add(site.Name, site);
				sitesListBox.BeginUpdate();
				// update the list to reflect the new name
				sitesListBox.Refresh();
				/*
				int i = sitesListBox.SelectedIndex;
				sitesListBox.Items.RemoveAt(i);
				sitesListBox.Items.Insert(i, site);
				*/
				sitesListBox.EndUpdate();
			}
			changed = false;

		}

		private void NewSite()
		{
			if (this.name.Text.Length > 0) 
			{

				int sslMode = 2;
				if (this.ftps.Checked)
				{
					sslMode = 1;
				}
				else if (this.authTls.Checked)
				{
					sslMode = 2;
				}
				else if (this.authSsl.Checked)
				{
					sslMode = 3;
				}
				if (this.hostnamePortPairs.Text.IndexOf(":") == -1)
				{
					MessageBox.Show("Please supply atleast one hostname:port pair");
					return;
				}
				Site site = new Site(this.hostnamePortPairs.Lines, 
					this.username.Text, 
					this.password.Text, 
					this.name.Text, 
					(this.randomAntiIdle.Checked ? "" : this.idleCommand.Text), 
					Convert.ToInt32(this.idleTime.Value), 
					this.statForList.Checked, 
					this.usePassiveMode.Checked, 
					this.idle.Checked, 
					this.ssl.Checked, 
					sslMode, 
					this.sslList.Checked, 
					this.sslTransfer.Checked, 
					this.reconnect.Checked, 
					this.leech.Checked, 
					this.allowAsSource.Checked, 
					this.allowAsDestination.Checked);
				
				int i = 0;
				if (Config.Sites.Contains(site.Name))
				{
					DialogResult dr = MessageBox.Show("A site with that name already exists, do you want to overwrite?");
					if (dr == DialogResult.OK)
					{
						Config.Sites.Add(site.Name, site);
						i = sitesListBox.Items.Add(site);
					}
					// if we don't want to overwrite, do nothing...
				}
				else
				{
					Config.Sites.Add(site.Name, site);
					i = sitesListBox.Items.Add(site);
				}
				newSite = false;
				changed = false;
				sitesListBox.SetSelected(i, true);
			}
			else
			{
				MessageBox.Show("please enter atleast a name.");
			}
		}

		private void deleteSiteButton_Click(object sender, System.EventArgs e)
		{
			DialogResult dr = MessageBox.Show("Are you sure you wish to delete this site?", "Delete site?", MessageBoxButtons.YesNo);
			if (dr == DialogResult.Yes)
			{
				DeleteSelectedSite();
			}
		}

		private void DeleteSelectedSite()
		{
			Site site = (Site)sitesListBox.SelectedItem;
			sitesListBox.Items.Remove(site);
			Config.Sites.Remove(site.Name);
			
			// remove the botname->site mapping from memory
			foreach (DictionaryEntry bot in Config.Bots)
			{
				Site s = (Site)bot.Value;
				if (s.Name.Equals(site.Name))
				{
					Config.Bots.Remove(bot.Key);
				}
			}
		}

		private void saveSiteButton_Click(object sender, System.EventArgs e)
		{
			SaveSite();
		}

		private void sections_Click(object sender, System.EventArgs e)
		{
			// pass the current site object we're working on here (or maybe just the name)
			// save first
			SaveSite();
			Site s = (Site)sitesListBox.SelectedItem;
			if (s != null) 
			{
				SectionSettings ss = new SectionSettings(s);
				ss.ShowDialog();
			}
			else
			{
				MessageBox.Show("Please select a site first");
			}
		}

		private void SiteSettingsGui_Load(object sender, System.EventArgs e)
		{
			SslSettings(false);
			// populate the list
			foreach (Site site in Config.Sites.Values)
			{
				sitesListBox.Items.Add(site);
			}
			newSite = true;
		}
		private void SiteSettingsGui_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			VerifySave();
		}

		#region changed

		private void usePassiveMode_CheckedChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void name_TextChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void username_TextChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void password_TextChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void hostnamePortPairs_TextChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void statForList_CheckedChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void leech_CheckedChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void idle_CheckedChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void reconnect_CheckedChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void allowAsSource_CheckedChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void allowAsDestination_CheckedChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void idleCommand_TextChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void idleTime_ValueChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void ftps_CheckedChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void sslList_CheckedChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void sslTransfer_CheckedChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void randomAntiIdle_CheckedChanged(object sender, System.EventArgs e)
		{
			this.idleCommand.Enabled = !this.randomAntiIdle.Checked;
			changed = true;
		}
		#endregion
	}
}

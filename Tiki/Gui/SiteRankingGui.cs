using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Tiki.Gui
{
	/// <summary>
	/// Summary description for SiteRankingGui.
	/// </summary>
	public class SiteRankingGui : System.Windows.Forms.Form
	{
		private System.Windows.Forms.ListBox sites;
		private System.Windows.Forms.Button up;
		private System.Windows.Forms.Button down;
		private System.Windows.Forms.Button ok;
		private System.Windows.Forms.Button cancel;

		private ClientEngine engine;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public SiteRankingGui(ClientEngine engine)
		{
			this.engine = engine;
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SiteRankingGui));
			this.sites = new System.Windows.Forms.ListBox();
			this.up = new System.Windows.Forms.Button();
			this.down = new System.Windows.Forms.Button();
			this.ok = new System.Windows.Forms.Button();
			this.cancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// sites
			// 
			this.sites.Location = new System.Drawing.Point(8, 8);
			this.sites.Name = "sites";
			this.sites.Size = new System.Drawing.Size(112, 381);
			this.sites.TabIndex = 0;
			// 
			// up
			// 
			this.up.Location = new System.Drawing.Point(128, 144);
			this.up.Name = "up";
			this.up.Size = new System.Drawing.Size(48, 24);
			this.up.TabIndex = 1;
			this.up.Text = "UP";
			this.up.Click += new System.EventHandler(this.up_Click);
			// 
			// down
			// 
			this.down.Location = new System.Drawing.Point(128, 176);
			this.down.Name = "down";
			this.down.Size = new System.Drawing.Size(48, 24);
			this.down.TabIndex = 2;
			this.down.Text = "DOWN";
			this.down.Click += new System.EventHandler(this.down_Click);
			// 
			// ok
			// 
			this.ok.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.ok.Location = new System.Drawing.Point(8, 400);
			this.ok.Name = "ok";
			this.ok.Size = new System.Drawing.Size(48, 24);
			this.ok.TabIndex = 3;
			this.ok.Text = "Ok";
			this.ok.Click += new System.EventHandler(this.ok_Click);
			// 
			// cancel
			// 
			this.cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancel.Location = new System.Drawing.Point(64, 400);
			this.cancel.Name = "cancel";
			this.cancel.Size = new System.Drawing.Size(56, 24);
			this.cancel.TabIndex = 4;
			this.cancel.Text = "Cancel";
			this.cancel.Click += new System.EventHandler(this.cancel_Click);
			// 
			// SiteRankingGui
			// 
			this.AcceptButton = this.ok;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.cancel;
			this.ClientSize = new System.Drawing.Size(178, 431);
			this.Controls.Add(this.cancel);
			this.Controls.Add(this.ok);
			this.Controls.Add(this.down);
			this.Controls.Add(this.up);
			this.Controls.Add(this.sites);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SiteRankingGui";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "SiteRankingGui";
			this.Load += new System.EventHandler(this.SiteRankingGui_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void SiteRankingGui_Load(object sender, System.EventArgs e)
		{
			sites.BeginUpdate();
			for (int i = 0; i < Config.SiteRanking.Count; i++)
			{
				string siteRanking = (string) Config.SiteRanking[i];
				if (Config.Sites.Contains(siteRanking)) 
				{
					// only add sites that actually exist
					sites.Items.Add(siteRanking);
				}
			}
			// get the sites that aren't in the list yet
			foreach (string s in Config.Sites.Keys)
			{
				if (!sites.Items.Contains(s))
				{
					// if we don already have it in the list, add it to the end.
					sites.Items.Add(s);
				}
			}
			sites.EndUpdate();
		}

		private void up_Click(object sender, System.EventArgs e)
		{
			if (sites.Items.Count > 1 && sites.SelectedIndex != 0) 
			{	
				int i = sites.SelectedIndex;
				string t = (string)sites.Items[sites.SelectedIndex - 1];
				sites.Items[sites.SelectedIndex - 1] = sites.Items[sites.SelectedIndex];
				sites.Items[sites.SelectedIndex] = t;
				sites.SelectedIndex = i-1;
			}
		}

		private void down_Click(object sender, System.EventArgs e)
		{
			if (sites.Items.Count > 1 && sites.SelectedIndex != sites.Items.Count) 
			{
				int i = sites.SelectedIndex;
				string t = (string)sites.Items[sites.SelectedIndex + 1];
				sites.Items[sites.SelectedIndex + 1] = sites.Items[sites.SelectedIndex];
				sites.Items[sites.SelectedIndex] = t;
				sites.SelectedIndex = i+1;
			}
		}

		private void ok_Click(object sender, System.EventArgs e)
		{
			Config.SiteRanking = new ArrayList();
			foreach (string s in sites.Items)
			{
				Config.SiteRanking.Add(s);
			}
			Close();
		}

		private void cancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}
	}
}

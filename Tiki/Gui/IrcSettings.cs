using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Tiki.Gui
{
	/// <summary>
	/// Summary description for IrcSettings.
	/// </summary>
	public class IrcSettings : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox serverHostName;
		private System.Windows.Forms.TextBox serverPort;
		private System.Windows.Forms.TextBox username;
		private System.Windows.Forms.TextBox nickname;
		private System.Windows.Forms.TextBox password;
		private System.Windows.Forms.Button OKButton;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.CheckBox ssl;
		private System.Windows.Forms.Button newIrcServerButton;
		private System.Windows.Forms.Button deleteIrcServerButton;
		private System.Windows.Forms.Button saveCurrentIrcServerButton;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		private System.Windows.Forms.ListView ircServers;

		private bool changed = false;

		public IrcSettings(ArrayList ircs)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			/*if (ircs != null) 
			{
				this.serverHostName.Text = ircs.Hostname;
				this.serverPort.Text = Convert.ToString(ircs.Port);
				this.nickname.Text = ircs.Nickname;
				this.username.Text = ircs.Username;
				this.password.Text = ircs.Password;
				this.ssl.Checked = ircs.Ssl;
			}*/
			Populate(ircs);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(IrcSettings));
			this.serverHostName = new System.Windows.Forms.TextBox();
			this.serverPort = new System.Windows.Forms.TextBox();
			this.username = new System.Windows.Forms.TextBox();
			this.nickname = new System.Windows.Forms.TextBox();
			this.password = new System.Windows.Forms.TextBox();
			this.OKButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			this.ssl = new System.Windows.Forms.CheckBox();
			this.newIrcServerButton = new System.Windows.Forms.Button();
			this.deleteIrcServerButton = new System.Windows.Forms.Button();
			this.saveCurrentIrcServerButton = new System.Windows.Forms.Button();
			this.ircServers = new System.Windows.Forms.ListView();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
			this.SuspendLayout();
			// 
			// serverHostName
			// 
			this.serverHostName.Location = new System.Drawing.Point(8, 8);
			this.serverHostName.Name = "serverHostName";
			this.serverHostName.Size = new System.Drawing.Size(136, 20);
			this.serverHostName.TabIndex = 0;
			this.serverHostName.Text = "";
			this.serverHostName.TextChanged += new System.EventHandler(this.serverHostName_TextChanged);
			// 
			// serverPort
			// 
			this.serverPort.Location = new System.Drawing.Point(144, 8);
			this.serverPort.Name = "serverPort";
			this.serverPort.Size = new System.Drawing.Size(40, 20);
			this.serverPort.TabIndex = 1;
			this.serverPort.Text = "";
			this.serverPort.TextChanged += new System.EventHandler(this.serverPort_TextChanged);
			// 
			// username
			// 
			this.username.Location = new System.Drawing.Point(240, 8);
			this.username.Name = "username";
			this.username.Size = new System.Drawing.Size(64, 20);
			this.username.TabIndex = 3;
			this.username.Text = "";
			this.username.TextChanged += new System.EventHandler(this.username_TextChanged);
			// 
			// nickname
			// 
			this.nickname.Location = new System.Drawing.Point(184, 8);
			this.nickname.Name = "nickname";
			this.nickname.Size = new System.Drawing.Size(56, 20);
			this.nickname.TabIndex = 2;
			this.nickname.Text = "";
			this.nickname.TextChanged += new System.EventHandler(this.nickname_TextChanged);
			// 
			// password
			// 
			this.password.Location = new System.Drawing.Point(304, 8);
			this.password.Name = "password";
			this.password.Size = new System.Drawing.Size(56, 20);
			this.password.TabIndex = 4;
			this.password.Text = "";
			this.password.TextChanged += new System.EventHandler(this.password_TextChanged);
			// 
			// OKButton
			// 
			this.OKButton.Location = new System.Drawing.Point(8, 248);
			this.OKButton.Name = "OKButton";
			this.OKButton.Size = new System.Drawing.Size(56, 24);
			this.OKButton.TabIndex = 5;
			this.OKButton.Text = "OK";
			this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
			// 
			// cancelButton
			// 
			this.cancelButton.Location = new System.Drawing.Point(72, 248);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(56, 24);
			this.cancelButton.TabIndex = 6;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
			// 
			// ssl
			// 
			this.ssl.Enabled = false;
			this.ssl.Location = new System.Drawing.Point(376, 8);
			this.ssl.Name = "ssl";
			this.ssl.Size = new System.Drawing.Size(24, 24);
			this.ssl.TabIndex = 10;
			this.ssl.CheckedChanged += new System.EventHandler(this.ssl_CheckedChanged);
			// 
			// newIrcServerButton
			// 
			this.newIrcServerButton.Location = new System.Drawing.Point(328, 248);
			this.newIrcServerButton.Name = "newIrcServerButton";
			this.newIrcServerButton.Size = new System.Drawing.Size(80, 24);
			this.newIrcServerButton.TabIndex = 12;
			this.newIrcServerButton.Text = "New";
			this.newIrcServerButton.Click += new System.EventHandler(this.newIrcServerButton_Click);
			// 
			// deleteIrcServerButton
			// 
			this.deleteIrcServerButton.Location = new System.Drawing.Point(240, 248);
			this.deleteIrcServerButton.Name = "deleteIrcServerButton";
			this.deleteIrcServerButton.Size = new System.Drawing.Size(80, 24);
			this.deleteIrcServerButton.TabIndex = 13;
			this.deleteIrcServerButton.Text = "Delete";
			this.deleteIrcServerButton.Click += new System.EventHandler(this.deleteIrcServerButton_Click);
			// 
			// saveCurrentIrcServerButton
			// 
			this.saveCurrentIrcServerButton.Location = new System.Drawing.Point(152, 248);
			this.saveCurrentIrcServerButton.Name = "saveCurrentIrcServerButton";
			this.saveCurrentIrcServerButton.Size = new System.Drawing.Size(80, 24);
			this.saveCurrentIrcServerButton.TabIndex = 14;
			this.saveCurrentIrcServerButton.Text = "Save current";
			this.saveCurrentIrcServerButton.Click += new System.EventHandler(this.saveCurrentIrcServerButton_Click);
			// 
			// ircServers
			// 
			this.ircServers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																						 this.columnHeader1,
																						 this.columnHeader2,
																						 this.columnHeader3,
																						 this.columnHeader4,
																						 this.columnHeader5,
																						 this.columnHeader6});
			this.ircServers.FullRowSelect = true;
			this.ircServers.GridLines = true;
			this.ircServers.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.ircServers.Location = new System.Drawing.Point(8, 32);
			this.ircServers.Name = "ircServers";
			this.ircServers.Size = new System.Drawing.Size(400, 208);
			this.ircServers.Sorting = System.Windows.Forms.SortOrder.Ascending;
			this.ircServers.TabIndex = 15;
			this.ircServers.View = System.Windows.Forms.View.Details;
			this.ircServers.SelectedIndexChanged += new System.EventHandler(this.ircServers_SelectedIndexChanged);
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "hostname";
			this.columnHeader1.Width = 136;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "port";
			this.columnHeader2.Width = 39;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "nickname";
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "username";
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "password";
			// 
			// columnHeader6
			// 
			this.columnHeader6.Text = "ssl";
			this.columnHeader6.Width = 32;
			// 
			// IrcSettings
			// 
			this.AcceptButton = this.OKButton;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.cancelButton;
			this.ClientSize = new System.Drawing.Size(418, 279);
			this.Controls.Add(this.ircServers);
			this.Controls.Add(this.saveCurrentIrcServerButton);
			this.Controls.Add(this.deleteIrcServerButton);
			this.Controls.Add(this.newIrcServerButton);
			this.Controls.Add(this.ssl);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.OKButton);
			this.Controls.Add(this.password);
			this.Controls.Add(this.username);
			this.Controls.Add(this.nickname);
			this.Controls.Add(this.serverPort);
			this.Controls.Add(this.serverHostName);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "IrcSettings";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Settings: IRC";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.IrcSettings_Closing);
			this.Load += new System.EventHandler(this.IrcSettings_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void Populate(ArrayList ircs)
		{
			for (int i = 0; i < ircs.Count; i++)
			{
				IrcConnectionSettings irc = (IrcConnectionSettings) ircs[i];
				ListViewItem lvi = new ListViewItem(new string[] {irc.Hostname, Convert.ToString(irc.Port), irc.Nickname, irc.Username, irc.Password, Convert.ToString(irc.Ssl) });
				ircServers.Items.Add(lvi);
			}
		}
		private void newIrcServerButton_Click(object sender, System.EventArgs e)
		{
			if (changed)
			{
				CheckSave();
			}
			ClearFields();
		}

		private void CheckSave()
		{
			DialogResult dr = MessageBox.Show("The values have changed, do you wish to save?", "Save?", MessageBoxButtons.YesNo);
			if (dr == DialogResult.Yes)
			{
				SaveServer();
			}
		}

		private bool SaveServer()
		{
			try
			{
				Convert.ToInt32(serverPort.Text);
				ListViewItem lvi = new ListViewItem(new string[] {serverHostName.Text, serverPort.Text, nickname.Text, username.Text, password.Text, Convert.ToString(ssl.Checked)});
				ircServers.Items.Add(lvi);
				changed = false;
				return true;
			}
			catch
			{
				// change this later to a field that only accepts numbers, in a cool way =)
				MessageBox.Show("Please make sure that there are only digits in the 'port' field");
				return false;
			}
		}

		private void SaveServerList()
		{
			Config.IrcConnectionSettings.Clear();
			for (int i = 0; i < ircServers.Items.Count; i++)
			{
				ListViewItem lvi = (ListViewItem) ircServers.Items[i];
				ListViewItem.ListViewSubItemCollection items = lvi.SubItems;
				IrcConnectionSettings irccs = new IrcConnectionSettings(items[0].Text, Convert.ToInt32(items[1].Text), items[3].Text, items[2].Text, items[4].Text, Boolean.Parse(items[5].Text));
				Config.IrcConnectionSettings.Add(irccs);
			}
		}

		private void ClearFields()
		{
			this.serverHostName.Text = "";
			this.serverPort.Text = "";
			this.nickname.Text = "";
			this.username.Text = "";
			this.password.Text = "";
			this.ssl.Checked = false;
			changed = false;
		}

		private void deleteIrcServerButton_Click(object sender, System.EventArgs e)
		{
			if (ircServers.SelectedItems.Count == 1) 
			{
				ListViewItem lvi = ircServers.SelectedItems[0];
				ircServers.Items.Remove(lvi);
			}
			else
			{
				MessageBox.Show("You must select a server to delete first");
			}
		}

		private void saveCurrentIrcServerButton_Click(object sender, System.EventArgs e)
		{
			bool saved = false;
			if (changed)
			{
				saved = SaveServer();
			}
			if (saved) 
			{
				ClearFields();
			}

		}

		#region changed
		private void serverHostName_TextChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void serverPort_TextChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void nickname_TextChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void username_TextChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void password_TextChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}

		private void ssl_CheckedChanged(object sender, System.EventArgs e)
		{
			changed = true;
		}
		#endregion

		private void ircServers_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (ircServers.SelectedItems.Count == 1)
			{
				ListViewItem.ListViewSubItemCollection items = ircServers.SelectedItems[0].SubItems;
				this.serverHostName.Text = items[0].Text;
				this.serverPort.Text = items[1].Text;
				this.nickname.Text = items[2].Text;
				this.username.Text = items[3].Text;
				this.password.Text = items[4].Text;
				this.ssl.Checked = Boolean.Parse(items[5].Text);
			}
		}

		private void IrcSettings_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			
		}

		private void IrcSettings_Load(object sender, System.EventArgs e)
		{
			
		}

		private void OKButton_Click(object sender, System.EventArgs e)
		{
			SaveServerList();
			Close();
		}

		private void cancelButton_Click(object sender, System.EventArgs e)
		{
			Close();
		}
	}
}

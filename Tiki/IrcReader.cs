using System;
using System.IO;

namespace Tiki
{
	/// <summary>
	/// Summary description for IrcReader.
	/// </summary>
	public class IrcReader
	{
		private StreamReader reader;
		private IrcParser parser;

		/// <summary>
		/// Creates the reader thread. This will read stuff from the server and send it to the parser.
		/// </summary>
		/// <param name="reader">The reader to read from.</param>
		public IrcReader(StreamReader reader, IrcParser parser)
		{
			this.reader = reader;
			this.parser = parser;
		}

		public void run()
		{
			string line;
			while ((line = reader.ReadLine()) != null)
			{
				Response r = new Response(line);
				parser.parse(r);
			}
		}
	}
}

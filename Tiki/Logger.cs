using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Tiki
{
	public delegate void Output(string message);
	/// <summary>
	/// Summary description for Logger.
	/// </summary>
	public class Logger
	{
		private static Regex newline = new Regex(@"(?:\r\n|\r|\n|" + Environment.NewLine + ")" , RegexOptions.Compiled);
		private static object myLock = new object();
		private static StreamWriter writer = null;
		private static Output output = null;
		
		public static void Initialize()
		{
			if (writer == null)
			{
				writer = new StreamWriter("debug-" + System.DateTime.Now.Year + "-" + (System.DateTime.Now.Month < 10 ? "0" + System.DateTime.Now.Month.ToString() : System.DateTime.Now.Month.ToString()) + "-" + System.DateTime.Now.Day + "-" +(System.DateTime.Now.TimeOfDay.ToString().Length == 16 ? System.DateTime.Now.TimeOfDay.ToString().Replace(':', '.') : System.DateTime.Now.TimeOfDay.ToString().Replace(':', '.') + ".0000000") + ".log", true, Encoding.ASCII);
				writer.WriteLine(Environment.NewLine + "New Logging session started at: " + System.DateTime.Now + Environment.NewLine);
				writer.Flush();
			}
		}

		public static void Log(string linePrefix, string message)
		{
			// this stops output from multiple threads to be written at the same time, now they will be logged in an orderly fashion
			lock (myLock) 
			{
				// todo: either use classes of logging like log4j, or use a debug flag
				string[] data = newline.Split(message);
				for (int i = 0; i < data.Length; i++)
				{
					string s = data[i];
					string line = (System.DateTime.Now.TimeOfDay.ToString().Length == 16 ? System.DateTime.Now.TimeOfDay.ToString() : System.DateTime.Now.TimeOfDay.ToString() + ".0000000") + " : " + Thread.CurrentThread.Name + " : " + linePrefix + " " + s;
					Console.Out.WriteLine(line);
					if (writer != null) 
					{
						writer.WriteLine(line);
						writer.Flush();
					}
					if (output != null)
					{
						output(line);
					}
				}
			}
		}
		public static void Log(string message)
		{
			Log("", message);
		}

		public static Output Output
		{
			get { return output; }
			set { output = value; }
		}

		public static void Close()
		{
			writer.Close();
		}
	}
}

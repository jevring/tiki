using System;
using System.IO;
using System.Net.Sockets;

namespace Tiki
{
	/// <summary>
	/// Summary description for IdentRequestHandler.
	/// </summary>
	public class IdentRequestHandler
	{
		private TcpClient client;
		private StreamReader reader;
		private StreamWriter writer;


		public IdentRequestHandler(TcpClient client)
		{
			this.client = client;
		}

		public void HandleIdentRequest()
		{
			reader = new StreamReader(client.GetStream());
			writer = new StreamWriter(client.GetStream());
			string line = reader.ReadLine();
				
			//Logger.Log("Got ident request: '" + line + "' from " + client.ToString());
			string[] data = line.Split(',');
			string ident = (string)Config.GeneralSettings["ident"];
			if (ident == null || ident.Length == 0)
			{
				ident = "unknown";
			}
			string response = data[1] + " , " + data[0] + " : USERID : UNIX : " + ident + "\r\n";
			//Logger.Log("Responding with: " + response);
			writer.Write(response);
			writer.Flush();
			reader.Close();
			writer.Close();
			client.Close();
		}
	}
}

using System;
using System.Collections;

namespace Tiki
{
	/// <summary>
	/// Summary description for GroupNameStatisticsEntry.
	/// </summary>
	[Serializable]
	public class GroupNameStatisticsEntry
	{
		private string groupname;
		private Hashtable sectionToFrequency;
		private int numberOfReleases;
		private string alwaysTreatThisGroupAsSection = "";

		public GroupNameStatisticsEntry(string groupname)
		{
			this.groupname = groupname;
			sectionToFrequency = new Hashtable();
			numberOfReleases = 0;
		}

		public void NewRelease(string section)
		{
			if (!"unknown".Equals(section)) 
			{
				int i = 0;
				if (sectionToFrequency.Contains(section))
				{
					i = (int)sectionToFrequency[section];
				}
				sectionToFrequency[section] = i++;
				numberOfReleases++;
			}
		}
		public double GetSectionProbability(string section)
		{
			Logger.Log("Group " + groupname);
			if ("".Equals(alwaysTreatThisGroupAsSection)) 
			{
				int numberOfReleasesForSection = 0;
				foreach (DictionaryEntry entry in sectionToFrequency)
				{
					Logger.Log("section: " + entry.Key + " number of releases: " + entry.Value + " ratio: " + ((int)entry.Value / numberOfReleases));
					if (entry.Key.Equals(section))
					{
						numberOfReleasesForSection = (int)entry.Value;
					}
				}
				return numberOfReleasesForSection / numberOfReleases;
			}
			else
			{
				if (alwaysTreatThisGroupAsSection.Equals(section))
				{
					return 1.0;
				}
				else
				{
					return 0.0;
				}
			}
		}
		public string GetMostProbableSection()
		{
			string currentSection = "unknown";
			double currentProbability = 0.0d;
			if ("".Equals(alwaysTreatThisGroupAsSection)) 
			{
				foreach (DictionaryEntry entry in sectionToFrequency)
				{
					Logger.Log("section: " + entry.Key + " number of releases: " + entry.Value + " ratio: " + ((int)entry.Value / numberOfReleases));
					// this only becomes relevant after we've had a certain amount of releases submitted
					if (((int)entry.Value / numberOfReleases) > currentProbability && ((int)entry.Value) > 5)
					{
						currentProbability = ((int)entry.Value / numberOfReleases);
						currentSection = (string)entry.Key;
					}
				}
				return currentSection;
			}
			else
			{
				return alwaysTreatThisGroupAsSection;
			}
		}

		public string AlwaysTreatThisGroupAsSection
		{
			get { return alwaysTreatThisGroupAsSection; }
			set { alwaysTreatThisGroupAsSection = value; }
		}
	}
}

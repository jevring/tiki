using System;

namespace Tiki
{
	/// <summary>
	/// Summary description for IrcConnectionSettings.
	/// </summary>
	[Serializable]
	public class IrcConnectionSettings
	{
		private string hostname;
		private int port;
		private string username;
		private string nickname;
		private string password;
		private bool ssl;

		public IrcConnectionSettings(string hostname, int port, string username, string nickname, string password, bool ssl)
		{
			this.hostname = hostname;
			this.port = port;
			this.username = username;
			this.nickname = nickname;
			this.password = password;
			this.ssl = ssl;
		}

		public string Hostname
		{
			get { return hostname; }
		}

		public int Port
		{
			get { return port; }
		}

		public string Username
		{
			get { return username; }
		}

		public string Nickname
		{
			get { return nickname; }
		}

		public string Password
		{
			get { return password; }
		}

		public bool Ssl
		{
			get { return ssl; }
		}

		public override string ToString()
		{
			return hostname + ";" + port + ";" + username + ";" + nickname + ";" + ssl + ";" + password;
		}

	}
}

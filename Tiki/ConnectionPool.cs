using System;
using System.Collections;
using System.Threading;

namespace Tiki
{
	/// <summary>
	/// Summary description for ConnectionPool.
	/// </summary>
	public class ConnectionPool
	{
		private static Hashtable connections = new Hashtable();
		private static Hashtable threads = new Hashtable();
		private static Hashtable checkedOutConnections = new Hashtable();
		private static Hashtable deadSites = new Hashtable();
		private static Hashtable busySites = new Hashtable();
		private static Hashtable siteToRaceConnection = new Hashtable();
		private static Hashtable sitesBeingPurged = new Hashtable();
		private static Hashtable numRetriedConnections = new Hashtable();
		private static int MAXRECONNECTTRIALS = 4;
		private static object myLock = new object();

		public static void Initialize()
		{
			foreach (Site site in Config.Sites.Values)
			{
				if (site.AllowsIdle)
				{
					Connect(site);
				}
				if (!site.AllowAsDestination && !site.AllowAsSource && !site.AllowsIdle)
				{
					// this keeps "disabled" sites from being available
					ReportDeadSite(site.Name);
				}
			}
		}
		public static void ClearAutoDeadSites()
		{
			deadSites.Clear();
			foreach (Site site in Config.Sites.Values)
			{
				if (!site.AllowAsDestination && !site.AllowAsSource && !site.AllowsIdle)
				{
					// this keeps "disabled" sites from being available
					ReportDeadSite(site.Name);
				}
			}
		}
		public static void Connect(Site site)
		{
			Connect(site, false);
		}

		public static void Connect(Site site, bool waitForConnectToFinish)
		{
			FtpConnection source = null;
			FtpConnection destination = null;
			if (!connections.Contains(site.Name + "-DST") && !connections.Contains(site.Name + "-SRC"))
			{
				// don't ever enclose these in conditionals, they must always exist, even if they are not connected!
				// source
				source = new FtpConnection(site, true);
				connections[site.Name + "-SRC"] = source;
				// destination
				destination = new FtpConnection(site, false);
				connections[site.Name + "-DST"] = destination;
				
			}
			else
			{
				// todo: check their connected status. any that aren't connected, but should be (depending on what loginresponsecode they return) should start a new conenciton thread
				// OR, should we jsut skip this conditional? there should be no other references to these connections.
				// TODO: ponder this
				// no, the callback function takes care of any reconnets due to any kind of response

				// disconencted sites without reconnect doesn't seem to ever want to be conencted again.
				source = (FtpConnection)connections[site.Name + "-SRC"];
				destination = (FtpConnection)connections[site.Name + "-DST"];
			}
			Thread s = null;
			if (site.AllowAsSource && !source.Connected && !source.Connecting) 
			{
				s = new Thread(new ThreadStart(source.Connect));
				threads[site.Name + "-SRC"] = s;
				s.Name = "Connect:" + site.Name + "(src)";
				s.IsBackground = true;
				s.Start();
			}
			Thread d = null;
			if (site.AllowAsDestination && !destination.Connected && !destination.Connecting) 
			{
				d = new Thread(new ThreadStart(destination.Connect));
				threads[site.Name + "-DST"] = d;
				d.Name = "Connect:" + site.Name + "(dst)";
				d.IsBackground = true;
				d.Start();
			}
			if (waitForConnectToFinish)
			{
				// this enables us to hold of execution until both of the connections have returned to their connected state.
				// right now this is only used for force reconnects.
				// it'll be interesting to see what happens if there is a problem in this branch
				if (s != null)
				{
					s.Join();
				}
				if (d != null)
				{
					d.Join();
				}
			}
		}

		public static void ReportDeadSite(string sitename)
		{
			Logger.Log("site " + sitename + " is dead");
			deadSites[sitename] = null;
		}
		public static void ReportBusySite(string sitename, bool busy)
		{
			if (busy) 
			{
				busySites[sitename] = System.DateTime.Now;
				Logger.Log("site " + sitename + " is busy");
			}
			else
			{
				if (busySites.Contains(sitename)) 
				{
					busySites.Remove(sitename);
					Logger.Log("site " + sitename + " is no longer busy");
				}
				numRetriedConnections[sitename] = 0;
			}
		}
		public static bool IsDead(string sitename)
		{
			return deadSites.Contains(sitename);
		}
		public static bool IsBusy(string sitename)
		{
			return busySites.Contains(sitename);
		}

		public static void ConnectSites(RaceAnnounce announce)
		{
			Logger.Log("connecting sites");
			Connect(announce.Source);
			for (int i = 0; i < announce.Destinations.Count; i++)
			{
				Site site = (Site) announce.Destinations[i];
				Connect(site);
			}
		}
		
		public static SiteConnection CheckoutConnection(string sitename, Race race)
		{
			// NOTE: only return connections that aren't 421 or 0 (what should we do with 530 (site full)?)

			SiteConnection sc = null;
			lock (myLock) 
			{
				if (!checkedOutConnections.Contains(sitename))
				{
					// connect the SiteConnection we're returning to a specified race.
					siteToRaceConnection[sitename] = race;
					Logger.Log("Found connection " + sitename + " and it's not checked out");
					sc = GetConnection(sitename);
					checkedOutConnections[sitename] = null;
				}
				else
				{
					Logger.Log("Connection " + sitename + " was already checked out");
				}
			}
			return sc;
		}
		public static void ReturnConnection(string sitename)
		{
			if (siteToRaceConnection.Contains(sitename))
			{
				siteToRaceConnection.Remove(sitename);
			}
			else
			{
				Logger.Log("Failed to dissassociate sitename from race, since the conneciton didn't exist");
			}
			if (checkedOutConnections.Contains(sitename))
			{
				Logger.Log("returned connection: " + sitename);
				checkedOutConnections.Remove(sitename);
			} 
			else
			{
				Logger.Log("failed to return connection " + sitename + " because it wasn't checked out");
			}

		}

		private static SiteConnection GetConnection(string sitename)
		{
			Logger.Log("g1");
			SiteConnection sc = null;
			// source
			Thread s = (Thread)threads[sitename + "-SRC"];
			Logger.Log("g2");

			// we have problems here with sites maked as DST only
			// todo: check this

			if (s != null)
			{
				// remove this block later, thsi is debug
				Logger.Log("s: " + s.Name + " current: " + Thread.CurrentThread.Name);
			}
			Logger.Log("g21");
			if (s != null && (!s.Name.Equals(Thread.CurrentThread.Name)))
			{
				
				Logger.Log("g3");
				threads.Remove(sitename + "-SRC");
				Logger.Log("g4");
				s.Join();
				Logger.Log("g5");
			}

			Logger.Log("g6");
			Thread d = (Thread)threads[sitename + "-DST"];
			Logger.Log("g7");
			if (d != null)
			{
				// remove this block later, thsi is debug
				Logger.Log("d: " + d.Name + " current: " + Thread.CurrentThread.Name);
			}
			Logger.Log("g71");
			if (d != null && (!d.Name.Equals(Thread.CurrentThread.Name)))
			{
				
				Logger.Log("g8");
				threads.Remove(sitename + "-DST");
				Logger.Log("g9: ");
				d.Join();
				Logger.Log("g10");
			}
			// when we get here, we have both our connections.
			// hmm, should return null if we're not connected (failed for some reason)

			Logger.Log("g11");
			FtpConnection source = (FtpConnection)connections[sitename + "-SRC"];
			Logger.Log("g12");
			FtpConnection destination = (FtpConnection)connections[sitename + "-DST"];
			// this check really shouldn't be necessary, since the IrcParser shouldhave checked this before, but we check anyhow
			Logger.Log("g13");
			if (!IsDead(sitename)) 
			{
				Logger.Log("g14");
				sc = new SiteConnection((Site)Config.Sites[sitename], source, destination);
				Logger.Log("g15");
				// this just adds the entry, let's not take up space with pointers to other data (we really just need a set here)
				
			}
			Logger.Log("g16");
			return sc;
		}

		public static void ReportConnectionState(FtpConnection conn)
		{
			// if we're connected and all is well, that's fine. it's the problems we need to deal with.
			if (conn.LoginResponseCode == 0 || conn.LoginResponseCode == 421)
			{
				// hmm, this won't do. if this site has more than one bnc, then it'll be down if the first bnc is down.
				// or not, this will only report after trying all the bncs
				Logger.Log("Found dead site: " + conn.Site.Name);
				ReportDeadSite(conn.Site.Name);
			}
			else if (conn.LoginResponseCode == 530)
			{
				// actually, this should flag the site as down or something, so we can start racing and not have to wait for the site to check out
				// we should have a temp dead or blocked or full list or whatever, that we can add sites too, like if we're in the middle of a race, and we're not allowed onto the site
				ReportBusySite(conn.Site.Name, true);
				// retry the connect
				// maybe we should wait a bit before retrying, and the ftpconnection execution can just skip this site or something
				if (conn.LoginErrorMessage.ToLower().IndexOf("login incorrect") > -1)
				{
					Logger.Log("WARNING: Password specified for site " + conn.Site.Name + " was wrong, please update");
					ReportDeadSite(conn.Site.Name);
				}
				else if (conn.LoginErrorMessage.ToLower().IndexOf("site is full") > -1)
				{
					// what should we do if the site is full?
					// skip it I think?
					// or maybe run the race, and when the rest of the hosts are done, add the full site to the list
					// on fast sites with small races, we might aswell just skip this site in the race.
					// if we do, how tdo we let the race know to skip this site?
					// check what race it's currently occupied by, and tell it to remove this site
					// this shouldn't happen often, since we'll mostly be idling on the sites anyway, 
					// so go ahead and skip this site from the race, but keep retrying, at a nice, long interval
					// use a general setting for the interval, named reconnection interfval or somesuch, usually longer than 30 seconds

					RemoveSiteFromRace(conn.Site);
					// start a reconnect thread that wait for [reconnect_timeout] seconds
					int timeout = (int)Config.GeneralSettings["reconnect_timeout"];
					// question, will this block the entire execution. I shouldn't think so. It might, however, offset parallell threads wanting to reconnect.
					// ohh well, well deal with that when the rest is working, this happens very seldom
					Thread.Sleep(timeout * 1000);
					Reconnect(conn.Site);
					
					
				}
				else if (conn.LoginErrorMessage.ToLower().IndexOf("is not valid for the account specified") > -1)
				{
					// todo: parse the "ident@ip.ip.ip.ip" line here, if someone changes glstrings
					// this means you don't have the proper IP added for this site
					Logger.Log("WARNING: IP specified on site " + conn.Site.Name + " was wrong, please update");
					ReportDeadSite(conn.Site.Name);
				}
				else 
				{
					// if we get a "account limited to n logins" here, and the other connection isn't busy, then we'll disconnect to other one, and log in with !username, log out, and then relogin with both accounts
					// todo: if we get the "too many connections for this user" message more than once, we disconnect both connections for the site, log in with !username, cwd to the last known dir (to cleanse via cubnc aswell), log out, and then log both back in again with no !

					// todo: find THE race+transfer the connection is involved in (if there are 2, then we obviously have no ghosts)
					// no race+transfers are also fine
					if (conn.LoginErrorMessage.ToLower().IndexOf("sorry, your maximum number of connections from this ip") > -1 || conn.LoginErrorMessage.ToLower().IndexOf("sorry, your account is restricted to") > -1 || conn.LoginErrorMessage.ToLower().IndexOf("sorry, your group is restricted to") > -1)
					{
						ForceReconnect(conn.Site.Name);
					}
					else
					{
						// todo: restrict this to a certain number of possible retries
						Reconnect(conn.Site);
						Logger.Log("Retrying connection to: " + conn.Site.Name);
					}
				}
			}
			else if (conn.LoginResponseCode == 230)
			{
				// this is the only acceptable state. or rather, the only state where we have succeeded
				// maybe this should be with a (dst)/(src) suffixed...
				ReportBusySite(conn.Site.Name, false);
			}
			else
			{
				// do something else, this happens, for example, if the user supplied a faulty username or password, or is connecting from the wrong ip
				Logger.Log("Some other error upon connection, tagging this site as dead. Errorcode: " + conn.LoginResponseCode);
				ReportDeadSite(conn.Site.Name);
			}
		}

		private static void RemoveSiteFromRace(Site site)
		{
			// ACTION:
			if (siteToRaceConnection.Contains(site.Name))
			{
				Race race = (Race)siteToRaceConnection[site.Name];

				// remove site from race (wanted and active)
				race.RemoveSite(site);

				// remove the site to race mapping
				siteToRaceConnection.Remove(site.Name);
			}
		}

		private static void Reconnect(Site site)
		{
			int i = 1;
			if (numRetriedConnections.Contains(site.Name))
			{
				i = (int)numRetriedConnections[site.Name];
				i++;
				numRetriedConnections[site.Name] = i;
			}
			// only let it try to reconnect a max number of times. otherwise, report the site as dead. If we get a connection, we reset the counter.
			if (i < MAXRECONNECTTRIALS)
			{
				Connect(site);
			}
			else
			{
				// todo: should we remove this site from any possible races it might be in aswell?
				RemoveSiteFromRace(site);
				ReportDeadSite(site.Name);
			}
		}

		public static void ForceReconnect(string sitename)
		{
			Logger.Log("we had ghosts, or connections from elsewhere. step aside, tiki is coming through! purge all connections, and reconnect.");
			if (!sitesBeingPurged.Contains(sitename)) 
			{
				sitesBeingPurged[sitename] = null;
				Race race = (Race)siteToRaceConnection[sitename];
				Logger.Log("1");
				SiteConnection sc = GetConnection(sitename);
				Logger.Log("2");
				if (race != null)
				{
					Logger.Log("3");
					race.Pause();
					Logger.Log("4");
					if (sc.Source.Transfer != null)
					{
						Logger.Log("5");
						sc.Source.Transfer.Pause();
					}
					// problems with checking .Transferring here, since it can have that set to true, milliseconds before the tansfer actualy starts
					if (sc.Destination.Transfer != null)
					{
						Logger.Log("6");
						sc.Destination.Transfer.Pause();
					}
					Logger.Log("7");
					PurgeGhostsOnSiteAndReconnect(sc);
					Logger.Log("8");
					if (sc.Source.Transfer != null)
					{
						Logger.Log("9");
						sc.Source.Transfer.Continue();
					}
					if (sc.Destination.Transfer != null)
					{
						Logger.Log("10");
						sc.Destination.Transfer.Continue();
					}
					Logger.Log("11");
					race.Continue();
					Logger.Log("12");
				}
				else
				{
					Logger.Log("13");
					PurgeGhostsOnSiteAndReconnect(sc);
					Logger.Log("14");
				}
				Logger.Log("15");
				sitesBeingPurged.Remove(sitename);
			}
			else
			{
				Logger.Log("we're alreday in the progress of purging site " + sitename + " please hold...");
			}
		}
		public static void PurgeGhostsOnSiteAndReconnect(SiteConnection sc)
		{
			Logger.Log("Purging ghosts for site: " + sc.Site.Name);
			// pwd is for cubnc purging.
			
			// if one of the connections is transferring, wait until the current file is over, then pause the transfer.
			// DONE in above

			// log out both connections
			sc.Source.Disconnect(false);
			sc.Destination.Disconnect(false);

			// login to source with !username
			sc.Source.Purge = true;
			// this will call back, so we need to take care of this with a state for that site.
			// no, that won't be a problem, it'll return a 230, so we're fine
			sc.Source.Connect();

			if (sc.Source.Connected) 
			{
				// cwd to sourcepwd
				if (sc.Source.Pwd != null && !"".Equals(sc.Source.Pwd)) 
				{
					string oldSourcePwd = sc.Source.Pwd;
					sc.Source.Cwd(sc.Source.Pwd);
					// cwd to /
					sc.Source.Cwd("/");
					sc.Source.Pwd = oldSourcePwd;
				}
				// cwd to destinationpwd
				if (sc.Destination.Pwd != null && !"".Equals(sc.Destination.Pwd)) 
				{
					string oldDestinationPwd = sc.Destination.Pwd;
					sc.Source.Cwd(sc.Destination.Pwd);
					sc.Source.Cwd("/");
					sc.Destination.Pwd = oldDestinationPwd;
				}
				// disconnect
				sc.Source.Disconnect(false);
				sc.Source.Purge = false;
				// call Connect(sitename)
				// the 'true' tells the system to wait until the connections are completed before returning.
				Logger.Log("Done purging site: " + sc.Site.Name);
				Connect(sc.Site, true);
				Logger.Log("Reconnected to site: " + sc.Site.Name);
			}
			else
			{
				// we didn't manage to reconnect for some reason, let's try that again
				// or not, maybe we should just oust this site from the race
				Logger.Log("We failed to reconnect in the beginning of the force-purge for site " + sc.Site.Name);
				// this should rarely, if ever happen (unless we hit a "site is full", in which case the site will be removed from the race.
				// todo: figure out what action we should take now that we're here.
			}
		}
		public static Hashtable Connections
		{
			get { return connections; }
		}

		public static Hashtable CheckedOutConnections
		{
			get { return checkedOutConnections; }
		}

		public static void Shutdown()
		{
			foreach (FtpConnection conn in connections.Values)
			{
				if (!IsDead(conn.Site.Name)) 
				{
					conn.Terminate(false);
				}
			}
		}
	}
}

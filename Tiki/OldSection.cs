using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace Tiki
{
	/// <summary>
	/// Summary description for Section.
	/// </summary>
	[Serializable]
	public class OldSection
	{
		private string name;
		private Site site;
		private string path;
		private string botname;
		private bool active;
		// rules
		private ArrayList alwaysAllow;
		private ArrayList banned;
		private ArrayList allowedYears;

		public OldSection(string name, Site site, string path, string botname, bool active)
		{
			alwaysAllow = new ArrayList();
			banned = new ArrayList();
			allowedYears = new ArrayList();

			this.name = name.ToLower();
			this.site = site;
			this.path = path;
			this.botname = botname;
			this.active = active;
		}

		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		public Site Site
		{
			get { return site; }
			set { site = value; }
		}

		public string Path
		{
			get { return path; }
			set { path = value; }
		}

		public string Botname
		{
			get { return botname; }
			set { botname = value; }
		}

		public bool Active
		{
			get { return active; }
			set { active = value; }
		}

		public ArrayList AlwaysAllow
		{
			get { return alwaysAllow; }
			set { alwaysAllow = value; }
		}

		public ArrayList Banned
		{
			get { return banned; }
			set { banned = value; }
		}

		public ArrayList AllowedYears
		{
			get { return allowedYears; }
			set { allowedYears = value; }
		}

		public double GetWeight()
		{
			// todo: fix this!
			return 1.0;
		}
		
		public override string ToString()
		{
			return name;
		}

	}
}

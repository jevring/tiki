using System;
using System.Collections;
using System.Threading;

namespace Tiki
{
	/// <summary>
	/// Summary description for FxpTransfer.
	/// </summary>
	public class FxpTransfer
	{
		private Queue queue;
		private FtpConnection source;
		private FtpConnection destination;
		private Race race;
		private string filename;
		private double speedOfLastTransfer = 0.0;
		private Hashtable dupes;
		private bool ending = false;
		private int currentFileSize = 0;
		private DateTime queueStartTime;
		private DateTime currentFileTransferStartTime;
		private bool pause = false;
		private Hashtable skippedFiles;
		private Thread executingThread = null;

		public FxpTransfer(Queue queue, FtpConnection source, FtpConnection destination, Race race)
		{
			source.Transferring = true;
			destination.Transferring = true;
			this.queue = queue;
			this.source = source;
			this.destination = destination;
			this.race = race;
			this.dupes = new Hashtable();
			this.skippedFiles = new Hashtable();
		}

		public void Start()
		{
			queueStartTime = System.DateTime.Now;
			Logger.Log("REAL! fxp between " + source.Site.Name + " and " + destination.Site.Name);
			while (queue.Count > 0 && !ending)
			{
				if (pause)
				{
					while (pause)
					{
						Logger.Log("Transfer " + this.Id + " paused to cleansing. sleeping 4 seconds");
						Thread.Sleep(4000);
					}
					// use .Continue() to get out of this situation.
				}
				filename = (string)queue.Dequeue();
				if (dupes.Contains(filename))
				{
					Logger.Log("Found dupe (via xdupe), skipping: " + filename);
					continue;
				}
				// we simply KNOW that we're in the right dirs before we do this...
				Logger.Log("transferring " + filename + " from " + source.Site.Name + " to " + destination.Site.Name);
				currentFileTransferStartTime = System.DateTime.Now;
				FxpResult fxpr = source.SendFile(filename, destination, this);
				if (ending)
				{
					break;
				}
				if (fxpr.Rc != 0)
				{
					Logger.Log("s00");
					
					race.IncreaseProgress(destination.Site.Name);
					// only add the speed if the file is big enough for a proper measurement
					speedOfLastTransfer = Convert.ToDouble(Convert.ToDouble(fxpr.FileSize) / Convert.ToDouble(Convert.ToDouble(fxpr.EndTime - fxpr.StartTime) / Convert.ToDouble(10000000)) );
					Logger.Log("Filesize was: " + fxpr.FileSize);
					if (speedOfLastTransfer > 0 && fxpr.FileSize > 1048576)
					{
						Config.SpeedMatrix.SetSpeed(source.Site, destination.Site, speedOfLastTransfer);
					}
					else
					{
						Logger.Log("speed was 0 or file was too small: speed: " + speedOfLastTransfer + " filesize: " + fxpr.FileSize + " size conditional is " + (fxpr.FileSize > 1048576));
						// faking last transfer speed:
						speedOfLastTransfer = Config.SpeedMatrix.GetSpeed(source.Site, destination.Site);
					}
					// update list of what files have been sent from a certain source (and how many times)
				}
				else if (fxpr.Rc == -1)
				{
					if (!skippedFiles.Contains(filename)) 
					{
						Logger.Log("We FAILED to transfer file: " + filename + " because of " + fxpr.Reponse + ". Retransferring...");
						// resend the the file
						queue.Enqueue(filename);
					}
					// otherwise this file is skipped in this trasnfer, maybe it can be tried again later.
					// this is more of a faisafe, so we won't get into the 05 cindy lauper foreverfile deal
				}
				race.UpdateStatus();
			}
			Logger.Log("s1");
			filename = "";
			source.Transferring = false;
			destination.Transferring = false;
			race.EndTransfer(this);
			Logger.Log("s2");
		}

		public void ReportXdupe(Hashtable dupes)
		{
			this.dupes = dupes;
		}

		public int CurrentFileSize
		{
			get { return currentFileSize; }
			set
			{
				currentFileSize = value;
				race.UpdateStatus();
			}
		}

		public void ClearQueue()
		{
			// this is used when we're waiting for a transfer to be completed. 
			// Because if a host has been tagged complete based on the current file being trasnferred, and if the queue is longer than 0
			// the remaining files in the queue are already at the destination
			queue.Clear();
		}

		public void End(bool force)
		{
			ending = true;
			if (force)
			{
				if (executingThread != null)
				{
					executingThread.Abort();
					// the join makes sure that the thread has aborted
					executingThread.Join();
					source.Transferring = false;
					destination.Transferring = false;
					source.Execute("ABOR");
					destination.Execute("ABOR");
				}
			}
		}
		public void Pause()
		{
			Logger.Log("Pausing transfer...");
			// doesn't return until that file is completed.
			pause = true;
		}
		public void Continue()
		{
			Logger.Log("Continuing transfer...");
			pause = false;
		}

		public Queue Queue
		{
			get { return queue; }
		}

		public FtpConnection Source
		{
			get { return source; }
		}

		public FtpConnection Destination
		{
			get { return destination; }
		}

		public string Filename
		{
			get { return filename; }
		}

		public double SpeedOfLastTransfer
		{
			get { return speedOfLastTransfer; }
		}

		public DateTime QueueStartTime
		{
			get { return queueStartTime; }
		}

		public DateTime CurrentFileTransferStartTime
		{
			get { return currentFileTransferStartTime; }
		}
		public string Id
		{
			get
			{
				return source.Site.Name + "->" + destination.Site.Name;
			}
		}

		public Thread ExecutingThread
		{
			get { return executingThread; }
			set { executingThread = value; }
		}

		public void SkipCurrentFile()
		{
			skippedFiles[filename] = null;
			destination.Execute("ABOR");
			source.Execute("ABOR");
		}
	}
}

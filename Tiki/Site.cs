using System;
using System.Collections;

namespace Tiki
{
	/// <summary>
	/// Summary description for Site.
	/// </summary>
	[Serializable]
	public class Site
	{
		private string[] hostnamePortPairs;
		private string username;
		private string password;
		private string name;
		private string idleCommand;
		private int idleInterval;

		private bool statForList;
		private bool usePassiveMode;
		private bool allowsIdle;
		private bool ssl;
		// 1=ftps, 2=auth tls, 3=auth ssl
		private int sslMode;
		private bool sslDirlistings;
		private bool sslTransfers;
		private bool reconnect;
		private bool leech;
		private bool allowAsSource;
		private bool allowAsDestination;
		
		private Hashtable sections;

		public Site(string[] hostnamePortPairs, string username, string password, string name, string idleCommand, int idleInterval, bool statForList, bool usePassiveMode, bool allowsIdle, bool ssl, int sslMode, bool sslDirlistings, bool sslTransfers, bool reconnect, bool leech, bool allowAsSource, bool allowAsDestination)
		{
			sections = new Hashtable();
			
			this.hostnamePortPairs = hostnamePortPairs;
			this.username = username;
			this.password = password;
			this.name = name;
			this.idleCommand = idleCommand;
			this.idleInterval = idleInterval;
			this.statForList = statForList;
			this.usePassiveMode = usePassiveMode;
			this.allowsIdle = allowsIdle;
			this.ssl = ssl;
			this.sslMode = sslMode;
			this.sslDirlistings = sslDirlistings;
			this.sslTransfers = sslTransfers;
			this.reconnect = reconnect;
			this.leech = leech;
			this.allowAsSource = allowAsSource;
			this.allowAsDestination = allowAsDestination;
		}


		public string[] HostnamePortPairs
		{
			get { return hostnamePortPairs; }
			set { hostnamePortPairs = value; }
		}

		public string Username
		{
			get { return username; }
			set { username = value; }
		}

		public string Password
		{
			get { return password; }
			set { password = value; }
		}

		public string Name
		{
			get { return name; }
			set { name = value; }
		}

		public int SslMode
		{
			get { return sslMode; }
			set { sslMode = value; }
		}

		public string IdleCommand
		{
			get { return idleCommand; }
			set { idleCommand = value; }
		}

		public int IdleInterval
		{
			get { return idleInterval; }
			set { idleInterval = value; }
		}

		public bool StatForList
		{
			get { return statForList; }
			set { statForList = value; }
		}

		public bool UsePassiveMode
		{
			get { return usePassiveMode; }
			set { usePassiveMode = value; }
		}

		public bool AllowsIdle
		{
			get { return allowsIdle; }
			set { allowsIdle = value; }
		}

		public bool Ssl
		{
			get { return ssl; }
			set { ssl = value; }
		}

		public bool SslDirlistings
		{
			get { return sslDirlistings; }
			set { sslDirlistings = value; }
		}

		public bool SslTransfers
		{
			get { return sslTransfers; }
			set { sslTransfers = value; }
		}

		public bool Reconnect
		{
			get { return reconnect; }
			set { reconnect = value; }
		}

		public bool Leech
		{
			get { return leech; }
			set { leech = value; }
		}

		public bool AllowAsSource
		{
			get { return allowAsSource; }
			set { allowAsSource = value; }
		}

		public bool AllowAsDestination
		{
			get { return allowAsDestination; }
			set { allowAsDestination = value; }
		}

		public Hashtable Sections
		{
			get { return sections; }
			set { sections = value; }
		}

		public double GetWeight()
		{
			// (1 / sites.Items.Count) * sites.Items.IndexOf(s);
			// if it's no in the list, return (1 / nun_sites_in_list) / 2 or something.
			// and add it to the bottom of the list
			int i = 0;
			for (i = 0; i < Config.SiteRanking.Count; i++)
			{
				string site = (string) Config.SiteRanking[i];
				if (site.Equals(name))
				{
					Logger.Log("found site " + name + " in site ranking at place " + (i+1) + " out of " + Config.SiteRanking.Count);
					break;
				}
			}
			Logger.Log("i=" + i + " ranking for site: " + name + " is: " + (1.0d - ((1.0d / Config.SiteRanking.Count) * (i))));
			return 1.0d - ((1.0d / Config.SiteRanking.Count) * (i));
		}
		public override string ToString()
		{
			return name;
		}
	}
}

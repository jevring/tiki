using System;

namespace Tiki
{
	/// <summary>
	/// Summary description for FxpResult.
	/// </summary>
	public class FxpResult
	{
		private string reponse;
		private int rc;
		private long startTime;
		private long endTime;
		private int fileSize;

		public FxpResult(string reponse, int rc, long startTime, long endTime, int fileSize)
		{
			// add stuff like speed and such in here too?

			// rc = 0 if we fail, and explanation in "reponse" (such as "File Already Exists" and stuff like that
			this.reponse = reponse;
			this.rc = rc;
			this.startTime = startTime;
			this.endTime = endTime;
			this.fileSize = fileSize;
		}

		public string Reponse
		{
			get { return reponse; }
		}

		public int Rc
		{
			get { return rc; }
		}

		public long StartTime
		{
			get { return startTime; }
		}

		public long EndTime
		{
			get { return endTime; }
		}

		public int FileSize
		{
			get { return fileSize; }
		}
	}
}

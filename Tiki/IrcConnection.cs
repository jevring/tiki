using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
// using Org.Mentalis.Security.Ssl;

namespace Tiki
{
	/// <summary>
	/// Summary description for Irc.
	/// </summary>
	public class IrcConnection
	{
		// our nick
		private string nickname;
		// this is what we want to be called
		private string realname;
		// the host we're connecting from (in mIRC, this is the email we specify, for some reason)
		private string hostname;
		// this is the server we're connecting to, the user doesn't enter this, it comes from settings
		private string servername;
		// the password, if any
		private string password;
		// irc username
		private string username;

		private int port;
		private bool ssl;

		private StreamReader reader;
		private StreamWriter writer;
		private TcpClient client;
//		private SecureTcpClient secureClient;
		private bool connected = false;



		public IrcConnection(IrcConnectionSettings ircs)
		{
			this.nickname = ircs.Nickname;
			this.username = ircs.Username;
			this.hostname = "localhost";
			this.realname = ircs.Nickname;
			this.password = ircs.Password;
			this.servername = ircs.Hostname;
			this.port = ircs.Port;
			this.ssl = ircs.Ssl;

		}

		public bool Connect()
		{
			// todo: loop until we are connected...
			Console.Out.WriteLine(System.DateTime.Now + " " + "Connecting to irc.");
			if (ssl)
			{
/*				try {
					secureClient = new SecureTcpClient(servername, port, new SecurityOptions(SecureProtocol.Ssl3));
					reader = new StreamReader(new BufferedStream(secureClient.GetStream()));
					writer = new StreamWriter(new BufferedStream(secureClient.GetStream()));
					connected = Login();
				} 
				catch (Exception e)
				{
					Logger.Log("SSL: ");
					Logger.Log(e.Message);
					// return here or something.
					Logger.Log(e.StackTrace);
				}
*/
			}
			else
			{
				try 
				{
					client = new TcpClient(servername, port);
					reader = new StreamReader(client.GetStream());
					writer = new StreamWriter(client.GetStream());
					connected = Login();
					Logger.Log("Connected to irc...");
				} 
				catch
				{
					Logger.Log("Connection to irc failed, retrying.");
					Thread.Sleep(5000);
					Connect();
				}
			}
			return connected;

		}


		public void Disconnect()
		{
			connected = false;
			if (ssl)
			{
//				secureClient.Close();
			}
			else
			{
				client.Close();
			}
		}
		/// <summary>
		/// This send a properly terminated line to the server.
		/// </summary>
		/// <param name="line">the message.</param>
		public void WriteLine(string line)
		{
			writer.Write(line + "\n\n");
			writer.Flush();
			
		}

		public bool Login()
		{
			string line;
			WriteLine("NICK " + nickname);
			line = reader.ReadLine();
			WriteLine("USER " + username + " " + hostname + " " + servername + " :" + realname);
			Response r = new Response(reader.ReadLine());
			if (password.Length > 0)
			{
				WriteLine("PASS " + password);
				line = reader.ReadLine();
				// todo: check if the nick is already taken here (there should be no reason why it should be, we're connecting to a bnc)
				// we'll also be disconnected, so it's probably alot easier to check for that
				// todo: later, catch this disconnect instead.
				if (line.IndexOf(":Wrong Password") > -1)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			return true;
		}

		public string Nickname
		{
			get { return nickname; }
		}

		public StreamReader Reader
		{
			get { return reader; }
		}
	}
}

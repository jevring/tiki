using System;
using System.Threading;

namespace Tiki
{
	/// <summary>
	/// Summary description for SiteConnection.
	/// </summary>
	public class SiteConnection
	{
		private Site site;
		private FtpConnection source;
		private FtpConnection destination;

		public SiteConnection(Site site, FtpConnection source, FtpConnection destination)
		{
			this.site = site;
			this.source = source;
			this.destination = destination;
		}

		public Site Site
		{
			get { return site; }
		}

		public FtpConnection Source
		{
			get { return source; }
		}

		public FtpConnection Destination
		{
			get { return destination; }
		}

		public override string ToString()
		{
			return site.Name;
		}

	}
}

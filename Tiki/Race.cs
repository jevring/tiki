using System;
using System.Collections;
using System.Threading;

namespace Tiki
{
	/// <summary>
	/// Summary description for Race.
	/// </summary>
	public class Race
	{
		private ServerEngine engine;
		private RaceAnnounce announce;
		private ArrayList wantedConnections;
		private Hashtable connections;
		private Hashtable sources;
		private Hashtable destinations;
		private Hashtable transfers;
		
		private ArrayList sfv;
		private Hashtable progress;
		private string state;
		private bool complete = false;
		private DateTime raceStart;

		private bool terminate = false;
		private string prePauseState = "";
		private bool dizRace = false;
		private bool firstDizRacePassDone = false;

		// only used if we register it via GetSitesFromParent() 8should really be somethign like register parent race)
		private Race parentRace = null;
		private Hashtable subRaces = null;
		private Hashtable completeSubRaces = null;
		private ArrayList sourcesToBeRemoved = null;

		// sources that have the release dir, but doesn't have the sfv
		private Hashtable emptySources;

		private bool inUse = false;
		private bool pause = false;
		private bool started = false;

		private object checkoutLock = new object();
		private Thread executingThread = null;

		public Race(ServerEngine engine, RaceAnnounce announce)
		{
			raceStart = System.DateTime.Now;
			this.engine = engine;
			this.announce = announce;
			state = "Pending";
			wantedConnections = new ArrayList();
			// this is virtually only used for checking the connections back in again
			connections = new Hashtable();
			sources = new Hashtable();
			destinations = new Hashtable();
			transfers = Hashtable.Synchronized(new Hashtable());
			progress = new Hashtable();
			emptySources = new Hashtable();
			sfv = new ArrayList();
			sourcesToBeRemoved = new ArrayList();

			subRaces = new Hashtable();
			completeSubRaces = new Hashtable();


			wantedConnections.Add(announce.Source);
			wantedConnections.AddRange(announce.Destinations);

			if (announce.Section.Equals("0day") || announce.Section.Equals("dox") || announce.Section.Equals("bookware"))
			{
				dizRace = true;
			}
		}

		public bool RemoveSite(Site site)
		{
			lock (checkoutLock) 
			{
				if (wantedConnections.Contains(site))
				{
					wantedConnections.Remove(site);
				}
				if (connections.Contains(site.Name))
				{
					connections.Remove(site.Name);
				}
				if (sources.Contains(site.Name))
				{
					sources.Remove(site.Name);
				}
				if (destinations.Contains(site.Name))
				{
					destinations.Remove(site.Name);
				}
				if (connections.Count > 2)
				{
					// no need to continue if we don't have enough sites to race
					Terminate(false);
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		public void CheckOutWantedSites()
		{
			ArrayList connectionsToBeRemoved = new ArrayList();
			inUse = true;
			string tstate = state;
			state = "Checking out sites";
			UpdateStatus();
			lock (checkoutLock) 
			{
				// NOTE: we can NOT remove anything from teh wantedConnections list, since we return the connections when we check it back in aas a pending race!
				if (parentRace == null) 
				{
					for (int i = 0; i < wantedConnections.Count; i++)
					{
						Site wantedConnection = (Site) wantedConnections[i];
						Logger.Log("checking out site " + (i+1) + " (" + wantedConnection.Name + ") of " + wantedConnections.Count);
						// this way, we don't need to worry about trying to trade on connections that are never going to come back (during this session anyway)
						if (!ConnectionPool.IsDead(wantedConnection.Name)) 
						{
							SiteConnection sc = ConnectionPool.CheckoutConnection(wantedConnection.Name, this);
							if (sc != null)
							{
								FtpConnection reconnect = null;
								// we checked it out.
								connections[wantedConnection.Name] = sc;
								string path = ((Section)sc.Site.Sections[announce.Section]).Path;
								if (sc.Site.AllowAsSource) 
								{
									sc.Source.Racing = true;
									sources[sc.Site.Name] = sc.Source;
									// just to both make it look more real, and appease a possible cubnc that might have timed out
									bool b = sc.Source.Cwd(path);
									if (!b)
									{
										if (sc.Source.LastCommandResponse.Response.IndexOf("530 Sorry, your account is restricted to") > -1)
										{
											// this is cubnc compatibility mode
											reconnect = sc.Source;
										}
										Logger.Log("CWD failed, the section doesn't exist, or it's been poorly configured (should we remove the site here?)");
										Logger.Log("removing source: " + sc.Site.Name);
										sources.Remove(sc.Site.Name);
										sc.Source.Racing = false;
									}
									// also, we'll know it' it's misconfigured from the start
								}
								if (sc.Site.AllowAsDestination) 
								{
									sc.Destination.Racing = true;
									destinations[sc.Site.Name] = sc.Destination;
									// just to both make it look more real, and appease a possible cubnc that might have timed out
									bool b = sc.Destination.Cwd(path);
									if (!b)
									{
										if (sc.Destination.LastCommandResponse.Response.IndexOf("530 Sorry, your account is restricted to") > -1)
										{
											// this is cubnc compatibility mode
											reconnect = sc.Destination;
										}
										Logger.Log("CWD failed, the section doesn't exist, or it's been poorly configured (should we remove the site here?)");
										Logger.Log("removing destination: " + sc.Site.Name);
										destinations.Remove(sc.Site.Name);
										sc.Destination.Racing = false;
									}
									// also, we'll know it' it's misconfigured from the start
								}
								if (reconnect != null)
								{
									Logger.Log("forcing reconnect on cubnc-site: " + reconnect.Site.Name);
									ConnectionPool.ForceReconnect(reconnect.Site.Name);
									// this is important, it tells the loop to test this connection again, and cwd to the section dir.
									i--;
									if (i < 0)
									{
										i = 0;
									}
									// should say "checking out site n" for whatever n was the last when we lloop around
								}
								// we've checked it out properly, so we don't want it anymore, we have it.
								// no, don't do this
							}
							else
							{
								Logger.Log("we couldn't check out site: " + wantedConnection.Name);
								if (ConnectionPool.IsDead(wantedConnection.Name))
								{
									//wantedConnections.Remove(wantedConnection);
									connectionsToBeRemoved.Add(wantedConnection);

									// we can't do any i-- action here, because if it's the first site that's dead, we then wan't to look at index -1, and that messes things up
								}
							}
						}
						else
						{
							// the conneciton is dead.
							//wantedConnections.Remove(wantedConnection);
							connectionsToBeRemoved.Add(wantedConnection);
						}
					}
				}
				else
				{
					// and this iw why we can't remove sites from wanted conenctions if we have checked them out.
					// or can't we? why do we even keep that set around after it's been gone over and we have our connections?

					// this is a sub race
					wantedConnections = (ArrayList)parentRace.WantedConnections.Clone();
					// since we want our own copies to modify, we're using clone.
					sources = (Hashtable)parentRace.Sources.Clone();
					destinations = (Hashtable)parentRace.Destinations.Clone();
				}
			}
			for (int i = 0; i < connectionsToBeRemoved.Count; i++)
			{
				// this is here to support removing sites that we want, but that turn up dead or otherwise unusable
				Site toBeRemoved = (Site) connectionsToBeRemoved[i];
				wantedConnections.Remove(toBeRemoved);
			}
			state = tstate;
			inUse = false;
			UpdateStatus();
		}

		public void ReturnConnections()
		{
			inUse = true;
			string tstate = state;
			state = "Returning connections";
			// we'll just have to make sure this isn't called when we're actually trading.

			// return the connections
			foreach (DictionaryEntry site in connections)
			{
				FtpConnection s = ((SiteConnection)site.Value).Source;
				s.Reset();

				FtpConnection d = ((SiteConnection)site.Value).Destination;
				d.Reset();

				string sitename = (string)site.Key;
				ConnectionPool.ReturnConnection(sitename);
			}
			connections.Clear();

			// clear sources
			sources.Clear();

			// clear destinations
			destinations.Clear();

			// DON'T clear wanted connections
			state = tstate;
			inUse = false;
		}

		public void Trade()
		{
			inUse = true;
			started = true;
			
			state = "Running";
			UpdateStatus();
			while (destinations.Count > 0) 
			{
				// if we have a race with multiple dirs, there has GOT to be 2 or more of them, otherwise there would be no need for subdirs
				if (sfv.Count == 0 && subRaces.Count > 1 && completeSubRaces.Count == subRaces.Count)
				{

					// TODO: maybe we should add a number of retries here, so we get all the subdirs for sure

					// we only have subraces if we don't have an sfv...
					Logger.Log("all the subraces for this race is over, terminating. subRaces.Count was: " + subRaces.Count);
					EndRace();
					return;
				}
				for (int i = 0; i < sourcesToBeRemoved.Count; i++)
				{
					string toBeRemoved = (string) sourcesToBeRemoved[i];
					if (sources.Contains(toBeRemoved))
					{
						Logger.Log("Removing " + toBeRemoved + " from sources");
						sources.Remove(toBeRemoved);
						sourcesToBeRemoved.RemoveAt(i);
						if (i != 0) 
						{
							i--;
						}
					}
				}

				if (sources.Count == 0)
				{
					// check if this causes any trouble?
					// this'll also be used as an exit point latern when we check if all the sources have transfered their files max 2 times and such (later revision)
					Logger.Log("We ran out of sources with " + destinations.Count + " destinations left to go. " + NumberOfCompleteSites + " complete sites of " + NumberOfSites + " participating");
					EndRace();
					return;
				}
				foreach (FtpConnection source in sources.Values)
				{
					Logger.Log("originating site: " + announce.Source.Name);
					string sourcesLeft = "";
					foreach (string s in sources.Keys)
					{
						sourcesLeft += s + " ";
					}
					
					Logger.Log("sources left: " + sourcesLeft);
					string destinationsLeft = "";
					foreach (string s in destinations.Keys)
					{
						destinationsLeft += s + " ";
					}
					Logger.Log("destinations left: " + destinationsLeft);


					Logger.Log("current source: " + source.Site.Name + Environment.NewLine);
					
					if (!source.Connected && !source.Site.AllowAsSource)
					{
						// maybe we should check to see if AllowAsSource has changed, and if so, connect that thread aswell
						Logger.Log("not allowed as source, skipping to next source.");
						Logger.Log("WE SHOULD NEVER MAKE OUR WAY IN HERE! (since we check this condition when we create the race)");
						continue;
					}
					if (terminate)
					{
						// this is a force terminate (maybe we'll add a nicer one later)
						Logger.Log("Race terminated in loop");
						EndRace();
						return;
					}
					if (destinations.Count == 0)
					{
						// NOTE: this should be the only place where we end the race
						// also outside the loop
						Logger.Log("ending race at position 'check if I'm needed 1'");
						EndRace();
						return;
					}
					if (source.Transferring)
					{
						Logger.Log("source was transferring skipping to next");
						Thread.Sleep(2000);
						continue;
					}
					if (ConnectionPool.IsBusy(source.Site.Name) || ConnectionPool.IsDead(source.Site.Name))
					{
						// source is busy, we're not let in, or it's something other that ahs to do with a 530 report, then remove it from this race, atleast for now
						// maybe we'll handle this differently in the future
						// note that this might cause incomplete races if you are the only one racing. maybe this needs to be looked at later.
						// right now it's mostly here for the ghosts that cubnc creates, for some reason
						// TODO: make this work better

						Logger.Log("source was busy or dead, removing from set of sources and moving along");
						sourcesToBeRemoved.Add(source.Site.Name);
						continue;
					}
					// ADDED: ofcourse we should reconnect if we're in the middle of a race! 2005-04-03 18:25
					if (!source.Connected)
					{
						// do we ever end up here?
						Logger.Log("source wasn't connected, reconnecting...");
						source.Connect();
						Logger.Log("done reconnecting");
					}
					// CWD on source (if it fails, it doesn't have any files yet, move on (continue;)

					// todo: if we can CWD to the dir, but can't manage to get an sfv, go on to the next host, so we don't race empty dirs!
					# region PrepSource
					string sourcePath = ((Section)source.Site.Sections[announce.Section]).Path + "/" + announce.ReleaseName;
					// CWD and list the source, so we know that we're in the right dir, and so we know what files this source has
					if (!source.Pwd.Equals(sourcePath)) 
					{
						bool cwdSuccess = source.Cwd(sourcePath);
						// todo: also check with a PWD to make sure we're not in "/" (just to be safe)
						if (!cwdSuccess)
						{
							// if we can't CWD to a dir, and we don't exist as a destination (something is VERY wrong) and we should remove this source from the list of sourcess
							if (!destinations.Contains(source.Site.Name))
							{
								Logger.Log("Source could not CWD to race dir, and is no longer a destination, something is wrong, ending race");
								EndRace();
								return;
							}
							if (sources.Count == 1)
							{
								// we only have one source, and it doesn't have the dir, then terminate.
								Logger.Log("we only have one source, and it doesn't have the dir, quitting...");
								EndRace();
								return;
							}
							Logger.Log("the source " + source.Site.Name + " did not have the releasedir yet, trying the next one.");
							// this only means that this source doesn't have the dir yet. skip to the next source
							continue;
						}
					}
					// mostly applicable when we have only one contender in the race, and it's complete or disallowed (or pred) on all the other sites.
					// actually, if we only have one source/destination (incomplete) and no other usable destinations, the race should be ended
					if (sources.Count == 1 && destinations.Count == 1)
					{
						// if they are the same (i.e. we have no more OTHER sources or destinations), quit.
						foreach (string s in sources.Keys)
						{
							foreach (string d in destinations.Keys)
							{
								if (s.Equals(d))
								{
									// this'll compare the SINGLE entry in each list. (since there is only one)
									Logger.Log("Ending race in the 'only one source and one destination, and they are the same' place");
									EndRace();
									return;
								}
							}
						}
					}

					// for 0day, dox and ebook, we need to start transferring before we check the number of files, since most things are just 1 file.
					if (!dizRace || firstDizRacePassDone) 
					{
						// do this instead of a plain listing, otherwise, if we have no destinations, we'll never end this race.
						CheckDestinationCompleteness(source); // includes list
						if (sfv.Count == 0)
						{
							if (source.DirList.Count == 0)
							{
								Logger.Log("first source, download sfv");
								sfv = source.DownloadSfvForCurrentDir();
								if (source.FileList.Count == 0)
								{
									// we found no sfv (or diz), and no subdirs. wait a while, and then loop again, in hope of finding some dirs to race in
									Logger.Log("We couldn't find any subdirs or an sfv-file, sleeping and trying next source.");
									Thread.Sleep(3000);
									// next source
									continue;

								}
							}
							else
							{
								foreach (string dir in source.DirList)
								{
									// todo: make damn sure we dont' repeat ehs ame subrace multiple times, but also make sure we don't forget things like cd3 and so forth
									if (!completeSubRaces.Contains(announce.ReleaseName + "/" + dir) && !subRaces.Contains(dir)) 
									{
										string tstate = state;
										state = "Running subraces";
										Logger.Log("Creating subrace for: " + dir);
										Race subRace = engine.CreateAndRunNewSubRace(this, dir); 
										subRaces.Add(dir, subRace); // <- this is mostly for keeping track of the number of races
										// this block until the race is done
										state = tstate;
									}
									else
									{
										Logger.Log("we already had a completed subrace called: " + dir);
									}
								}
								break;
							}
						}
					}
					# endregion

					#region destination selection and transfer
					Hashtable exemptDestinations = new Hashtable();
					exemptDestinations[source.Site.Name] = null;
					bool foundMatchedPair = false;
					while (!foundMatchedPair)
					{
						FtpConnection destination = GetFastestDestination(source.Site, exemptDestinations);
						
						if (destination == null)
						{
							// there is no available destination for this source. they are all either busy or more complete than the source
							Logger.Log("we had no destinations for this source (" + source.Site.Name + "), waiting...");
							Thread.Sleep(1000);
							break;
						}
						else
						{
							if (ConnectionPool.IsBusy(destination.Site.Name))
							{
								// source is busy, we're not let in, or it's something other that ahs to do with a 530 report, then remove it from this race, atleast for now
								// maybe we'll handle this differently in the future
								// note that this might cause incomplete races if you are the only one racing. maybe this needs to be looked at later.
								// right now it's mostly here for the ghosts that cubnc creates, for some reason
								// TODO: make this work better

								Logger.Log("destination was busy, removing from set of destinations and movng along");
								destinations.Remove(destination.Site.Name);
								continue;
							}

							if (!destination.Connected)
							{
								// since we can't get dead sites from the cache, we can only assume that it's in the process of (re)connecting still
								// just skip this one, and try the next
								Logger.Log("destination is not connected, calling continue");
								exemptDestinations[destination.Site.Name] = null;
								continue;
							}	
						}
						string destinationPath = ((Section)destination.Site.Sections[announce.Section]).Path + "/" + announce.ReleaseName;
						Logger.Log("destination path: " + destinationPath);

						// since we dont' want to race with the wrong dirname, check all possible spellings
						bool destinationHasReleasedir = false;
						if (destination.Pwd.Equals(destinationPath))
						{
							// if we're already in the dir, it obviously has it.
							destinationHasReleasedir = true;
						}
						else
						{
							if (parentRace != null)
							{
								// if we're in a child race, there might be casing difficulties on MKD, so try to cwd to it first
								destinationHasReleasedir = destination.Cwd(destinationPath);
							}
							if (!destinationHasReleasedir) 
							{
								if (parentRace != null) 
								{
									// we must create the parent dir for the subraces first
									destination.Mkd(destinationPath.Substring(0, destinationPath.LastIndexOf("/") + 1));
								}
								destination.Mkd(destinationPath);
							}
							destinationHasReleasedir = destination.Cwd(destinationPath);	
						}
						
						Logger.Log("a1");
						if (destinationHasReleasedir)
						{
							Logger.Log("a3");
							Queue queue = CreateQueue(source, destination);
							Logger.Log("a4");
							if (queue.Count != 0)
							{
								Logger.Log("a5");
								TransferQueue(queue, source, destination);
								Logger.Log("a6");
								foundMatchedPair = true;
							}
							else
							{
								Logger.Log("a7");
								// so this is skipped next time we look for a destination
								exemptDestinations[destination.Site.Name] = null;
							}
							Logger.Log("a8");
						}
						else
						{
							Logger.Log("a9 removing destination " + destination.Site.Name + " because it did not have the release dir");
							// we weren't allowed in, remove this destination and try again
							destinations.Remove(destination.Site.Name);
							// if we're not allowed in as a destination, we won't be allowed in as a source.
							sourcesToBeRemoved.Add(destination.Site.Name);
						}
						Logger.Log("a10");
					}
					Logger.Log("a11");
					#endregion
				}
				// this might behave better then there are more than 2 sies involved
				// when there's not, the complementing pair keeps listing all the time!
				Logger.Log("going over the sources one more time (and sleeping for 10 seconds, maybe this should be less later)");
				if (dizRace)
				{
					firstDizRacePassDone = true;
					Thread.Sleep(2000);
				}
				else
				{
					Thread.Sleep(5000);	
				}
				
			}
			Logger.Log("outside while-loop in race");
			EndRace();
			inUse = false;
		}

		public void RegisterParentRace(Race race)
		{
			// this is now a subrace
			parentRace = race;
		}


		private void TransferQueue(Queue queue, FtpConnection source, FtpConnection destination)
		{
			if (pause)
			{
				Logger.Log("pausing...");
				while (pause)
				{
					Logger.Log("Race paused in TransferQueue(). sleeping 4 seconds");
					Thread.Sleep(4000);
				}
			}
			FxpTransfer transfer = new FxpTransfer(queue, source, destination, this);
			lock (transfers.SyncRoot) 
			{
				transfers[source.Site.Name + "->" + destination.Site.Name] = transfer;
			}
			Thread t = new Thread(new ThreadStart(transfer.Start));
			t.Name = "FxpTransfer:" + announce.ReleaseName + ":" + source.Site.Name + "->" + destination.Site.Name;
			t.IsBackground = true;
			t.Start();
			transfer.ExecutingThread = t;

			// todo: IMPORTANT: count the times a source has been used, then remove it from the sources-list
			// when it has sent its files the specified number of times.
			// when this is done, the sources-list will have .Count == 0, and then we stop.
		}


		public bool CheckDestinationCompleteness(FtpConnection destination)
		{
			// this pausing stuff should really be done by monitors 
			Logger.Log("checking destination completeness");
			if (pause)
			{
				Logger.Log("pausing...");
				while (pause)
				{
					Logger.Log("Race paused in CheckDestinationCompleteness(). sleeping 4 seconds");
					Thread.Sleep(4000);
				}
			}
			// checks if a destinations is complete or not.
			// if it is, it's removed from the list of destinations
			// this is called from the fxp transfer.
			if (destinations.Contains(destination.Site.Name)) 
			{
				// only list it if it's still available as a destination
				// if it's not, then that means that it's already complete (just to save some time)
				destination.List();
			}
			if (sfv.Count == 0)
			{
				// since we don't deal with diz-races quite yet, a destination/source can't be complete if there are no files in the sfv.
				return false;
			}
			bool complete = true;
			int filesFromSfvPresent = 0;
			for (int i = 0; i < sfv.Count; i++)
			{
				string s = (string) sfv[i];
				if (!destination.FileList.Contains(s))
				{
					// we nly need one to proclaim it's not complete
					complete = false;
				}
				else
				{
					filesFromSfvPresent++;
				}
			}	
			
			if (complete)
			{
				Logger.Log("found complete destination, removing: " + destination.Site.Name);
				// this won't work if we're called with the source conneciton, since they aren't identical.
				// this mush be hash tables
				destinations.Remove(destination.Site.Name);
				progress[destination.Site.Name] = sfv.Count;
			}
			else
			{
				progress[destination.Site.Name] = filesFromSfvPresent;
			}
			UpdateStatus();
			return complete;
		}

		public void Pause()
		{
			Logger.Log("Pausing race..");
			prePauseState = state;
			state = "Paused";
			pause = true;
			UpdateStatus();
		}
		public void Continue()
		{
			Logger.Log("Continuing race...");
			pause = false;
			state = prePauseState;
			UpdateStatus();
		}
		private Queue CreateQueue(FtpConnection source, FtpConnection destination)
		{
			ArrayList queue = new ArrayList();
			Logger.Log("creating queue");
			CheckDestinationCompleteness(source); // <- this lists the source aswell, and keeps us from having to do it again later to check if it's complete or not
			// we need to do this here, because if we never get a transfer going to this host, it'll never otherwise be checked.
			// it also does a list, so now worries
			bool complete = CheckDestinationCompleteness(destination);
			if (complete)
			{
				return new Queue();
			}
			for (int i = 0; i < source.FileList.Count; i++)
			{
				string file = (string) source.FileList[i];
				// todo: introduce some kind of skiplist here for imdb.msg and such
				if (!destination.FileList.Contains(file))
				{
					Logger.Log("Enqueing: " + file);
					if (file.EndsWith(".sfv"))
					{
						queue.Insert(0, file);
					}
					else
					{
						queue.Add(file);
					}
				}
			}
			
			return new Queue(queue);;
		}

		private FtpConnection GetFastestDestination(Site source, Hashtable exemptDestinations)
		{
			// don't do the 1 file test queue thing here, just run the gauntlet until the speed matrix is filled

			FtpConnection fastestDestination = null;
			double highestWeight = -1.0d;
			foreach (FtpConnection destination in destinations.Values)
			{
				if (destination.Transferring || exemptDestinations.Contains(destination.Site.Name))
				{
					continue;
				}
				double matrixSpeed = Config.SpeedMatrix.GetSpeed(source, destination.Site);
				Site site = destination.Site;
				Section section = (Section)site.Sections[announce.Section];
				// todo: this needs to be modified with the individual 3rd-part internal weighting
				// todo: rewrite the site ranking, then bring this back
				
				double sectionWeight = 0.0d;
				object swo = Config.GeneralSettings["section_weight"];
				if (swo != null)
				{
					sectionWeight = (double)swo;
				}
				double sitePriorityWeight = 0.0d;
				object spwo = Config.GeneralSettings["site_priority_weight"];
				if (spwo != null)
				{
					sitePriorityWeight = (double)spwo;
				}
				double siteSpeedWeight = 0.0d;
				object sswo = Config.GeneralSettings["site_speed_weight"];
				if (sswo != null)
				{
					siteSpeedWeight = (double)sswo;
				}
				double weight = (siteSpeedWeight * matrixSpeed) + (sitePriorityWeight * site.GetWeight()); // + (sectionWeight + section.GetWeight());
				Logger.Log("weight=" + weight + "; highestWeigh=" + highestWeight);
				if (weight > highestWeight || matrixSpeed == 0.0d)
				{
					Logger.Log("found a new 'best suited' destination: " + destination.Site.Name);
					// if we find something better, update that
					// or if we find soemthing that has 0 weight (not in the matrix usually)
					highestWeight = weight;
					fastestDestination = destination;
				}
			}
			// if this returns null, it means that we couldn't find ANY destinations as all.
			return fastestDestination;

		}

		private void EndRace()
		{
			// todo: this needs to be handled alot better, especially if we have a race with few files but big ones.
			Logger.Log("ending race " + ReleaseName);
			state = "Ending, waiting for transfers to complete.";
			UpdateStatus();
			
			if (terminate)
			{
				// go over the cloned list instead
				lock (transfers.SyncRoot) 
				{
					foreach (FxpTransfer transfer in transfers.Values)
					{
						if (transfer != null) 
						{
							transfer.End(true);
						}
					}
				}
			}
			else
			{
				// todo: this should wait for the transfers to finish before terminating anything
				while (transfers.Count > 0)
				{
					// todo: if this takes too long, we can assume that one or more of the transfer participants have hanged, so we should just kill the connections
					// we issue the lock here instead of outside the if, so as to keep the gui from locking when we're waiting for a release to finish
					lock (transfers.SyncRoot) 
					{
						Logger.Log("Waiting for the following transfers for this race to complete");
						foreach (DictionaryEntry transfer in this.transfers)
						{
							// if we've ran into trouble with the fucking transfer enumeration at some point, we might need to do this check here:
							string filename = ((FxpTransfer)transfer.Value).Filename;
							if ("".Equals(filename))
							{
								((FxpTransfer)transfer.Value).End(true);
							}

							// todo: visa hur stor filen �r, och hur l�ng tid vi tror att �verf�ringen har kvar.
							Logger.Log(transfer.Key + " : " + ((FxpTransfer)transfer.Value).Filename + " files left in queue: " + ((FxpTransfer)transfer.Value).Queue.Count + Environment.NewLine);
							string speed = (((FxpTransfer)transfer.Value).SpeedOfLastTransfer / 1024.0).ToString();
							string size = (((FxpTransfer)transfer.Value).CurrentFileSize / 1024.0).ToString();
							Logger.Log("Last file transfered at " + speed + " kB/s. Current file is " + size + " kilobytes" + Environment.NewLine);
							double transferTime = Convert.ToDouble(((FxpTransfer)transfer.Value).CurrentFileSize / ((FxpTransfer)transfer.Value).SpeedOfLastTransfer);
							if (transferTime > 0 && !double.IsInfinity(transferTime) && !double.IsNaN(transferTime))
							{
								Logger.Log("File should have ended " + System.DateTime.Now.AddSeconds(transferTime).TimeOfDay + " and time is now " + System.DateTime.Now.TimeOfDay);
							}
							else
							{
								Logger.Log("transfer SHOULD be COMPLETE already");
							}
							// .Substring(0, speed.IndexOf(',') + 2)
						}
					}
					Thread.Sleep(10000);
					// this should sleep like the expected time of the current file or the queue or something.
				}	
			}
			

			// NOTE: this must wait for all transfers to end
			// no, we can't reach destinations.Count == 0 if we still have threads transferring
			// apparently, we can.
			if (parentRace == null)
			{
				// only return the connections if this isn't a child race
				ReturnConnections();
			}
			else
			{
				parentRace.SubraceComplete(this);
			}
			
			complete = true;
			UpdateStatus();
			inUse = false;
		}

		public void StopRacingNukedReleaseOnSites(ArrayList sites)
		{
			for (int i = 0; i < sites.Count; i++)
			{
				// todo: make this better and more secure!!!!!
				Site site = (Site) sites[i];
				Logger.Log("removing " + site.Name + " from race");
				sourcesToBeRemoved.Add(site);
				destinations.Remove(site);
				lock (transfers.SyncRoot) 
				{
					foreach (FxpTransfer transfer in this.Transfers.Values)
					{
						if (transfer.Source.Site.Name.Equals(site.Name) || transfer.Destination.Site.Name.Equals(site.Name))
						{
							// use true here, it's a nuke, we want to abort as fast as we can
							transfer.End(true);
						}
					}
				}
			}
		}

		public void EndTransfer(FxpTransfer transfer)
		{
			Logger.Log("calling end transfer in Race.cs");
			CheckDestinationCompleteness(transfer.Destination);
			CheckDestinationCompleteness(transfer.Source);
			// I wonder if we get a dual lock here maybe...
			Logger.Log("trying to remove race from list");
			lock (transfers.SyncRoot) 
			{
				// this creates a race condition, since we're killing things while we're iterating over them.
				transfers.Remove(transfer.Source.Site.Name + "->" + transfer.Destination.Site.Name);
			}
			Logger.Log("done removing race from list");
			UpdateStatus();
		}

		public void SetProgress(string sitename, int i)
		{
			progress[sitename] = i;
			UpdateStatus();
		}
		public void IncreaseProgress(string sitename)
		{
			int i = 1;
			if (progress.Contains(sitename))
			{
				i = Convert.ToInt32(progress[sitename]);
				i++;
			}
			progress[sitename] = i;
			UpdateStatus();
		}
		public int GetProgress(string sitename)
		{
			if (progress.Contains(sitename))
			{
				return Convert.ToInt32(progress[sitename]);
			}	
			else
			{
				return 0;
			}
		}

		public void UpdateStatus()
		{
			engine.UpdateRace(this);
		}

		public void Terminate(bool force)
		{
			// this stops the race dead in it's tracks. this is called if the release is nuked
			// this also kills any possible transfer threads we had.
			state = (force ? "Force " : "") + "Aborting...";
			UpdateStatus();
			terminate = true;
			lock (transfers.SyncRoot) 
			{
				foreach (FxpTransfer transfer in this.transfers.Values)
				{
					if (transfer != null) 
					{
						transfer.End(force);
					}
				}
			}
			if (subRaces.Count > 0)
			{
				foreach (Race race in subRaces.Values)
				{
					race.Terminate(force);
				}
			}
			if (force)
			{
				Logger.Log("Aborting race thread: " + this.ReleaseName);
				// we have to do stuff like this because we can't extend thread =(
				executingThread.Abort();
				Logger.Log("Joining race thread: " + this.ReleaseName);
				executingThread.Join();
				Logger.Log("done aborting race thread: " + this.ReleaseName);
				ReturnConnections();
				// this will clear the race from the lists in the parent object
				complete = true;
				UpdateStatus();
				Logger.Log("Race terminated: " + this.ReleaseName);

			}

		}
		public string ReleaseName
		{
			get { return announce.ReleaseName; }
		}

		public string State
		{
			get { return state; }
		}

		public bool Complete
		{
			get { return complete; }
		}

		public Hashtable Sources
		{
			get { return sources; }
		}

		public Hashtable Destinations
		{
			get { return destinations; }
		}
		public int NumberOfSites
		{
			get { return connections.Count; }
		}
		public int NumberOfCompleteSites
		{
			get
			{
				int r = 0;
				foreach (int i in progress.Values)
				{
					if (i >= sfv.Count)
					{
						r++;
					}
				}
				return r;
			}
		}

		public int NumberOfFilesInSfv
		{
			get { return sfv.Count; }
		}

		public DateTime RaceStart
		{
			get { return raceStart; }
		}

		public Hashtable Connections
		{
			get { return connections; }
		}

		public Hashtable Transfers
		{
			get
			{
				lock (transfers.SyncRoot) 
				{
					return transfers;
					/*// return a clones version here for display, and all the actual operations will be done on the main one, and we'll jsut check for null in the listings
					Hashtable h = (Hashtable)transfers.Clone();
					if (h == null)
					{
						Logger.Log("transfers was null, VERY strange (LOOK FOR ME!)");
						return new Hashtable();
					}
					else 
					{
						return (Hashtable)transfers.Clone();
					}*/
				}
			}
		}

		public RaceAnnounce Announce
		{
			get { return announce; }
		}

		public bool InUse
		{
			get { return inUse; }
		}

		public ArrayList WantedConnections
		{
			get { return wantedConnections; }
		}

		public bool Started
		{
			get { return started; }
		}

		public Thread ExecutingThread
		{
			get { return executingThread; }
			set { executingThread = value; }
		}

		public void SubraceComplete(Race race)
		{
			// continue to use add here, as a check, since we don't want to be running the same subrace multiple times
			completeSubRaces.Add(race.ReleaseName, race);
		}
	}
}

using System;
using System.Text.RegularExpressions;

namespace Tiki
{
	/// <summary>
	/// Summary description for Response.
	/// </summary>
	public class Response
	{
		private string sender = null;
		private string command = null;
		private string destination = null;
		private string message = null;
		private string senderNick = null;
		private DateTime time;

		public Response(string line)
		{
			time = System.DateTime.Now;
			// todo: add cypto capabilities
			string[] data = line.Split(' ');
			// remove ':'
			sender = data[0].Substring(1);
			int bangIndex = sender.IndexOf('!');
			if (bangIndex > -1)
			{
				senderNick = sender.Substring(0, bangIndex);
			}
			command = data[1];

			if (data.Length < 3)
			{
				message = data[2];
			} 
			else if (data[2].IndexOf(':') == 0)
			{
				string[] messageArray = new string[data.Length - 2];
				System.Array.Copy(data, 2, messageArray, 0, data.Length - 2);
				message = string.Join(" ", messageArray);
			}
			else
			{
				destination = data[2];
				string[] messageArray = new string[data.Length - 3];
				System.Array.Copy(data, 3, messageArray, 0, data.Length - 3);
				message = string.Join(" ", messageArray);
			}
			if (message.StartsWith(":+OK") || message.StartsWith(":mcps"))
			{
//				Console.Out.WriteLine(System.DateTime.Now + " " + "decrypted: " + message);
//				Console.Out.WriteLine(System.DateTime.Now + " " + "in channel: " + destination);
				string key = (string)Config.IrcChannelKeys[destination.ToLower()];
//				Console.Out.WriteLine(System.DateTime.Now + " " + "with key: "+key);
				if (key != null) 
				{
					message = DecryptMessage(key, message);
				}
				else
				{
					Logger.Log("found encrypted message but couldn't find key for channel: " + destination.ToLower());
				}
//				Console.Out.WriteLine(System.DateTime.Now + " " + "Into: " + message);
			}
			message = Strip(message);
		}

		private string DecryptMessage(string key, string message)
		{
			// trim the heading.
			string[] s = message.Split(' ');
			string r = CryptoWrapper.DecryptString(key, s[1], null, 0);
			// or possibly string rr = "" + r;
			string rr = (string)r.Clone();
			CryptoWrapper.FreeResultString(r);
			return rr;
		}
		public string Strip(string line)
		{
			// BOLD: ASC(2)
			// COLOR: ASC(3)
			// UNDERLINE: ASC(31)
			// REVERSE: ASC(22)

			// strips out the bold, underline, reverse and such.
			// also strips out "/" in the case of section/releasename. (and relasename/Cd! and such too)
			// todo: should strinp out colors aswell!
			//when re strip the colors, we want the color digits aswell
			//Replace(Convert.ToChar(3), Convert.ToChar(32)).


			// it should be greedy, but it's not.
			Regex colorStripper = new Regex(@Convert.ToChar(3)+"\\d{1,2}", RegexOptions.RightToLeft);
			line = colorStripper.Replace(line, " ");
			// todo: we should remove: !@#��$%&/{[]=}?+\�`^~�*'��<>|
			// maybe it's better with one big regexp then.
			// or rather, a regexp that keeps: ._-()0-9a-z

			// BOLD, underline, reverse, /, [,],!,?
			// strip color markers again, for some reason, people use doubles
			return line.Replace(Convert.ToChar(2), Convert.ToChar(32)).Replace(Convert.ToChar(3), Convert.ToChar(32)).Replace(Convert.ToChar(31), Convert.ToChar(32)).Replace(Convert.ToChar(22), Convert.ToChar(32)).Replace("/", " ").Replace("[", " ").Replace("]", " ").Replace("!", " ").Replace("?", " ");
			
		}

		public string Sender
		{
			get { return sender; }
		}

		public string Command
		{
			get { return command; }
		}

		public string Destination
		{
			get { return destination; }
		}

		public string Message
		{
			get { return message; }
		}

		public string SenderNick
		{
			get { return senderNick; }
		}

		public DateTime Time
		{
			get { return time; }
		}
	}
}

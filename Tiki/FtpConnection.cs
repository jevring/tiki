using System;
using System.Collections;
using System.IO;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Threading;

namespace Tiki
{
	public class FtpConnection
	{
		private Site site;
		private string pwd = "";
		private ArrayList fileList;
		private ArrayList dirList;
		private StreamReader reader;
		private StreamWriter writer;
		private TcpClient controlConnection;

		private Regex listLine;
		private bool transferring = false;
		private bool connected = false;
		private Thread antiIdle;
		private bool connecting;
		private object executeLock = new object();
		private bool source; // false if it's a destination

		// this helps us use a proxy
		private string currentWantedHostname = "";
		private int currentWantedPort = 0;

		// if .Connect() hasn't been called yet, we'll know, since this is < 0
		private int loginResponseCode = -1;
		private string loginErrorMessage = "";
		private int connectionTimeout = 15;
		
		private object logLock = new object();

		private Regex diskFinder;
		private bool terminating = false;
		private bool reconnecting = false;
		private bool racing = false;

		private ArrayList randomAntiIdleCommands;
		private Random random;

		private string currentCommand = "";

		private bool purge = false;
		private FxpTransfer transfer = null;
		private CommandResponse lastCommandResponse = null;

		private object disconnectionLock = new object();

		public FtpConnection(Site site, bool source)
		{
			this.source = source;
			this.site = site;
			listLine = new Regex(@"\s+", RegexOptions.Compiled);
			diskFinder = new Regex(@"\[xx/(\d{2,3})\]|\[\d{2,3}/(\d{2,3})\]", RegexOptions.IgnoreCase | RegexOptions.Compiled);
			fileList = new ArrayList();
			dirList = new ArrayList();
			randomAntiIdleCommands = new ArrayList();
			randomAntiIdleCommands.Add("stat -l");
			randomAntiIdleCommands.Add("CWD .");
			randomAntiIdleCommands.Add("PWD");
			randomAntiIdleCommands.Add("site new 1");
			randomAntiIdleCommands.Add("site stat");
			randomAntiIdleCommands.Add("NOOP");
			randomAntiIdleCommands.Add("RETR 0");
			random = new Random(System.DateTime.Now.Millisecond);
		}
		private void Timeout()
		{
			Log("Starting idletimeout for " + this.site.Name);
			// todo: get this connection timeout from the config
			//Thread.Sleep(connectionTimeout * 1000);
			int i = 0;
			while (i <= connectionTimeout)
			{
				Log((connectionTimeout - i++) + " seconds left until timeout for " + site.Name);
				Thread.Sleep(1000);
			}
			Log("The connection timed out...");
			controlConnection.LingerState = new LingerOption(true, 0);
			controlConnection.Close();
		}

		public void Connect()
		{
			loginResponseCode = 0;
// this is bad, it fucks up our reconnect stuff
//			pwd = "/";
			connecting = true;
			Log("connecting");

			// just do the first one for now, until we can do a proper connection timeout
			for (int i = 0; i < site.HostnamePortPairs.Length; i++)
			{
				//Log("trying hpp nr:" + (i + 1) + " out of " + site.HostnamePortPairs.Length);
				string[] hpp = site.HostnamePortPairs[i].Split(':');
				controlConnection = new TcpClient();
				// USER and PASS gets longer timeouts
				controlConnection.ReceiveTimeout = 15000;
				currentWantedHostname = hpp[0];
				currentWantedPort = Convert.ToInt32(hpp[1]);
				try
				{
					// try to get the connection within 15 seconds (this fucks sd, unfortunately)
					Thread timeout = new Thread(new ThreadStart(this.Timeout));
					timeout.Name = "ConnectionTimeout:" + site.Name + (source ? "(src)" : "(dst)");
					timeout.IsBackground = true;
					timeout.Start();
					if (site.Ssl) 
					{
						if (site.SslMode != 2)
						{
							Logger.Log("We currently only support AUTH TLS, this might change in the future, but we're forcing it here");
						}
						Logger.Log("Connecting via SSL proxy");
						controlConnection.Connect((string)Config.GeneralSettings["proxy_host"], Convert.ToInt32(Config.GeneralSettings["proxy_port"]));
					}
					else
					{
						controlConnection.Connect(currentWantedHostname, currentWantedPort);
					}

					timeout.Abort();

					reader = new StreamReader(controlConnection.GetStream());
					writer = new StreamWriter(controlConnection.GetStream());				
					connected = Login();
					if (connected)
					{
						break;
					}
				}
				catch (Exception e)
				{
					Log("Connection failed because: " + e.Message);
					if (site.Ssl)
					{
						Logger.Log("Make sure your ssl-proxy is running (recommend WinSSLWrap or TLSWrap (not always compatible with cubnc tho))");
					}
					//Log(e.Message);
					//Log(e.StackTrace);
					// try the next hostnamePortPair
				}
			}
			Log("logged in: " + connected);
			if (connected)
			{
				Execute("site xdupe 3");
				CommenceAntiIdle();
			}
			connecting = false;
			ConnectionPool.ReportConnectionState(this);
		}

		public bool Login()
		{
			CommandResponse cr = GetResponse(); // get the 220, make this nicer later
			if (cr.ResponseCode == 220)
			{
				Log("loggin in");
				// we can't send one site in with a "!" prefixed to the username, because if that's a cubnc installation, it'll kill all the future connections to slaves
				if (site.Ssl)
				{
					bool hashPrefix = false;
					try
					{
						hashPrefix = (bool)Config.GeneralSettings["prefix_username_with_hash"];
					}
					catch
					{
						/*Log("Failed to get setting for hashPrefix: " + e.Message);
						Log(e.StackTrace);*/
					}
				
					// this means we're using a proxy
					// at this time, we only support one kind of connection string
					// since there is no need for fxp support when we're purging, we can do this. (! and # never need to appear together, so this reduces the confusion)
					cr = Execute("USER " + (purge ? "!" : (hashPrefix ? "#" : "")) + site.Username + "@" + currentWantedHostname + ":" + currentWantedPort);
				}
				else
				{
					cr = Execute("USER " + (purge ? "!" : "") + site.Username);	
				}

				if (cr.ResponseCode == 331)
				{
					cr = Execute("PASS " + site.Password);

					loginResponseCode = cr.ResponseCode;
					// this latter part is added for glftpd 1.32 compatibility
					if (cr.ResponseCode == 230  && (cr.Response.IndexOf("site is full") > -1))
					{
						loginResponseCode = 530;
					}
					loginErrorMessage = cr.Response;
					//Log(cr.Response);
					return (loginResponseCode == 230 ? true : false);
				}
				else
				{
					Log("we weren't asked for a password");
					//Log(cr.Response);
					return false;
				}

			} 
			else
			{
				Log("login impossible, we got a crummy response");
				Log(cr.Response);
				return false;
			}
		}
		public ArrayList DownloadSfvForCurrentDir()
		{
			if (fileList.Count == 0)
			{
				List();
			}
			int i = 0;
			while (i++ < 10)
			{
				foreach (string s in fileList)
				{
					if (s.IndexOf(".sfv") > -1 || s.IndexOf(".diz") > -1)
					{
						return DownloadAndParseSfv(s);
					}
				}
				// loop again to try to find the sfv 8should be uploaded first, but hey...)
				Thread.Sleep(2000);
				List();
				Logger.Log("trying again to find the sfv or .diz in " + pwd);
			}
			return new ArrayList();
		}

		private ArrayList DownloadAndParseSfv(string sfvFileName)
		{
			CommandResponse cr;
			// check transfer type for downloading first really...
			ArrayList sfv = new ArrayList();
			Execute("TYPE A");
			cr = Execute("PASV");
			// NOTE: this length is a hack, find a better way to do it later
			string portline = cr.Response.Substring(cr.Response.IndexOf("(") + 1, (cr.Response.Length - ((cr.Response.IndexOf("(") + 1) + (cr.Response.Length - cr.Response.LastIndexOf(")"))))  );
			string[] data = portline.Split(',');
			string ip = data[0] + "." + data[1] + "." + data[2] + "." + data[3];
			int highPort = Convert.ToInt32(data[4]) << 8;
			int lowPort = Convert.ToInt32(data[5]);
			int passivePort = highPort + lowPort;
			TcpClient downloader = null;
			try 
			{
				downloader = new TcpClient(ip, passivePort);
			}
			catch (SocketException se)
			{
				// this should virtually NEVER happen unless there is something really really wrong
				Log("We failed to connect to the passive port to download the sfv because: " + se.Message);
				Log(se.StackTrace);
				return sfv;

			}
			
			cr = Execute("RETR " + sfvFileName);
			if (cr.ResponseCode != 150)
			{
				// well something wen't wrong, either a TAGLINES ENFORCED or a not enough credits.
				// ether way, we should skip this source. how we do this is a question for later
			}
			GetResponse(); // make sure we get the 226 aswell!
			
			StreamReader reader = new StreamReader(downloader.GetStream());
			string line;
/*			if (sfvFileName.EndsWith(".diz"))
			{
				line = reader.ReadToEnd();
				Match m = diskFinder.Match(line);
				if (m.Success)
				{
					int numberOfFiles = Convert.ToInt32( m.Groups[1].ToString() );
					// we can't just create an arraylist with the specified capacity, because capacity is not the same as count
					sfv = new ArrayList(numberOfFiles);
					for (int i = 0; i <= numberOfFiles; i++)
					{
						sfv.Add(sfvFileName);
					}
				}
				else
				{
					Log("Could not find the number of files for the release in the following string: " + Environment.NewLine + line);
				}
				// else, how do we signal the fact that we didn't find the number of files in the .diz file
			}
			else
			{*/
				while ((line = reader.ReadLine()) != null)
				{
					if (line != null && line != "" && line[0] != ';')
					{
						// skip all lines containing comments or that are empty.
						string[] sfvData = line.Split(' ');
						sfv.Add(sfvData[0]);
						Log("sfv data found: " + sfvData[0]);
					}
				}	
			//}
			
			// the server might close first, I wonder if that fucks things up here. we'll see I suppose.
			downloader.Close();
			return sfv;
		}

		

		public bool Cwd(string path)
		{
			CommandResponse cr = Execute("CWD " + path);

			if (cr.ResponseCode == 250)
			{
				Log("CWD to " + path + " successful");
				pwd = path;
				return true;
			}
			else
			{
				Log("CWD to " + path + " FAILED because: " + cr.Response);
				return false;
			}
		}
		public bool Mkd(string path)
		{
			CommandResponse cr = Execute("MKD " + path);
			if (cr.ResponseCode == 257)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public void List()
		{
			Log("listing dir: " + pwd);
			CommandResponse cr = Execute("stat -l");
			// HACK: added compatibility for glftpd 2.00 stat -l -password bug
			if (cr.ResponseCode == 213 || cr.Response.StartsWith("total "))
			{
				ParseListData(cr.Response);
			}
			Log("list in dir " + pwd + " found " + fileList.Count + " files");
		}

		private void ParseListData(string response)
		{
			fileList = new ArrayList();
			dirList = new ArrayList();
			string[] data = response.Split(new char[]{'\r', '\n'});
			for (int i = 0; i < data.Length; i++)
			{
				string s = data[i];
				if (s.StartsWith("213") || s.StartsWith("total "))
				{
					continue;
				}
				string[] listData = listLine.Split(s);
				if (listData.Length > 7) 
				{
					Regex subraceFinder = new Regex(@"^(?:cd|dvd|disc)\d+", RegexOptions.IgnoreCase);
					if (listData[0].StartsWith("d")) {
						Match m = subraceFinder.Match(listData[8]);
						if (m.Success) 
						{
							// todo: this must check that cd, dvd and disc is only followed by digits
							Logger.Log("Adding dir for subraces: " + listData[8]);
							dirList.Add(listData[8]);
						}
					}
					else if (!listData[0].StartsWith("d") && listData[4] != "0" && !listData[8].EndsWith("-missing") && !listData[8].StartsWith("."))
					{
						// add skiplist files in this check, like .message and such.
						// skip empty files and dirs
						fileList.Add(listData[8]);	
					}
					
				}
			}
		}
		public FxpResult SendFile(string filename, FtpConnection destination, FxpTransfer transfer)
		{
			this.transfer = transfer;
			// assume that nothing should be uploaded in "/"
			if ("/".Equals(pwd))
			{
				// F A I L (nicely), and find the damn source of the error!
			}
			Log("sending file: " + filename);
			CommandResponse scr;
			CommandResponse dcr;
			// (PROT P)
			
			// SIZE filename.ext (source)
			scr = Execute("SIZE " + filename);
			string[] r = scr.Response.Split(' ');
			// in BYTES!
			int fileSize = 0;
			try 
			{
				if (r.Length > 1 && scr.ResponseCode == 213) 
				{
					fileSize = Convert.ToInt32(r[1]);
				}
				else
				{
					// something else wen't wrong, we probably got an empty response
					DisconnectAndCheckReconnect(true);
					return new FxpResult(scr.Response, -2, 0, 0, 0);
				}
			}
			catch
			{
				// we failed to convert this here, that means we got a funky response from the source, abort.
				Log("We failed to convert filesize to a proper int, it looked like this: " + scr.Response);
				// this happens in cubnc 1.0.1 with cached responses. bug fied in 1.0.2
				// this can also happen if the file dissapears from the source before we can issue the command. that, however is EXTREMELY unlikely.
				// if that is the case, we'll get a 550 no such file or directory error
				if (scr.ResponseCode != 550) 
				{
					// if we get virtually anything else, we might aswell disconnect and reconnect, because there's probably something very wrong
					DisconnectAndCheckReconnect(true);
				}
				// -2 triggers a resend of the file.
				return new FxpResult(scr.Response, -2, 0, 0, 0);
			}
			transfer.CurrentFileSize = fileSize;

			// SIZE filename.ext (destination, f�r att se att filen inte redan finns)
			dcr = destination.Execute("SIZE " + filename);
			if (dcr.ResponseCode != 213)
			{
				// continue
				string type = "I";

				if (filename.EndsWith(".sfv") || filename.EndsWith(".m3u") || filename.EndsWith(".nfo") || filename.EndsWith(".txt") || filename.EndsWith(".diz"))
				{
					type = "A";
				}

				// TYPE I
				scr = Execute("TYPE " + type);
				dcr = destination.Execute("TYPE " + type);

				// PASV 
				// PORT xxxxx
				bool alternateFxp = false;
				try
				{
					alternateFxp = (bool)Config.GeneralSettings["use_alternate_fxp"];
				}
				catch (Exception e)
				{
					Log("Failed to get setting for alternateFxp: " + e.Message);
					Log(e.StackTrace);
				}

				if (alternateFxp) 
				{
					dcr = destination.Execute("PASV");
					string portline = dcr.Response.Substring(dcr.Response.IndexOf("(") + 1, (dcr.Response.Length - ((dcr.Response.IndexOf("(") + 1) + (dcr.Response.Length - dcr.Response.LastIndexOf(")"))))  );
					scr = Execute("PORT " + portline);
				}
				else
				{
					scr = Execute("PASV");
					string portline = scr.Response.Substring(scr.Response.IndexOf("(") + 1, (scr.Response.Length - ((scr.Response.IndexOf("(") + 1) + (scr.Response.Length - scr.Response.LastIndexOf(")"))))  );
					dcr = destination.Execute("PORT " + portline);
					// source does PASV
					// destination does PORT	
				}
				
				// STOR filename.ext
				dcr = destination.Execute("STOR " + filename);
				// todo: check xdupe here, and check any 500 responses we might get for permission problems
				// the xdupe needs to send stuff back to the queue
				if (dcr.ResponseCode != 150)
				{
					if (dcr.ResponseCode == 553 && dcr.Response.IndexOf("X-DUPE") > -1)
					{
						Hashtable xdupeFilenames = new Hashtable();
						string[] xdupes = listLine.Split(dcr.Response);
						for (int i = 0; i < xdupes.Length; i++)
						{
							string xdupe = xdupes[i];
							string[] xdupeContent = xdupe.Split(' ');
							if (xdupeContent.Length > 2 && xdupeContent[1].Equals("X-DUPE:"))
							{
								xdupeFilenames[xdupeContent[2]] = null;
								Log("Found xdupe: " + xdupeContent[2]);
							}
						}
						transfer.ReportXdupe(xdupeFilenames);
					}
					destination.Execute("ABOR");
					return new FxpResult(dcr.Response, 0, 0, 0, fileSize);
				}

				// RETR filename.ext
				scr = Execute("RETR " + filename);
				// todo: check any "insufficient credit" messages that might appear here
				if (scr.ResponseCode != 150)
				{
					destination.Execute("ABOR");
					Execute("ABOR");
					return new FxpResult(scr.Response, 0, 0, 0, fileSize);
				}
				
				// timetaking for speed evaluation
				long startTime = System.DateTime.Now.Ticks;
				// NOTE: changed the order of these tow, it SHOULDN'T make a difference
				// how should we do this if it actually matters who has to return first, and that differs
				// todo: check: should this change with alternate fxp aswell?

				// there's a problem here. and that's that if we send an ABOR, who catches the response?
				// either way, one of the line readers wills till be stuck trying to read.
				scr = GetResponse();
				dcr = destination.GetResponse();
				long endTime = System.DateTime.Now.Ticks;
				// .Ticks returns 100-nanosecond intervals
				long timeTaken = endTime - startTime;
				if (scr.ResponseCode > 399 || dcr.ResponseCode > 399)
				{
					Log("Transfer failed: " + filename);
					return new FxpResult(dcr.Response, -1, 0, 0, 0);
				}
				else 
				{
					Log("speed: " + Convert.ToDouble(Convert.ToDouble(fileSize) / Convert.ToDouble(Convert.ToDouble(timeTaken) / Convert.ToDouble(10000000)) )  + " bytes per second ( I think? )" );
					Log("done sending file: " + filename);
					return new FxpResult(dcr.Response, dcr.ResponseCode, startTime, endTime, fileSize);
				}
			}
			else
			{
				Log("file existed on destination, skipping");
				// the site already has the file
				return new FxpResult(null, 0, 0, 0, 0);
			}
		}

		public CommandResponse Execute(string command)
		{
			currentCommand = command;
			CommandResponse cr = new CommandResponse("", 0);
			lock (executeLock) 
			{
				try
				{
					
					if (!connected && !connecting)
					{
						// only let unconnected commands pass if we're in the act of connecting
						return cr;
					}
					try 
					{
						if (currentCommand.StartsWith("STOR") || currentCommand.StartsWith("RETR"))
						{
							// BUT OFCOURSE, we can't have the connection timeout after 30 seconds if we're sending or receiving
							controlConnection.ReceiveTimeout = 0;
						}
						else if (currentCommand.StartsWith("USER") || currentCommand.StartsWith("PASS") || currentCommand.StartsWith("CWD"))
						{
							// CWD added to support login on cubnc
							controlConnection.ReceiveTimeout = 30000;
						}
						else
						{
							controlConnection.ReceiveTimeout = 15000;
						}
					}
					catch
					{
						// what the fuck, this is pissing me off!
					}
					bool handleDisconnectError = false;
					if (writer != null)
					{
						Log("Sending: " + command);
						try
						{
							writer.Write(command + "\r\n");
							writer.Flush();
							cr = GetResponse();
							if (cr.ResponseCode == -1)
							{
								handleDisconnectError = true;
							}
						} 
						catch (Exception e)
						{
							Log("Problem with the writer 1: " + e.Message);
							handleDisconnectError = true;
						}
					}
					else
					{
						Log("Problem with the writer 2");
						handleDisconnectError = true;
					}
					// ok, we can't just return null, -1 here, since alot of things depend on it, so we'll have to solve it more elegantly, unless it's the user/pass deal.
					if (handleDisconnectError)
					{
						if (currentCommand.StartsWith("USER") || currentCommand.StartsWith("PASS") || currentCommand.StartsWith("ABOR"))
						{
							return new CommandResponse("", -1);
						}
						DisconnectAndCheckReconnect(true);
						// don't try anything if we've already disconnected the source at this point
						if (!connected || ConnectionPool.IsDead(site.Name) || ConnectionPool.IsBusy(site.Name))
						{
							// maybe if it's busy, we should remove it from the race?
							// todo: fix that
							return cr;
						}
						else
						{
							bool b = true;
							if (!"".Equals(pwd)) 
							{
								b = Cwd(pwd);
							}
							if (b) 
							{
								return Execute(command);
							}
							else
							{
								pwd = "/";
								// this almost only happens with cubnc together with ghosts on the wanted slave server
								// but we can't get into the dir, so do something worth while
								return new CommandResponse("", -3);
							}
						}
					}
					else
					{
						return cr;
					}
				}
				catch (ThreadAbortException e)
				{
					// makeing sure we're not still in a lock when we abot a thread.
					/*
					writer.Flush();
					reader.DiscardBufferedData();
					*/
					// those dn't seem to be necessary. it was ony if we were aborting in the middle of a commande being sent to the server, but screw them then
					Monitor.Exit(executeLock);
				}
				return cr;
			}
		}

		public CommandResponse GetResponse() 
		{
			lock (executeLock) 
			{
				try
				{
					// this needs to be here, since we call .GetReponse() by itself when we're sending or receiving a file
					if (currentCommand.StartsWith("STOR") || currentCommand.StartsWith("RETR"))
					{
						// BUT OFCOURSE, we can't have the connection timeout after 30 seconds if we're sending or receiving
						controlConnection.ReceiveTimeout = 0;
					}
					try 
					{
						string line = reader.ReadLine();
					
						if (line != null) 
						{
							string response = "";
							// this first line SHOULD contain the response code.
							int rc = 0;
							try
							{
								rc = Convert.ToInt32(line.Substring(0,3));
							}
							catch
							{
								if (line.StartsWith("total "))
								{
									rc = 213;
								}
								else
								{
									Log("Failed to convert the first 3 digits of " + line + " to a number...");	
								}
							}

							if (line.IndexOf("-") == 3 || rc == 213) 
							{
								// we're found a multiline response
								response += line + "\r\n";
								// do this until we find something like "230 ass", i.e. something that does not have the dash in it.
								// NOTE: this " " (space) is VERY important!
								while (!line.StartsWith(rc + " ")) 
								{
									line = reader.ReadLine();
									response += line + "\r\n";
								}
							} 
							else 
							{
								response = line;
							}
							Log(response);
							// Actually, no, it doesn't. it means the DATA connection was terminated, nothing else. the control connection is still fine.
							/*if (rc == 426)
							{
								// this means that we sent an ABOR, and got a terminated connection in return. we need to reconnect.
								DisconnectAndCheckReconnect(true);
							}*/
							lastCommandResponse = new CommandResponse(response, rc);
							return lastCommandResponse;
						}
						else
						{
							// we also end up here when we've issued an ABOR, and we're force-closing the reader.
							// maybe there is a problem with the reader...
							//Log("line was null, FUCK!");
							lastCommandResponse = new CommandResponse("", 0);
							return lastCommandResponse;
							// check reconnect
						}
					}
					catch (IOException e)
					{
						Log("IOException on " + site.Name + " : " + e.Message);
						Log(e.StackTrace);
						// this should NOT be 'throw e;' : http://www.tkachenko.com/blog/archives/000352.html
						// ok, so this doens't allow us to throw the exception outside the method for some fucking reason, so we have to signal up in a different manner
						// throw;
						lastCommandResponse = new CommandResponse("", -1);
						return lastCommandResponse;
					}
				}
				catch (ThreadAbortException e)
				{
					//reader.DiscardBufferedData();
					Monitor.Exit(executeLock);
					lastCommandResponse = new CommandResponse("", -1);
					return lastCommandResponse;
				}

			} 

		}
		public void DisconnectAndCheckReconnect(bool force)
		{
			Disconnect(force);
			// only go in here it nobodyes a) disconnecting, or b) is already in here
			if (Monitor.TryEnter(disconnectionLock))
			{
				if (!reconnecting && !terminating && !connected)
				{
					reconnecting = true;
					// always reconnect it we're doing something in a race.
					// actually the only time we don't want to reconect is when doing anti-idle outside a race and the always reconnect setting is false
					if (!terminating && (site.Reconnect || transferring || racing))
					{
						Log("reconnecting");
						// this method automatically restarts the anti-idle, so that's cool
						Connect();
						// maybe we should give this a certain amount of tries, or a certain time, after which time we exclude it from the race.
						// don't do this, it returns immediately, which is wrong. the normal Connect() method returns back to the conenction pool anyway
						//ConnectionPool.Connect(site);
						// this needs to wait until it's actually connected, which it fails to do when we encounter ghosts and have to force-reconnect
						Log("done reconnecting (DisconnectAndCheckReconnect)");
					}
					else
					{
						Log("NOT reconnecting");
						// we're already terminating anti-idle in the disconnect method
						/*if (antiIdle != null)
						{
							Log("aborting anti idle");
							antiIdle.Abort();	
							antiIdle = null;
						}*/
						// NOTE: we can't handle returning this connection to the cache here, we nedd to return the entire site connection
					}
					reconnecting = false;
				}
				Monitor.Exit(disconnectionLock);
			}
			else
			{
				// we can't very well be disconnecting and reconnecting at the same time.
				// a disconnect has to come first, and complete. and since we can't try a reconnect without a disconnect, this will work just fine
				Logger.Log("somebody was already disconnecting or reconnecting (in reconnect)");
			}
		}

		public void Disconnect(bool force)
		{
			Log("disconnecting...");
			if (Monitor.TryEnter(disconnectionLock)) 
			{
				if (connected)
				{
					bool wasConnected = connected;
					connected = false;
					connecting = false;
					if (wasConnected) 
					{
						if (force)
						{
							reader.Close();
							writer.Close();
							try 
							{
								controlConnection.LingerState = new LingerOption(true, 0);
							}
							catch
							{
								// nothing much we can do here, we just don't want to do this on a "disposed" object
							}
						}
						else
						{
							// let's not do this here, we don't want Execute() to do funky stuff to the connection pool if something goes awry here
							//Execute("QUIT");
							try
							{
								writer.Write("QUIT\r\n");
								writer.Flush();
								reader.ReadLine();
							}
							catch
							{
								// nothing here, it doesn't matter.
							}
						}

						controlConnection.Close();
						if (antiIdle != null)
						{
							antiIdle.Abort();
							antiIdle = null;
						}
					}
				}
				Monitor.Exit(disconnectionLock);
			}
			else
			{
				// somebody else was already disconnectin, let's skip it
				Logger.Log("somebody was already disconnecting or reconnecting (in disconnect)");
			}
		}


		private void CommenceAntiIdle()
		{
			if (antiIdle == null)
			{
				antiIdle = new Thread(new ThreadStart(this.AntiIdle));
				antiIdle.Name = "AntiIdle:" + site.Name + (source ? "(src)" : "(dst)");
				// this way we don't have to worry about terminating it when we quit
				antiIdle.IsBackground = true;
				antiIdle.Start();
			}
		}

		private void AntiIdle()
		{
			while (true)
			{
				// sleep first, so that if we execute commands right after eachother, we have nothing in the reader to purge
				Thread.Sleep(site.IdleInterval * 1000);
				if (transferring)
				{
					// don't send anti-idle while we're transferring
					Log("we're transferring data, skipping anti-idle");
					continue;
				}
				lock (executeLock) 
				{
					CommandResponse cr = null;
					if (site.IdleCommand.Length == 0) 
					{
						string cmd = (string)randomAntiIdleCommands[random.Next(randomAntiIdleCommands.Count)];
						Log("Anti-idle(random): " + cmd);
						cr = Execute(cmd);
					}
					else 
					{
						Log("Anti-idle: " + site.IdleCommand);
						cr = Execute(site.IdleCommand);
					}
//					Log(cr.Response);
				}
			}
		}

		public void Terminate(bool force)
		{
			terminating = true;
			Disconnect(force);
		}
		private void Log(string message)
		{
			lock (logLock) 
			{
				Logger.Log(site.Name + (source ? "(src)" : "(dst)"), message);
			}
		}

		public override string ToString()
		{
			return site.Name + (source ? "(src)" : "(dst)");
		}


		public ArrayList FileList
		{
			get
			{
				return fileList;
			}
		}

		public ArrayList DirList
		{
			get { return dirList; }
		}

		public Site Site
		{
			get { return site; }
		}

		public bool Transferring
		{
			get { return transferring; }
			set { transferring = value;}
		}

		public bool Connected
		{
			get { return connected; }
		}

		public bool Connecting
		{
			get { return connecting; }
		}

		public bool Racing
		{
			get { return racing; }
			set { racing = value; }
		}

		public int LoginResponseCode
		{
			get { return loginResponseCode; }
		}

		public bool Source
		{
			get { return source; }
		}

		public string Pwd
		{
			get { return pwd; }
			set { pwd = value; }
		}

		public string LoginErrorMessage
		{
			get { return loginErrorMessage; }
		}

		public FxpTransfer Transfer
		{
			get { return transfer; }
			set { transfer = value; }
		}

		public bool Purge
		{
			get { return purge; }
			set { purge = value; }
		}

		public CommandResponse LastCommandResponse
		{
			get { return lastCommandResponse; }
		}

		public class CommandResponse
		{
			private string response;
			private int responseCode;

			public CommandResponse(string response, int resposneCode)
			{
				this.response = response;
				this.responseCode = resposneCode;
			}

			public string Response
			{
				get { return response; }
			}

			public int ResponseCode
			{
				get { return responseCode; }
			}
		}

		public void Reset()
		{
			fileList = new ArrayList();
			dirList = new ArrayList();
			racing = false;
			if (connected) 
			{
				Cwd("/");
			}

		}
	}
}

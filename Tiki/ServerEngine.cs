using System;
using System.Collections;
using System.Threading;

namespace Tiki
{
	/// <summary>
	/// Summary description for ServerEngine.
	/// </summary>
	public class ServerEngine
	{
		private ClientEngine clientEngine;
		private Hashtable races;
		private ArrayList pendingRaces;
		private object raceCreateLock = new object();
		private object pendingRacesLock = new object();

		public ServerEngine(ClientEngine clientEngine)
		{
			this.clientEngine = clientEngine;
			// todo: when a site disconnects, it has to be removed from the set. (remember synchro)
			races = Hashtable.Synchronized(new Hashtable());
			pendingRaces = new ArrayList();
		}
		public Race CreateNewRace(RaceAnnounce announce)
		{
			// TODO: this MUST be a proper queue, otherwise two races might check out eachothers sites, and that'll cause problems
			// one queue per race section?
			// maybe put a lock here that's not released quicker than 5 seconds, so we start races at the earliest in 5 second intervals

// TEST: try putting this outside the lock, so that the gui can be updated faster!
			Race race = new Race(this, announce);
			UpdateRace(race);

			// this is where new race creation stops becure listing new races on the gui, that might have to be remedied
			lock (raceCreateLock) 
			{

				// NEW: move this here so that the race shows up in the listing faster
				races.Add(announce.ReleaseName, race);
				pendingRaces.Add(race);

				// NOTE: the new connection pool is completely async, so we need to wait for connections to come through.
				// the connection pool will wait for connections before returning them
				ConnectionPool.ConnectSites(announce);
				// must do this first. a queue is a queue is a queue
				CheckPendingRaces();
				
// there is no need to this at all, the CheckPendingRaces() will do the exact same thing anyway!
/*				Logger.Log("6");
				// PROBLEM!: this takes time, so the gui becomes unresponsive while this happens, which is, ofcourse, unacceptable.
				// yet we can't start a thread for something that requires parameters...	
				// SOLVED via the RaceStart class		
				race.CheckOutWantedSites();
				if (race.Connections.Count > 1)
				{
					Logger.Log("7");
					// tell the gui that the race is active (no, races are active by default)
					Thread t = new Thread(new ThreadStart(race.Trade));
					Logger.Log("8");
					t.Name = "Race:" + announce.ReleaseName;
					t.IsBackground = true;
					t.Start();	
					Logger.Log("9");
					UpdateRace(race);
					Logger.Log("10");
				}
				else
				{
					Logger.Log("Could not check out enough sites for this race, putting it in queue:" + race.ReleaseName);
					Logger.Log("11");
					// NOTE: don't return the possible ONE connection we have here. if we keep it, there's a better chance that we can get
					// this race of the ground next time
					// or not, just let the races finish, and let the other connections hang free, since CheckPendingRaces() return sit's connections
					race.ReturnConnections();
//					pendingRaces.Add(race);
					UpdateRace(race);
					// add this race to the race queue, and see if we can't start it the next time something is checked in.
					// tell the gui that this race is pending...
				}
				Logger.Log("12");
*/
				return race;
			}
		}
		public Race CreateAndRunNewSubRace(Race parentRace, string subdirName)
		{
			// hmm, maybe this shoulnd't start a thread.
			RaceAnnounce announce = new RaceAnnounce(parentRace.Announce.AnnounceType, parentRace.Announce.ReleaseName + "/" + subdirName, parentRace.Announce.Source, parentRace.Announce.Destinations, parentRace.Announce.Section, parentRace.Announce.AnnounceLine);
			if (!races.Contains(announce.ReleaseName)) 
			{
				
				Race subrace = new Race(this, announce);
				subrace.RegisterParentRace(parentRace);
				lock (raceCreateLock) 
				{
					races.Add(announce.ReleaseName, subrace);
				}
				subrace.CheckOutWantedSites();
				// this blocks while the race is in progress
				// you can't change the name of a thread
				subrace.Trade();
				UpdateRace(subrace);
				lock (raceCreateLock) 
				{
					races.Remove(announce.ReleaseName);
				}
				return subrace;
			}
			else
			{
				Logger.Log("Subrace " + (parentRace.Announce.ReleaseName + "/" + subdirName) + " already existed");
				return null;
			}
		}

		public void UpdateRace(Race race)
		{
			clientEngine.UpdateRace(race);
			// this must check if the race is complete, remove it from out lists.
			if (race.Complete)
			{
				races.Remove(race.ReleaseName);
				// it doesn't matter if we run a CheckPendingRaces when we terminate subraces, since their parent race will still have the connections checked out.
				// it won't happen, now that we block for each subrace.
				// which is also good, otherwise the checkout connections would be automatic, and all hell would break loose
				CheckPendingRaces();
			}
		}

		private void CheckPendingRaces()
		{
			lock (pendingRacesLock) 
			{
				// this will peek into the pendingRaces queue and see if we can't get some more hosts to try the race...
				Logger.Log("checking pending races");
				ArrayList pendingRacesToBeRemoved = new ArrayList();
				for (int i = 0; i < pendingRaces.Count; i++)
				{
					Race race = (Race) pendingRaces[i];
					race.CheckOutWantedSites();
					if (race.Connections.Count > 1)
					{
						// the source/destination thing didn't work, since they need to be 2 different hosts
						Logger.Log("starting a new race from the pending stack");
						Thread t = new Thread(new ThreadStart(race.Trade));
						t.Name = "Race:" + race.ReleaseName;
						t.Start();	
						UpdateRace(race);
						pendingRacesToBeRemoved.Add(race);
						race.ExecutingThread = t;
					}
					else
					{
						race.ReturnConnections();
					}
				}
			
				for (int i = 0; i < pendingRacesToBeRemoved.Count; i++)
				{
					Race toBeRemoved = (Race) pendingRacesToBeRemoved[i];
					pendingRaces.Remove(toBeRemoved);
				}
			}

			// going over a list instead keeps the races in order.			

			Logger.Log("done checking pending races");
		}

		public Hashtable Races
		{
			get { return races; }
		}

		public void Shutdown(bool force)
		{
			Logger.Log("Shutting down server engine");
			// todo: do stuff here
			foreach (Race race in races.Values)
			{
				race.Terminate(force);
			}
		}

		public void RemoveRaceFromQueue(Race race)
		{
			if (races.Contains(race.ReleaseName))
			{
				races.Remove(race.ReleaseName);
			}
			if (pendingRaces.Contains(race))
			{
				pendingRaces.Remove(race);
			}
			Logger.Log("race " + race.ReleaseName + " removed from engine");
		}

		public void Nuke(RaceAnnounce announce, bool entireRace)
		{
			Race race = (Race)races[announce.ReleaseName];
			if (race != null)
			{
				ArrayList sites = new ArrayList();
				sites.Add(announce.Source);
				if (entireRace)
				{
					sites.AddRange(announce.Destinations);
				}
				race.StopRacingNukedReleaseOnSites(sites);
			}
			
		}
	}
}

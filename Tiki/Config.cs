using System;
using System.Collections;
using System.Runtime.Serialization.Formatters.Soap;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace Tiki
{

	/// <summary>
	/// Summary description for Sites.
	/// </summary>
	[Serializable]
	public class Config
	{
		private static ArrayList ircs = new ArrayList();
		
		private static Hashtable bots = new Hashtable();
		private static Hashtable sites = new Hashtable();
		private static Hashtable ircChannelKeys = new Hashtable();
		private static ArrayList siteRanking = new ArrayList();
		private static SpeedMatrix speedMatrix = new SpeedMatrix();
		private static Hashtable generalSettings = new Hashtable();

		private static bool loaded = false;

		// .ToString() <-- for sending the contens over to the standalone engine

		public static void Load()
		{
			// todo: verify that we're not saving null and such, when we do new installations.
			Logger.Log("Loading...");
			SoapFormatter sf = new SoapFormatter();
			FileStream fs;

			// site ranking
			try 
			{
				fs = new FileStream("SiteRanking.xml", FileMode.Open);
				siteRanking = (ArrayList)sf.Deserialize(fs);
				fs.Close();
			}
			catch
			{
				Logger.Log("Could not load SiteRankings, skipping");
			}

			// sections + sites
			try 
			{
				// NOTE: we'll require a NewSites.xml here for a couple of versions, then we'll jsut rename, and force the users to do too (or execute a rename for them)
				fs = new FileStream("Sites.xml", FileMode.Open);
				sites = (Hashtable)sf.Deserialize(fs);
				fs.Close();
				
				// go over the sections and populate an "easy-access" map for botnames.
				foreach (Site site in sites.Values)
				{
					foreach (Section section in site.Sections.Values)
					{
						// IMPORTANT: the botnick NEEDS to be lowercase
						Config.bots[section.Botname.ToLower()] = site;
					}
				}
			} 
			catch
			{
				Logger.Log("Could not load Sites, skipping");
			}


			// channel keys
			try
			{
				fs = new FileStream("IrcChannelKeys.xml", FileMode.Open);
				ircChannelKeys = (Hashtable)sf.Deserialize(fs);
				fs.Close();
			} 
			catch
			{
				Logger.Log("Could not load IrcChannelKeys, skipping");
			}

			// irc
			try
			{
				fs = new FileStream("Irc.xml", FileMode.Open);
				ircs = (ArrayList)sf.Deserialize(fs);
				fs.Close();
			} 
			catch
			{
				Logger.Log("Could not load IrcSettings, skipping");
			}
			
			// speed matrix
			try 
			{
				fs = new FileStream("SpeedMatrix.xml", FileMode.Open);
				speedMatrix = (SpeedMatrix)sf.Deserialize(fs);
				fs.Close();
			} 
			catch
			{
				Logger.Log("Could not load SpeedMatrix, skipping");
			}

			// general settings
			try 
			{
				fs = new FileStream("GeneralSettings.xml", FileMode.Open);
				generalSettings = (Hashtable)sf.Deserialize(fs);
				fs.Close();
			} 
			catch
			{
				Logger.Log("Could not load GeneralSettings, skipping");
				generalSettings = new Hashtable();
			}
			
			loaded = true;
			// add dupelog later
			// no, initialized from engine
			// saved from there to.
			Logger.Log("Settings loaded");
			
		}
		public static void Save()
		{
			bool saveOk = true;
			if (!loaded)
			{
				DialogResult dr = MessageBox.Show("You haven't loaded your files yet. If you save now, you will destroy all your previous settings. Are you sue you want to do this?", "Overwrite previous settings?", MessageBoxButtons.YesNo);
				if (dr == DialogResult.No)
				{
					saveOk = false;
				}
			}
			if (saveOk) 
			{
				// site ranking
				FileStream fs = new FileStream("SiteRanking.xml", FileMode.Create);
				SoapFormatter sf = new SoapFormatter();
				if (siteRanking != null) 
				{
					sf.Serialize(fs, siteRanking);
				}
				fs.Close();

				// sites
				fs = new FileStream("Sites.xml", FileMode.Create);
				if (sites != null) 
				{
					sf.Serialize(fs, sites);
				}
				fs.Close();

				// channel keys
				fs = new FileStream("IrcChannelKeys.xml", FileMode.Create);
				if (ircChannelKeys != null) 
				{
					sf.Serialize(fs, ircChannelKeys);
				}
				fs.Close();

				// irc
				fs = new FileStream("Irc.xml", FileMode.Create);
				if (ircs != null) 
				{
					sf.Serialize(fs, ircs);
				}
				fs.Close();

				// speed matrix
				fs = new FileStream("SpeedMatrix.xml", FileMode.Create);
				if (speedMatrix != null) 
				{
					sf.Serialize(fs, speedMatrix);
				}
				fs.Close();

				// general settings
				fs = new FileStream("GeneralSettings.xml", FileMode.Create);
				if (generalSettings != null) 
				{
					sf.Serialize(fs, generalSettings);
				}
				fs.Close();

				// add dupelog here later
				// no, loaded and saved rom client engine
				Logger.Log("Settings saved...");
			}
		}

		public static ArrayList IrcConnectionSettings
		{
			get { return ircs; }
			set { ircs = value; }
		}

		public static Hashtable Sites
		{
			get { return sites; }
		}

		public static Hashtable Bots
		{
			get { return bots; }
		}

		public static Hashtable IrcChannelKeys
		{
			get { return ircChannelKeys; }
		}

		public static ArrayList SiteRanking
		{
			get { return siteRanking; }
			set { siteRanking = value; }
		}

		public static SpeedMatrix SpeedMatrix
		{
			get { return speedMatrix; }
		}

		public static Hashtable GeneralSettings
		{
			get { return generalSettings; }
			set { generalSettings = value; }
		}
		// skriv ett "s�kert" s�tt att h�mta olika typer av data, som g�srt kommer om det finns, sen konverterar och liknande
	}
}
